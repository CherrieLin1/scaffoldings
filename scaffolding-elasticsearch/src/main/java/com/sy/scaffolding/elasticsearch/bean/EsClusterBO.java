package com.sy.scaffolding.elasticsearch.bean;

import lombok.Data;

import java.util.Objects;

/**
 * 通常情况下，一个系统不会只有一个集群，集群的配置信息如下所示
 * @ClassName EsClusterDO
 * @Description ES集群的配置信息
 * @Author linshengyan
 * @Date 2022/3/29 11:26 上午
 */
@Data
public class EsClusterBO {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 编码
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 集群分类：0公共集群，1私有集群
     */
    private Integer type;

    /**
     * 节点
     */
    private String nodes;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 最大连接数
     */
    private Integer maxConnTotal;

    /**
     * 最大路由连接数
     */
    private Integer maxConnPerRoute;

    /**
     * 连接超时时间
     */
    private Integer connectTimeout;

    /**
     * 连接超时时间
     */
    private Integer socketTimeout;

    /**
     * 描述
     */
    private String remark;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EsClusterBO that = (EsClusterBO) o;
        return code.equals(that.code);
    }

    @Override
    public int hashCode() {
        return Objects
                .hash(code);
    }
}
