package com.sy.scaffolding.elasticsearch.test;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import com.sy.scaffolding.elasticsearch.bean.EsClusterBO;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName ConcurrentMapTest
 * @Description
 * @Author linshengyan
 * @Date 2022/3/29 11:30 上午
 */
public class ConcurrentMapTest {
    private static ConcurrentHashMap<EsClusterBO, RestHighLevelClient> map = new ConcurrentHashMap<>();

    public static void main(String[] args) {
        testHap();
    }

    public static void testHap() {
        EsClusterBO esClusterBO = new EsClusterBO();
        esClusterBO.setCode("ghjkjbn");
        RestHighLevelClient restHighLevelClient = new RestHighLevelClient(RestClient.builder(
                new HttpHost("127.0.0.1", 9200, "http")));
        RestHighLevelClient put = map.put(esClusterBO, restHighLevelClient);
        RestHighLevelClient restHighLevelClient1 = map.computeIfAbsent(esClusterBO, x -> restHighLevelClient);
        System.out.println("put == restHighLevelClient1 = " + (put == restHighLevelClient1));
    }
}
