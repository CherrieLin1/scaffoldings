package com.sy.scaffolding.elasticsearch.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 这个类主要演示了，如果只有一个es集群，在项目初始化的时候，就可以创建这个客户端类
 * @author create by lsy on 2022/2/8 6:40 下午
 */
@Configuration
public class ElasticsearchClientConfig {


    /**
     * 构建一个Es的高级客户端
     * @return
     */
    @Bean
    public RestHighLevelClient restHighLevelClient() {
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("127.0.0.1",9200,"http")));
        return client;
    }
}
