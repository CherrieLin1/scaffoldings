package study.concurrent.kuangstudy.demo01;


//多线程实现图片下载

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class TestThread2 extends Thread {

    /**
     * url
     */
    private String url;
    /**
     * name
     */
    private String name;

    public TestThread2(String url,String name){
        this.url = url;
        this.name = name;
    }

    @Override
    public void run() {
        //下载图片
        WebDownloader webDownloader = new WebDownloader();//下载器
        webDownloader.downloader(url,name);//下载文件的方式
        System.out.println("下载了图片-->"+name);

    }

    public static void main(String[] args) {
        TestThread2 t1 = new TestThread2("https://image.baidu.com/search/detail?ct=503316480&z=0&ipn=d&word=common-io%E5%8C%85&step_word=&hs=0&pn=0&spn=0&di=22170&pi=0&rn=1&tn=baiduimagedetail&is=0%2C0&istype=0&ie=utf-8&oe=utf-8&in=&cl=2&lm=-1&st=undefined&cs=1967255326%2C2324840954&os=2484583416%2C1607090568&simid=3242553629%2C37726539&adpicid=0&lpn=0&ln=1575&fr=&fmq=1637308628415_R&fm=&ic=undefined&s=undefined&hd=undefined&latest=undefined&copyright=undefined&se=&sme=&tab=0&width=undefined&height=undefined&face=undefined&ist=&jit=&cg=&bdtype=14&oriquery=&objurl=https%3A%2F%2Fimg14.360buyimg.com%2Fpop%2Fjfs%2Ft1%2F161006%2F24%2F18923%2F151819%2F60783be6Ef5519910%2Fb0a2590db0be356b.jpg&fromurl=ippr_z2C%24qAzdH3FAzdH3Ftpj4_z%26e3B4_z%26e3B31_z%26e3Bv54AzdH3Fr6517vpAzdH3F8aadl9a08dadnc_z%26e3Bip4s%3Fv7%3Dp67j%267p4_f576vj%3Dkwt17-37ij%267p4_4j1t74%3Dh5g2%267p4_vw4rwt2g%3Dp_8aaa8c8dna_37ij&gsm=1&rpstart=0&rpnum=0&islist=&querylist=&nojc=undefined&dyTabStr=MCw1LDEsNiw0LDMsNyw4LDIsOQ%3D%3D","你好1.jpg");
        TestThread2 t2 = new TestThread2("https://image.baidu.com/search/detail?ct=503316480&z=0&ipn=d&word=common-io%E5%8C%85&step_word=&hs=0&pn=0&spn=0&di=22170&pi=0&rn=1&tn=baiduimagedetail&is=0%2C0&istype=0&ie=utf-8&oe=utf-8&in=&cl=2&lm=-1&st=undefined&cs=1967255326%2C2324840954&os=2484583416%2C1607090568&simid=3242553629%2C37726539&adpicid=0&lpn=0&ln=1575&fr=&fmq=1637308628415_R&fm=&ic=undefined&s=undefined&hd=undefined&latest=undefined&copyright=undefined&se=&sme=&tab=0&width=undefined&height=undefined&face=undefined&ist=&jit=&cg=&bdtype=14&oriquery=&objurl=https%3A%2F%2Fimg14.360buyimg.com%2Fpop%2Fjfs%2Ft1%2F161006%2F24%2F18923%2F151819%2F60783be6Ef5519910%2Fb0a2590db0be356b.jpg&fromurl=ippr_z2C%24qAzdH3FAzdH3Ftpj4_z%26e3B4_z%26e3B31_z%26e3Bv54AzdH3Fr6517vpAzdH3F8aadl9a08dadnc_z%26e3Bip4s%3Fv7%3Dp67j%267p4_f576vj%3Dkwt17-37ij%267p4_4j1t74%3Dh5g2%267p4_vw4rwt2g%3Dp_8aaa8c8dna_37ij&gsm=1&rpstart=0&rpnum=0&islist=&querylist=&nojc=undefined&dyTabStr=MCw1LDEsNiw0LDMsNyw4LDIsOQ%3D%3D","你好2.jpg");
        TestThread2 t3 = new TestThread2("https://image.baidu.com/search/detail?ct=503316480&z=0&ipn=d&word=common-io%E5%8C%85&step_word=&hs=0&pn=0&spn=0&di=22170&pi=0&rn=1&tn=baiduimagedetail&is=0%2C0&istype=0&ie=utf-8&oe=utf-8&in=&cl=2&lm=-1&st=undefined&cs=1967255326%2C2324840954&os=2484583416%2C1607090568&simid=3242553629%2C37726539&adpicid=0&lpn=0&ln=1575&fr=&fmq=1637308628415_R&fm=&ic=undefined&s=undefined&hd=undefined&latest=undefined&copyright=undefined&se=&sme=&tab=0&width=undefined&height=undefined&face=undefined&ist=&jit=&cg=&bdtype=14&oriquery=&objurl=https%3A%2F%2Fimg14.360buyimg.com%2Fpop%2Fjfs%2Ft1%2F161006%2F24%2F18923%2F151819%2F60783be6Ef5519910%2Fb0a2590db0be356b.jpg&fromurl=ippr_z2C%24qAzdH3FAzdH3Ftpj4_z%26e3B4_z%26e3B31_z%26e3Bv54AzdH3Fr6517vpAzdH3F8aadl9a08dadnc_z%26e3Bip4s%3Fv7%3Dp67j%267p4_f576vj%3Dkwt17-37ij%267p4_4j1t74%3Dh5g2%267p4_vw4rwt2g%3Dp_8aaa8c8dna_37ij&gsm=1&rpstart=0&rpnum=0&islist=&querylist=&nojc=undefined&dyTabStr=MCw1LDEsNiw0LDMsNyw4LDIsOQ%3D%3D","你好3.jpg");

        t1.start();
        System.out.println("执行了t1");
        t2.start();
        System.out.println("执行了t2");
        t3.start();
        System.out.println("执行了t3");
    }
}


/**
 * 下载图片
 */
class WebDownloader{

    //

    /**
     * 下载方法
     * @param url
     * @param name
     */
    public void downloader(String url,String name){
        try {
            FileUtils.copyURLToFile(new URL(url),new File(name));
        } catch (IOException e) {
            e.printStackTrace();
            //输出异常信息
            System.out.println("downloader方法出现异常");
        }
    }

}
