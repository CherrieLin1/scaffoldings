package study.concurrent.kuangstudy.state;

/**
 * 线程的礼让
 * 礼让不一定成功
 */
public class TestYield {

    public static void main(String[] args) throws InterruptedException {

        MyYield myYield = new MyYield();
        new Thread(myYield, "子线程").start();

        for (int i = 0; i < 1000; i++) {
            if (i == 50) {
                System.out.println("主线程执行到循环的50，开始礼让");
                Thread.yield();
            }
            System.out.println(Thread.currentThread().getName() + "---->" + i);
        }

    }

}

class MyYield implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println(Thread.currentThread().getName() + "---->" + i);
        }
    }
}