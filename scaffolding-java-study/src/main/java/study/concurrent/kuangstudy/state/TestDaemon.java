package study.concurrent.kuangstudy.state;

/**
 * 测试守护线程--用户线程结束，守护线程就可以结束了
 * 用户线程: 虚拟机要等待用户线程执行完毕
 * 守护线程: 虚拟机不用等待守护线程执行完毕
 */
public class TestDaemon {
    public static void main(String[] args) {
        God god = new God();
        Thread thread = new Thread(god);
        //设置线程为守护线程,默认为false
        thread.setDaemon(true);
        //守护线程是一个死循环，如果用户线程不结束，会一直执行
        thread.start();

        //用户线程
        new Thread(() -> {
            for (int i = 0; i < 30; i++) {
                System.out.println("你开心的在这个世界上活着" + i);
            }
            System.out.println("=======Goodbye , World!======= ");
        }).start();
    }
}

/**
 * 守护线程
 */
class God implements Runnable {
    @Override
    public void run() {
        while (true) {
            System.out.println("上帝保佑着你");
        }
    }
}