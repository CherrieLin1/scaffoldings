package study.concurrent.kuangstudy.state;

/**
 * 观测线程的状态
 */
public class TestState {
    public static void main(String[] args) throws InterruptedException {

        Thread thread = new Thread(()->{
            for (int i = 0; i < 5; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        //未启动，查看状态
        Thread.State state = thread.getState();
        System.out.println("未启动前的状态：state = " + state);


        //启动后的状态
        thread.start();
        state = thread.getState();
        System.out.println("启动后的状态state = " + state);

        while (state != Thread.State.TERMINATED){
            Thread.sleep(100);
            //更新状态
            state = thread.getState();
            System.out.println("当前的状态state = " + state);
        }


    }
}
