package study.concurrent.kuangstudy.state;

/**
 * @author create by lsy on 2021/9/25 2:38 下午
 */

public class TestStop implements Runnable{
    private boolean flag = true;

    @Override
    public void run() {
        int i =0;
        while (flag){
            System.out.println("run ...thread" + i++);
        }
    }

    public void stop(){
        this.flag = false;
    }


    public static void main(String[] args) throws InterruptedException {
        TestStop testStop = new TestStop();
        new Thread(testStop).start();
        Thread.sleep(1000);
        for (int i = 0; i < 1000; i++) {
            System.out.println("main......thread" + i);
            if (i ==900){
                testStop.stop();
                System.out.println("线程改停止了");
            }
        }
    }
}
