package study.concurrent.kuangstudy.state;

/**
 * 测试线程优先级
 */
public class TestPriority {

    public static void main(String[] args) {
        //主线程默认优先级
        System.out.println(Thread.currentThread().getName() + "-->" + Thread.currentThread().getPriority());


        MyPriority myPriority = new MyPriority();
        Thread thread1 = new Thread(myPriority, "线程1");
        Thread thread2 = new Thread(myPriority, "线程2");
        Thread thread3 = new Thread(myPriority, "线程3");
        Thread thread4 = new Thread(myPriority, "线程4");
        Thread thread5 = new Thread(myPriority, "线程5");


        thread1.setPriority(1);
        thread3.setPriority(Thread.MAX_PRIORITY);
        thread4.setPriority(3);
        thread5.setPriority(10);

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();

    }


}


class MyPriority implements Runnable {

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + "-->" + Thread.currentThread().getPriority());
    }
}