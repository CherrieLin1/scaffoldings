package study.concurrent.kuangstudy.state;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 线程休眠测试
 */
public class TestSleep2 {
    public static void main(String[] args) throws InterruptedException {

        //1.测试倒计时
        TestSleep2 testSleep2 = new TestSleep2();
        testSleep2.tenDown();

        //2.获取系统时间
        //获取系统当前时间
        Date startTime = new Date(System.currentTimeMillis());


        //注意这是一个死循环，要手动结束
        while (true) {
            System.out.println(new SimpleDateFormat("HH:mm:ss").format(startTime));
            Thread.sleep(1000);
            //更新当前时间
            startTime = new Date(System.currentTimeMillis());
        }

    }


    /**
     * 倒计时方法，一秒钟倒数一次
     * @throws InterruptedException
     */
    private void tenDown() throws InterruptedException {
        int num = 10;
        for (int i = 10; i > 0; i--) {
            Thread.sleep(1000);
            System.out.println("倒计时:"+i);
        }

    }


}
