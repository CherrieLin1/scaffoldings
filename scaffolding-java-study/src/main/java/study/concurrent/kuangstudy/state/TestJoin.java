package study.concurrent.kuangstudy.state;

/**
 * join 线程插队 - 会强制执行
 * 想要插队的线程调用join()方法
 */
public class TestJoin implements Runnable {

    public static void main(String[] args) throws InterruptedException {
        //插队线程启动
        TestJoin testJoin = new TestJoin();
        Thread thread = new Thread(testJoin);
        thread.start();

        for (int i = 0; i < 100; i++) {
            if (i == 80) {
                //主线程执行到80的时候，子线程强制插队执行，主线程不再执行
                thread.join();
            }
            System.out.println("我是主线程:" + i);
        }

    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("我是VIP线程:" + i);
        }
    }
}

