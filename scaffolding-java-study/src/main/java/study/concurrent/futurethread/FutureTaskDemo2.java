package study.concurrent.futurethread;

import org.apache.commons.io.monitor.FileAlterationMonitor;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @ClassName 线程池的方式实现future
 * @Description
 * @Author linshengyan
 * @Date 2022/7/29 2:40 下午
 */
public class FutureTaskDemo2 {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(10);
        ArrayList<Future> futureList = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            Future<String> future;
            if (i == 0 || i == 4) {
                future = service.submit(new SlowTask());
            } else {
                future = service.submit(new FastTask());
            }
            futureList.add(future);
        }

        for (Future future : futureList) {
            System.out.println("future.get() = " + future.get());
        }

        service.shutdown();
    }


    static class SlowTask implements Callable {
        @Override
        public String call() throws Exception {
            Thread.sleep(4000);
            return Thread.currentThread().getName() +"执行一个慢任务";
        }
    }


    static class FastTask implements Callable {

        @Override
        public String call() throws Exception {
            return Thread.currentThread().getName() +"执行一个快任务";
        }
    }

}
