package study.concurrent.futurethread;

import lombok.SneakyThrows;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @ClassName CompletableFutureDemo实现并发超时问题
 * @Description
 * @Author linshengyan
 * @Date 2022/7/29 2:54 下午
 */
public class CompletableFutureDemo {


    public static void main(String[] args) {
        CompletableFutureDemo completableFutureDemo = new CompletableFutureDemo();

        System.out.println("completableFutureDemo.getPrice() = " + completableFutureDemo.getPrice());

    }

    private Set<Integer> getPrice() {
        //方法用于返回一个同步的(线程安全的)有序set由指定的有序set支持
        Set<Integer> prices = Collections.synchronizedSet(new HashSet<Integer>());
        CompletableFuture<Void> future1 = CompletableFuture.runAsync(new Task(prices));
        CompletableFuture<Void> future2 = CompletableFuture.runAsync(new Task(prices));
        CompletableFuture<Void> future3 = CompletableFuture.runAsync(new Task(prices));
        CompletableFuture<Void> allTasks = CompletableFuture.allOf(future1, future2, future3);


        try {
            //阻塞获取线程的返回结果或者超时获取
            allTasks.get(1, TimeUnit.SECONDS);
//            allTasks.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
        return prices;


    }


    private class Task implements Runnable {
        Set<Integer> prices;

        public Task(Set<Integer> prices) {
            this.prices = prices;
        }

        @SneakyThrows
        @Override
        public void run() {
            int price = (int) (Math.random() * 8000);
            //模拟耗时
            Thread.sleep((long) (Math.random() * 4000));
            //模拟查询出来的票价
            prices.add(price);

        }
    }

}
