package study.concurrent.ThreadCooperation;


import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * condition的使用
 * ReentrantLock.lock()同时只能被同一个线程占用
 * 由于主线程执行速度比子线程快，理论上会执行完method1以后才会执行method2,
 * 但是method1中调用了condition.await()，会释放锁，此时method2获取了lock锁，便会执行程序，执行完调用signal()方法唤醒其他线程，并释放锁后，method1才会继续执行
 *
 *
 * @author create by lsy on 2021/11/19 6:23 下午
 */
public class ReentrantLockConditionTest {
    private ReentrantLock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();

    public static void main(String[] args) throws InterruptedException {
        ReentrantLockConditionTest conditionTest = new ReentrantLockConditionTest();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    conditionTest.method2();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        conditionTest.method1();
    }

    public void method1() throws InterruptedException {
        //是一个阻塞获取的方法
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + ":条件不满足，开始await");
            //Condition的await在调用时，内部会调用release去释放同步状态，来达到唤醒后继等待线程目的，从而实现释放锁的表象，实际是对于LockSupport的park和unpark的灵活运用。
            //LockSupport是用来创建locks的基本线程阻塞基元，比如AQS中实现线程挂起的方法，就是park,对应唤醒就是unpark。
            condition.await();
            System.out.println(Thread.currentThread().getName() + ":条件满足了，开始执行后续的任务");
        } finally {
            lock.unlock();
        }
    }

    public void method2() throws InterruptedException {
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + ":需要5秒钟的执行时间");
            Thread.sleep(5000);
            System.out.println(Thread.currentThread().getName() + ":工作完成，唤醒其他的线程");
            condition.signal();
        } finally {
            lock.unlock();
        }
    }


}
