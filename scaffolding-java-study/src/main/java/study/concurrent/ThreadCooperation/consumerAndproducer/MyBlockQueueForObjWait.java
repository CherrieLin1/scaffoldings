package study.concurrent.ThreadCooperation.consumerAndproducer;

import java.util.LinkedList;

/**
 * @ClassName MyBlockQueueForObjWait
 * @Description
 * @Author linshengyan
 * @Date 2022/7/27 4:12 下午
 */
public class MyBlockQueueForObjWait {
    private int max;
    private LinkedList<Object> storage;

    public MyBlockQueueForObjWait(int size) {
        this.max = size;
        storage = new LinkedList<>();
    }


    public void put() throws InterruptedException {
        synchronized (this) {
            while (storage.size() == max) {
                System.out.println(Thread.currentThread().getName() + "--->队列满了,等待！");
                wait();
            }
            System.out.println(Thread.currentThread().getName() + "----> 添加数据");
            storage.add(new Object());
            notifyAll();
        }
    }


    public Object take() throws InterruptedException {
        synchronized (this) {
            while (storage.size() == 0) {
                System.out.println(Thread.currentThread().getName() + "--->队列空的,等待！");
                wait();
            }
            System.out.println(Thread.currentThread().getName() + "----> 消费数据");
            Object item = storage.poll();
            notifyAll();
            return item;
        }
    }

    public static void main(String[] args) {
        MyBlockQueueForObjWait myBlockQueueForObjWait = new MyBlockQueueForObjWait(16);
        Runnable put = () -> {
            for (int i = 1; i <= 100; i++) {
                try {
                    myBlockQueueForObjWait.put();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("添加了-->" + i + "次");
            }
        };


        Runnable take = () -> {
            for (int i = 1; i <= 100; i++) {
                try {
                    myBlockQueueForObjWait.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("消费了-->" + i + "次");

            }


        };

        Thread thread1 = new Thread(put,"put1--A");
        Thread thread2 = new Thread(put,"put1--B");
        Thread thread3 = new Thread(take,"take1--C");
        Thread thread4 = new Thread(take,"take2--D");
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();

    }

}
