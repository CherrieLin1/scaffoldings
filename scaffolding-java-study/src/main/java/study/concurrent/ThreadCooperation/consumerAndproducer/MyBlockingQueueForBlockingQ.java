package study.concurrent.ThreadCooperation.consumerAndproducer;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * @ClassName BlockingQueueTest
 * @Description
 * @Author linshengyan
 * @Date 2022/7/27 10:56 上午
 */
public class MyBlockingQueueForBlockingQ {
    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<Object> queue = new ArrayBlockingQueue<>(10);

        Runnable produce = () -> {
            while (true) {
                try {
                    queue.put(new Object());
                    System.out.println(Thread.currentThread().getName() + "---->put object,after queue size() = " + queue.size());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(produce).start();
        new Thread(produce).start();


        //等待队列装满
        Thread.sleep(3000);


        Runnable consumer = () -> {
            while (true) {
                try {
                    queue.take();
                    System.out.println(Thread.currentThread().getName() + "---->take object,after queue size() = " + queue.size());
                    //阻塞查看生产队列是不是会生产数据
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        new Thread(consumer).start();
        new Thread(consumer).start();
    }

}
