package study.concurrent.ThreadCooperation.consumerAndproducer;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @ClassName MyBlockingQueueForCondition
 * @Description
 * @Author linshengyan
 * @Date 2022/7/27 11:15 上午
 */
public class MyBlockingQueueForCondition {
    private Queue queue;
    private int max = 16;
    private ReentrantLock lock = new ReentrantLock();
    /**
     * 两个condition的hashcode不一致
     */
    /**
     * 队列没有空
     */
    private Condition notEmpty = lock.newCondition();

    /**
     * 队列没有满
     */
    private Condition notFull = lock.newCondition();

    public MyBlockingQueueForCondition(int size) {
        this.max = size;
        queue = new LinkedBlockingQueue();
    }

    public void put() {
        lock.lock();
        try {
            //如果队列满了，等待消费，没满（notFull）等待，因为此时已经满了
            while (queue.size() == max) {
                System.out.println(Thread.currentThread().getName() + "-----> 队列满了，线程进入wait状态");
                notFull.await();
                System.out.println(Thread.currentThread().getName() + "-----> 被唤醒，当前队列的长度=MAX?" + (queue.size() == max));
            }

            System.out.println(Thread.currentThread().getName() + "----> 队列没有满，线程开始往队列添加数据");
            //队列没有满，添加数据
            queue.add(new Object());
            System.out.println(Thread.currentThread().getName() + "----> 添加后队列的长度为：" + queue.size() + ",并通知消费线程消费数据");

            //队列有数据了，notEmpty唤醒
            notEmpty.signalAll();
        } catch (InterruptedException e) {
            e.printStackTrace();

        } finally {
            lock.unlock();
        }
    }

    public Object take() {
        lock.lock();
        try {
            //获取数据，发现队列没有数据，没空（notEmpty）等待 ，因为此时确实已经空了
            while (queue.size() == 0) {
                System.out.println(Thread.currentThread().getName() + "--->队列为空，等待数据的添加");
                notEmpty.await();
                System.out.println(Thread.currentThread().getName() + "--->线程被唤醒，当前队列的长度=MAX?" + (queue.size() == max));
            }

            //没空的情况消费，消费完了队列肯定已经不满了，此时通知生产者队列继续添加
            System.out.println(Thread.currentThread().getName() + "----> 队列不为空，线程开始消费队列数据，消费前队列长度为:" + queue.size());
            Object item = queue.remove();
            System.out.println(Thread.currentThread().getName() + "----> 消费后队列长度为:" + queue.size() + ",并通知添加线程补充队列数据");
            notFull.signalAll();
            return item;
        } catch (Exception e) {
            e.printStackTrace();
            return null;

        } finally {
            lock.unlock();
        }

    }


    public static void main(String[] args) throws InterruptedException {
        MyBlockingQueueForCondition block = new MyBlockingQueueForCondition(16);

        Runnable put = () -> {
            while (true) {
                block.put();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Runnable take = () -> {
            while (true) {
                block.take();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };


        Thread thread2 = new Thread(put, "put线程--A");
        Thread thread3 = new Thread(put, "put线程--B");

        Thread thread4 = new Thread(take, "take获取线程--C");

        thread4.start();

        Thread.sleep(500);

        thread2.start();
        thread3.start();


    }


}
