package study.concurrent.ThreadCooperation;


import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @ClassName CountDownLatchTest
 * @Description 测试countDownLatch的使用
 * @Author linshengyan
 * @Date 2022/4/8 5:23 下午
 */
public class CountDownLatchTest {

    public static void main(String[] args) throws InterruptedException {
        //所有人等待统一的信号开始做事
        allWaitForOne();

        System.out.println("==================");

        //等待所有人结束了，才结束一件事
        oneWaitForAll();
    }


    /**
     * 所有人等待统一的信号开始做事
     * 例如：所有的运动员等待裁判枪声开始跑步
     */
    public static void allWaitForOne() throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        CountDownLatch count = new CountDownLatch(1);
        System.out.println("运动员有5秒的准备时间！");
        for (int i = 0; i < 5; i++) {
            final int current = i + 1;
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        System.out.println("第" + current + "运动员准备完毕，等待信号枪响！");
                        //这时候线程会被阻塞住，直到count满足条件【等待信号枪响】
                        count.await();
                        System.out.println("第" + current + "运动员开始跑步！");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            executorService.submit(runnable);
        }
        //养成好习惯，线程池用完了以后要关闭
        executorService.shutdown();
        //线程等待5秒
        Thread.sleep(5000);
        //信号枪开始响
        System.out.println("5秒准备时间已过，信号枪响，开始跑步！");
        count.countDown();
    }



    /**
     * 等待所有人结束了，才结束一件事
     * 例如：所有的运动员都跑完步了，比赛才算结束
     */
    public static void oneWaitForAll() throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        CountDownLatch count = new CountDownLatch(5);
        System.out.println("五个同学开始比赛跑步");
        for (int i = 0; i < 5; i++) {
            final int current = i + 1;
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        //模拟跑步
                        Thread.sleep(current * 1000);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        //计数减少
                        System.out.println("第" + current + "同学完成了跑步。");
                        count.countDown();

                    }
                }
            };
            executorService.submit(runnable);
        }
        //养成好习惯，线程池用完了以后要关闭
        executorService.shutdown();
        System.out.println("等待5个运动员都跑完。。。。。");
        //这里是await，不是wait,wait是进行等待，并且等待被唤醒
        count.await();
        System.out.println("所有运动员都跑完了，比赛结束。。。。");

    }
}
