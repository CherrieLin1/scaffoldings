package study.concurrent.ThreadCooperation;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * 信号量Semaphore，用来控制并发的量，类似于令牌，但是限流不要用这个
 * 不能代替固定线程池，因为线程池一旦创建不可改变，但是信号量可以在固定的时间段内进行限流
 * 在代码逻辑中加上判断，当在某个时间段内，获取令牌后才执行逻辑
 *
 * @author create by lsy on 2021/11/22 10:01 上午
 */
public class SemaphoreTest {
    /**
     * 初始化三个信号量
     */
    static Semaphore semaphore = new Semaphore(3);


    public static void main(String[] args) {
        //50个固定线程池执行1000个任务，但是只有3个信号量
        ExecutorService executorService = Executors.newFixedThreadPool(50);
        for (int i = 0; i < 1000; i++) {
            //1.通过这个方法可以看到同一个时刻只有3个线程在执行
            executorService.submit(new Task());
            //2.通过这个方法可以看到同一个时刻有50个线程在执行
//            executorService.submit(new SemaphoreTask());
        }
        //关闭线程池，养成好习惯
        executorService.shutdown();
    }

    static class SemaphoreTask implements Runnable {
        @Override
        public void run() {
            try {
                semaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "获取到了信号量，即将花2秒钟执行慢服务");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "慢服务执行完毕，释放信号量");
            semaphore.release();
        }
    }

    static class Task implements Runnable {
        @Override
        public void run() {

            System.out.println(Thread.currentThread().getName() + "即将花2秒钟执行慢服务");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "慢服务执行完毕");

        }
    }
}
