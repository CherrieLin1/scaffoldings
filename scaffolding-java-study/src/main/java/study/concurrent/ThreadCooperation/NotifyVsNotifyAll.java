package study.concurrent.ThreadCooperation;

/**
 * @ClassName NotifyVSNotifyAll
 * @Description
 * @Author linshengyan
 * @Date 2022/7/27 10:01 上午
 */
public class NotifyVsNotifyAll {
    /**
     * 出事的状态是flase
     */
    private volatile boolean go = false;

    public static void main(String[] args) throws InterruptedException {
        NotifyVsNotifyAll notify = new NotifyVsNotifyAll();

        //等待的线程
        Runnable wait = new Runnable() {
            @Override
            public void run() {
                try {
                    notify.shouldGo();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "---> is finish");
            }
        };

        //通知的线程
        Runnable notifyMethod = new Runnable() {
            @Override
            public void run() {
                notify.notifyMethod();
                System.out.println(Thread.currentThread().getName() + "---> is finish");
            }
        };


        //new three wait thread
        Thread wait1 = new Thread(wait);
        Thread wait2 = new Thread(wait);
        Thread wait3 = new Thread(wait);

        //new one notify method
        Thread notifyThread = new Thread(notifyMethod);

        wait1.start();
        wait2.start();
        wait3.start();

        //等待4秒，等待线程完全启动完毕
        Thread.sleep(4000);
        notifyThread.start();

    }


    /**
     * 等待执行的线程
     */
    private void shouldGo() throws InterruptedException {
        synchronized (this) {
            //while循环是条件，满足这个条件执行等待，直至条件不满足，才会执行循环外面的动作
            while (go != true) {
                System.out.println(Thread.currentThread().getName() + "---> is waiting for lock");
                wait();
                System.out.println(Thread.currentThread().getName() + "---> is wake up");
                //证明此时go是true,不再满足条件，跳出循环
                System.out.println("go---> " + go);
            }

            System.out.println(Thread.currentThread().getName() + "---->条件满足，开始执行");
            //自己执行完以后，再次将执行的条件置为false
            go = false;
        }
    }


    /**
     * 通知的线程
     */
    private void notifyMethod() {
        synchronized (this) {
            while (go == false) {
                go = true;
                //notify只会唤醒一个线程，具体那个不定
//                notify();
                //notifyAll会唤醒所有等待的线程，具体那个线程真的会获得锁，也不定
                notifyAll();
                System.out.println(Thread.currentThread().getName() + "---> is going to notify other thread");
            }
        }
    }
}
