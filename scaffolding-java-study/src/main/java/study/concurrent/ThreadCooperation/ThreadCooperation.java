package study.concurrent.ThreadCooperation;

/**
 * 线程协作：五种方式
 * Volitile
 * Object的wait()和notify（）方法
 * CountDownLatch
 * ReentrentLock的Condition
 * LockSupport
 * 参考链接:https://blog.csdn.net/jisuanji12306/article/details/86363390
 *
 * 经典面试题目
 * （1）2个猴子轮流拿10个苹果，线程的交替执行
 * （2）银行转帐
 * @author create by lsy on 2021/11/22 2:15 下午
 */
public class ThreadCooperation {
}
