package study.concurrent.ThreadCooperation;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * CyclicBarrier和countLatch的区别是，CyclicBarrier可以复用，countLatch不可以
 *
 * @author create by lsy on 2021/11/22 10:43 上午
 */
public class CyclicBarrierTest {
    /**
     * 原生的cyclicBarrier
     */
    static CyclicBarrier cyclicBarrier1 = new CyclicBarrier(3);

    public static void main(String[] args) {
        //自定义栅栏，当达到一定的条件时打印出特定的语句
        CyclicBarrier cyclicBarrier = new CyclicBarrier(3, new Runnable() {
            @Override
            public void run() {
                System.out.println("集齐三人，开始出发！");
            }
        });

        for (int i = 0; i < 6; i++) {
            //这里也可以用线程池提交，cyclicBarrier可以用自定义的，也可以用原生的
            new Thread(new BarrierTask(i + 1, cyclicBarrier)).start();
        }
    }
}

class BarrierTask implements Runnable {
    int i;
    CyclicBarrier cyclicBarrier;

    public BarrierTask(int i, CyclicBarrier cyclicBarrier) {
        this.i = i;
        this.cyclicBarrier = cyclicBarrier;
    }


    @Override
    public void run() {
        System.out.println("同学" + i + "正在走向大门口集合");
        try {
            Thread.sleep((long) (Math.random() * 10000));
            System.out.println("同学" + i + "到达集合地，等待其他同学的到达");
            cyclicBarrier.await();
            System.out.println("同学" + i + "开始骑车");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }

    }
}
