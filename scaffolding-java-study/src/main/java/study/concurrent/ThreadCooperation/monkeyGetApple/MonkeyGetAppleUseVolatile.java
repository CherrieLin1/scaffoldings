package study.concurrent.ThreadCooperation.monkeyGetApple;

/**
 * 100个苹果，A猴子一次吃3个，B猴子一次吃2个，A先吃，交替吃，如果苹果不够了，那么吃苹果结束
 *
 * @author create by lsy on 2021/11/22 2:20 下午
 */
public class MonkeyGetAppleUseVolatile {
    private static int apple = 100;
    private static volatile boolean flag = true;

    public static void main(String[] args) {
        Thread monkeyA = new Thread(new Runnable() {
            @Override
            public void run() {
                while (apple >= 3) {
                    if (flag) {
                        synchronized (MonkeyGetAppleUseVolatile.class) {
                            apple -= 3;
                            System.out.println("猴子A吃了三个苹果,还剩下" + apple + "个苹果");
                            flag = false;
                        }
                    }
                }
            }
        });

        Thread monkeyB = new Thread(new Runnable() {
            @Override
            public void run() {
                while (apple >= 2) {
                    if (!flag) {
                        synchronized (MonkeyGetAppleUseVolatile.class) {
                            apple -= 2;
                            System.out.println("猴子B吃了两个苹果，还剩下" + apple + "个苹果");
                            flag = true;
                        }
                    }
                }
            }
        });

        monkeyA.start();
        monkeyB.start();

    }


}
