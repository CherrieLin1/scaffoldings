package study.concurrent.ThreadCooperation.monkeyGetApple;

import java.util.concurrent.Semaphore;

/**
 * @ClassName MonkeyGetAppleWithSemaphore
 * @Description
 * @Author linshengyan
 * @Date 2022/7/27 8:17 下午
 */
public class MonkeyGetAppleWithSemaphore {

    public static void main(String[] args) throws InterruptedException {
        Eat eat = new Eat(100);
        new Thread(() -> {
            eat.monkeyAEat(3, "猴子A");
        }).start();

        Thread.sleep(3000);

        new Thread(() -> {
            eat.monkeyBEat(2, "猴子B");
        }).start();

    }


    static class Eat {
        private int apple;
       // 信号量是支持跨线程、跨线程池的，而且并不是哪个线程获得的许可证，就必须由这个线程去释
        private static Semaphore semaphoreA = new Semaphore(1);
        private static Semaphore semaphoreB = new Semaphore(0);

        public static void main(String[] args) {
            try {
                System.out.println("semaphoreA.availablePermits() = " + semaphoreA.availablePermits());
                semaphoreA.acquire();
                System.out.println("semaphoreA.availablePermits() = " + semaphoreA.availablePermits());
                System.out.println("semaphoreA.tryAcquire() = " + semaphoreA.tryAcquire());


                //阻塞等待获取
                semaphoreA.acquire();

                System.out.println("semaphoreA.availablePermits() = " + semaphoreA.availablePermits());
                System.out.println("semaphoreB.availablePermits() = " + semaphoreB.availablePermits());
                semaphoreB.release();
                semaphoreB.release();
                System.out.println("semaphoreB.availablePermits() = " + semaphoreB.availablePermits());

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        public Eat(int apple) {
            this.apple = apple;
        }


        public void monkeyAEat(int eat, String threadName) {
            while (apple >= 3) {
                eat(eat, threadName, semaphoreA, semaphoreB);
            }


        }

        public void monkeyBEat(int eat, String threadName) {
            while (apple >= 2) {
                eat(eat, threadName, semaphoreB, semaphoreA);
            }

        }

        public void eat(int eat, String threadName, Semaphore currentSemaphore, Semaphore nextSemaphore) {
            try {
                currentSemaphore.acquire();
                System.out.println("currentSemaphore.availablePermits() = " + currentSemaphore.availablePermits());
                apple -= eat;
                System.out.println(threadName + "吃了" + eat + "个苹果，还剩余：" + apple + "苹果");

                System.out.println("nextSemaphore.availablePermits() = " + nextSemaphore.availablePermits());
                nextSemaphore.release();
                System.out.println("nextSemaphore.availablePermits() = " + nextSemaphore.availablePermits());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
