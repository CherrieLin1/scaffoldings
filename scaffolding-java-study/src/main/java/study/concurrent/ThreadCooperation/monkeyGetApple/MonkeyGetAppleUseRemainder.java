package study.concurrent.ThreadCooperation.monkeyGetApple;

/**
 * @ClassName MonkeyGetAppleUseRemainder
 * @Description 整数取余实现线程的交替
 * @Author linshengyan
 * @Date 2022/7/29 11:16 上午
 */
public class MonkeyGetAppleUseRemainder {
    private static int step = 0;
    private static int apple = 100;

    /**
     * 吃苹果逻辑
     *
     * @param remainder 余数，根据这个执行对应线程的逻辑
     * @param eatNum    猴子吃苹果的数量
     */
    public static void eat(int remainder, int eatNum) {
        while (apple > 0) {
            if (step % 2 == remainder && apple >= eatNum) {
                apple -= eatNum;
                System.out.println("猴子" + Thread.currentThread().getName() + "--->吃了:" + eatNum + "个苹果，还剩 ：" + apple);
                step++;
            }
        }
        System.out.println(Thread.currentThread().getName() + "---->苹果没有了，停止吃苹果");
    }

    public static void main(String[] args) {
        new Thread(() -> eat(0, 3), "猴子A").start();
        new Thread(() -> eat(1, 2), "猴子B").start();
    }


}
