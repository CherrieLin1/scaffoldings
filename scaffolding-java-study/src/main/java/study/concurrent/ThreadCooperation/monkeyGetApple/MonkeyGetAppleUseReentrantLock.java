package study.concurrent.ThreadCooperation.monkeyGetApple;


import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @ClassName 100个苹果，A猴子一次吃3个，B猴子一次吃2个，A先吃，交替吃，如果苹果不够了，那么吃苹果结束
 * @Description
 * @Author linshengyan
 * @Date 2022/7/27 5:06 下午
 */
public class MonkeyGetAppleUseReentrantLock {

    public static void main(String[] args) throws InterruptedException {


        //注意苹果这个变量千万不要通过方法从外面传递，苹果应该是吃这个方法的一个私有变量
        Eat eat = new Eat(100);

        new Thread(() -> {
            while (eat.apple >= 3) {
                eat.monkeyAEat(3, "猴子-A");
            }
        }).start();

        Thread.sleep(2000);


        new Thread(() -> {
            while (eat.apple >= 2) {
                eat.monkeyBEat(2, "猴子-B");
            }
        }).start();
    }


    static class Eat {
        //1.A吃 2.B吃
        private int num = 1;
        private int apple = 0;
        private ReentrantLock lock = new ReentrantLock();
        private Condition a = lock.newCondition();
        private Condition b = lock.newCondition();

        public Eat(int apple) {
            this.apple = apple;
        }

        public void monkeyAEat(int eat, String threadName) {
            lock.lock();
            try {
                if (num != 1) {
                    a.await();
                }
                apple -= 3;
                System.out.println("猴子A吃了三个苹果，剩余：" + apple + ",猴子A暂停吃苹果，通知猴子B吃苹果");
                num = 2;
                b.signalAll();
            } catch (Exception e) {
            } finally {
                lock.unlock();
            }

        }

        public void monkeyBEat(int eat, String threadName) {
            lock.lock();
            try {
                if (num != 2) {
                    b.await();
                }
                apple -= 2;
                System.out.println("猴子B吃了两个苹果，剩余：" + apple + ",猴子B暂停吃苹果，通知猴子A吃苹果");
                num = 1;
                a.signalAll();
            } catch (Exception e) {
            } finally {
                lock.unlock();
            }
        }

    }

}
