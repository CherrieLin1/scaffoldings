package study.concurrent.ThreadCooperation.monkeyGetApple;

import java.util.concurrent.locks.LockSupport;

/**
 * @ClassName MonkeyGetAppleUseLockSupport
 * @Description
 * @Author linshengyan
 * @Date 2022/7/29 10:25 上午
 */
public class MonkeyGetAppleUseLockSupport {
    private static int apple = 100;
    private static final int MONKEY_A_NUM = 3;
    private static final int MONKEY_B_NUM = 2;
    static Thread monkeyA;
    static Thread monkeyB;

    public static void eat() {
        while (apple >= 0) {
            if (monkeyA == Thread.currentThread() && apple >= MONKEY_A_NUM) {
                apple -= MONKEY_A_NUM;
                System.out.println("猴子A吃了三个苹果，还剩下：" + apple + "个苹果");
                //放行猴子B
                LockSupport.unpark(monkeyB);
                //阻塞猴子A，直到被猴子B放行，当前猴子A线程阻塞了，直到被重新唤醒
                LockSupport.park();
            } else if (monkeyB == Thread.currentThread() && apple >= MONKEY_B_NUM) {
                //阻塞猴子B，直到被猴子A放行
                LockSupport.park();
                //走到这里说明猴子B已经被猴子A放行，且目前猴子B处于阻塞等待的状态，轮到猴子B开始吃苹果
                apple -= MONKEY_B_NUM;
                System.out.println("猴子B吃了两个苹果，还剩下：" + apple + "个苹果");
                //猴子B吃完了，放行猴子A来吃
                LockSupport.unpark(monkeyA);
            } else {
                System.out.println(Thread.currentThread().getName() + "苹果数量不够了，停止吃苹果");
                //数量不够了，两个线程都解锁，防止线程死锁
                LockSupport.unpark(monkeyA);
                LockSupport.unpark(monkeyB);
                break;
            }
        }

    }


    public static void main(String[] args) throws InterruptedException {
        monkeyA = new Thread(() -> eat(), "monkey-A");
        monkeyB = new Thread(() -> eat(), "monkey-B");
        monkeyB.start();
        //为了100%保证程序不会出问题，也就是猴子A先吃，猴子B已经在等待的状态了，可以先让猴子B等。
        //不加这个时间，或者让A先启动，猴子AB可能都会一直会处于LockSupport.park()状态而陷入互相等待的状态
        Thread.sleep(1000);
        monkeyA.start();


    }
}
