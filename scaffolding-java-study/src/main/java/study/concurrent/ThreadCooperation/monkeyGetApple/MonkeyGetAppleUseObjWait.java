package study.concurrent.ThreadCooperation.monkeyGetApple;

/**
 * @ClassName MonkeyGetAppleUseObjWait
 * @Description
 * @Author linshengyan
 * @Date 2022/7/29 11:15 上午
 */
public class MonkeyGetAppleUseObjWait {
    private static int apple = 100;
    private static Object lock = new Object();

    public static void main(String[] args) throws InterruptedException {
        Runnable monkeyA = () -> {
            synchronized (lock) {
                while (apple >= 3) {
                    apple -= 3;
                    System.out.println("猴子A吃了三个苹果，还剩：" + apple);
                    try {
                        lock.notifyAll();
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                //保证所有的线程正常退出
                lock.notifyAll();
                System.out.println(Thread.currentThread().getName() + "--->苹果不够了，停止吃苹果");
            }
        };

        Runnable monkeyB = () -> {
            synchronized (lock) {
                while (apple >= 2) {
                    apple -= 2;
                    System.out.println("猴子B吃了两个苹果，还剩：" + apple);
                    try {
                        lock.notifyAll();
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                //保证所有的线程正常退出
                lock.notifyAll();
                System.out.println(Thread.currentThread().getName() + "--->苹果不够了，停止吃苹果");
            }
        };

        //谁先吃苹果，谁就先启动
        new Thread(monkeyA, "猴子A").start();

        new Thread(monkeyB, "猴子B").start();


    }

}
