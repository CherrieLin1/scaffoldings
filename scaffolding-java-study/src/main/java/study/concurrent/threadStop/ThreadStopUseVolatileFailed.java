package study.concurrent.threadStop;

/**
 * @ClassName 用volatile关键字停止线程不合理，因为休眠期间可能无法感受到中断信号
 * @Description
 * @Author linshengyan
 * @Date 2022/7/26 6:05 下午
 */
public class ThreadStopUseVolatileFailed implements Runnable {
    public volatile boolean flag = false;
    Storage storage = new Storage();

    @Override
    public void run() {
        while (!flag) {
            storage.put();//如果这里阻塞/休眠，while并不能立刻感觉到中断信号，直至下一次循环
            System.out.println("storage.getNum() = " + storage.getNum());
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ThreadStopUseVolatileFailed f = new ThreadStopUseVolatileFailed();
        Thread thread = new Thread(f);
        thread.start();


        Thread.sleep(1000);
        f.flag = true;
        System.out.println("done");

    }
}


class Storage {
    int num;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void put() {
        if (num < 200) {
            num++;
            System.out.println("库存添加1");
        }
        try {
            Thread.sleep(50000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
