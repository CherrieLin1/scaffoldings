package study.concurrent.threadStop;

/**
 * @author create by lsy on 2021/11/19 10:49 上午
 */
public class ThreadStopUseInterruptWhenSleep implements Runnable {
    private static final int NUM = 1000;

    @Override
    public void run() {
        int count = 0;
        while (!Thread.currentThread().isInterrupted() && count < NUM) {
            try {
                System.out.println("开始休眠～");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("休眠期间感受到中断，会将重新将线程中断状态标记为false");
                //false
                System.out.println("Thread.currentThread().isInterrupted() = " + Thread.currentThread().isInterrupted());
                //标记为中断
                Thread.currentThread().interrupt();
                //true
                System.out.println("Thread.currentThread().isInterrupted() = " + Thread.currentThread().isInterrupted());

            }
            System.out.println("count = " + count);
            count++;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new ThreadStopUseInterruptWhenSleep());
        thread.start();
        Thread.sleep(5);

        //false
        System.out.println("1. thread.isInterrupted() = " + thread.isInterrupted());

        //中断线程，此时状态为true
        thread.interrupt();

        //修改完立刻获取，睡眠线程被打断后重新将线程状态标记为未中断，的大概率异常这时候还没有还没有将线程状态改回来
        System.out.println("2. thread.isInterrupted() = " + thread.isInterrupted());

        //一定是false,休眠5毫秒再去获取状态，等待线程将状态改回来false,因为休眠的线程依然会感受到interrupt信号，抛出异常且会重新将线程的中断状态重新置为false
        Thread.sleep(1000);
        //TERMINATED
        System.out.println("thread.getState() = " + thread.getState());
        //终止的线程状态肯定是false
        System.out.println("3. thread.isInterrupted() = " + thread.isInterrupted());
        thread.suspend();
        thread.stop();
        thread.resume();

    }


}
