package study.concurrent.threadStop;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.springframework.util.StopWatch;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 线程池只等待5秒，如果5面还没有执行完成的，直接关闭线程池
 * test isShutNow 和 awaitTermination
 *
 * @author create by lsy on 2021/11/1 2:26 下午
 */
public class ThreadPoolStop02 {
    public static void main(String[] args) throws InterruptedException {
        //线程的名字
        ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat("test-pool-%s").build();
        //线程池
        ExecutorService executorService = new ThreadPoolExecutor(10, 10, 10, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(10), threadFactory);

        for (int i = 0; i < 10; i++) {
            executorService.submit(new StopTask2(i));
        }

        //调用shutdown关闭线程，会等待所有任务执行完
        executorService.shutdown();
        System.out.println("main thread ---- 线程池调用shutdown方法");


        //这里用stopWatch证明以下awaitTermination是否是阻塞等待10秒，结果证明确实是这样
        StopWatch stopWatch= new StopWatch("test awaitTermination");
        stopWatch.start("~start");
        //该方法会阻塞等待一定的时间，例如10秒钟，但是线程依旧没有执行完，会执行if里面的方法
        if (!executorService.awaitTermination(10, TimeUnit.SECONDS)) {
            System.out.println("10秒时间已经过去了，线程依旧未执行完成");
            //这时候强制关闭线程，会有任务会强制中断
            executorService.shutdownNow();
            System.out.println("强制结束了线程");
        }
        stopWatch.stop();
        System.out.println("stopWatch.prettyPrint() = " + stopWatch.prettyPrint());



        //看线程是否真的停止了
        if (executorService.isShutdown()) {
            System.out.println("线程池被关闭了");
        }

    }
}


class StopTask2 implements Runnable {

    private int i;

    public StopTask2(int i) {
        this.i = i;
    }


    @Override
    public void run() {
        System.out.println("child thread ----- " + Thread.currentThread().getName() + "开始执行线程");
        try {
            System.out.println("child thread ----- " + Thread.currentThread().getName() + "线程正在工作，工作的时间为：" +  10 * i + "秒");
            Thread.sleep(10000 * i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("child thread ----- " + Thread.currentThread().getName() + "线程执行完成");
    }
}







