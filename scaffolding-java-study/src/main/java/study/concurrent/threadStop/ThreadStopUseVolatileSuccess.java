package study.concurrent.threadStop;

/**
 * @ClassName ThreadStopUseVolatile
 * @Description
 * @Author linshengyan
 * @Date 2022/7/26 5:50 下午
 */
public class ThreadStopUseVolatileSuccess implements Runnable {
    private volatile boolean canceled = false;

    @Override
    public void run() {
        int num = 0;
        while (!canceled && num < 1000000000) {
            if (num % 10 == 0) {
                System.out.println(num + "是10的倍数");
            }
            num++;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ThreadStopUseVolatileSuccess r = new ThreadStopUseVolatileSuccess();
        Thread thread = new Thread(r);
        thread.start();

        //线程启动后一分钟后将标志位置为true，当打印出关闭线程的字体后，自线程几乎是同时停止的
        Thread.sleep(1000);
        System.out.println("关闭线程");
        r.canceled = true;


    }
}
