package study.concurrent.threadStop;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 创建一个线程，等待所有的任务执行完毕关闭线程池
 * shutdown : JDK会等待所有任务执行完关闭线程; shutdown调用前:isShutdown = false ;  shutdown调用后:isShutdown = true
 * isTerminated ： 判断线程是否已经停止
 *
 * @author create by lsy on 2021/11/1 2:37 下午
 */
public class ThreadPoolStop01 {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("main thread ---- main线程开始执行");
        //绝大多数使用  Google guava 工具类提供的ThreadFactoryBuild
        ThreadFactory guavaThreadFactory = new ThreadFactoryBuilder().setNameFormat("test-pool-%s").build();
        ExecutorService exec = new ThreadPoolExecutor(10, 10, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(10), guavaThreadFactory);
        for (int i = 0; i < 10; i++) {
            //executorService.execute 无返回值，直接执行，因此不能阻塞线程，并且在执行异常时会抛出异常
            //executorService.submit 有三种类型，接返回future;可以阻塞获取线程执行结果;在执行异常时会被异常处理吃掉，不会抛出异常
            exec.execute(new StopTask1(i));
        }

        //调用shutDown方法之前，isShutDown返回false
        System.out.println("main thread ---- 线程池调用shutdown方法之前，exec.isShutdown() = " + exec.isShutdown());

        //关闭线程池，shutdown会等待线程池的任务执行完毕再关闭线程池
        exec.shutdown();
        System.out.println("main thread ---- 线程池调用shutdown方法");

        //只要调用了shutDown方法，isShutDown便会返回true
        System.out.println("main thread ---- 线程池调用shutdown方法之后，exec.isShutdown() = " + exec.isShutdown());

        try {
            exec.execute(new StopTask1(11111));
        } catch (Exception e) {
            System.out.println("main thread ---- 此时线程池的isShutDown状态为：" + exec.isShutdown() + ",已经调用过shutDown方法,此时继续提交任务会被拒绝,：错误信息为" + e.getMessage());
        }

        //用一个死循环来控制一下，3秒判断一次，线程池有没有执行完，执行完了关闭线程池
        while (true) {
            System.out.println("main thread ---- 判读线程池是否执行完毕");
            //当线程池调用shouDown，所有的任务执行完成后，或者线程池调用shutDownNow后，线程池关闭，这时候isTerminated会返回true
            if (exec.isTerminated()) {
                System.out.println("main thread ---- 线程池任务执行完毕");
                return;
            } else {
                System.out.println("main thread ---- 未执行完成，等待3秒");
                Thread.sleep(3000);
            }
        }
    }
}

 class StopTask1 implements Runnable {

    private int i;

    public StopTask1(int i) {
        this.i = i;
    }


    @Override
    public void run() {
        System.out.println("child thread ----- " + Thread.currentThread().getName() + "开始执行线程");
        try {
            System.out.println("child thread ----- " + Thread.currentThread().getName() + "线程正在工作，工作的时间为：" +  10 * i + "秒");
            Thread.sleep(10000 * i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("child thread ----- " + Thread.currentThread().getName() + "线程执行完成");
    }
}



