package study.concurrent.threadStop;

/**
 * 线程的中断
 * @author create by lsy on 2021/11/19 10:40 上午
 */
public class ThreadStopUseInterrupt implements Runnable{
    private static final int NUM= 1000;
    @Override
    public void run() {
        int count =0;
        while (!Thread.currentThread().isInterrupted() && count <NUM){
            System.out.println("count = " + count);
            count++;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new ThreadStopUseInterrupt());
        thread.start();
        Thread.sleep(5);
        System.out.println("thread.isInterrupted() = " + thread.isInterrupted());
        thread.interrupt();
        System.out.println("thread.isInterrupted() = " + thread.isInterrupted());
    }
}
