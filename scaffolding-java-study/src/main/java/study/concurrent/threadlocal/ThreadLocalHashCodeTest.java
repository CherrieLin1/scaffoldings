package study.concurrent.threadlocal;

/**
 * @ClassName ThreadLocalHashCodetest
 * @Description
 * @Author linshengyan
 * @Date 2022/8/5 9:34 上午
 */
public class ThreadLocalHashCodeTest {
    public static void main(String[] args) {
        int hashcode = -234553335;
        int length = hashcode & (16 - 1);
        System.out.println("length = " + length);

    }
}
