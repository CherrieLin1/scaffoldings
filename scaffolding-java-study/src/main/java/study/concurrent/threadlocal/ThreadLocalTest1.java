package study.concurrent.threadlocal;

/**
 * @ClassName ThreadLocalTest1
 * @Description
 * @Author linshengyan
 * @Date 2022/8/4 7:35 下午
 */
public class ThreadLocalTest1 {

    /**
     * 一个线程可以有多个ThreadLocal
     */
    private static ThreadLocal<Object> localVar1 = new ThreadLocal<>();
    private static ThreadLocal<Object> localVar2 = new ThreadLocal<>();

    static void print(String str) {
        //打印当前线程中本地内存中本地变量的值
        System.out.println(str + " :" + localVar1.get());

        //打印当前线程中本地内存中本地变量的值
        System.out.println(str + " :" + localVar2.get());

        //清除本地内存中的本地变量
        localVar1.remove();
        localVar2.remove();
    }

    public static void main(String[] args) throws InterruptedException {
        Runnable r1 = () -> {
            ThreadLocalTest1.localVar1.set("t1-local1");
            ThreadLocalTest1.localVar2.set("t1_local2");
            print(Thread.currentThread().getName());
            //打印本地变量
            System.out.println("after remove : " + localVar1.get());
            System.out.println("after remove : " + localVar2.get());
        };

        Runnable r2 = () -> {
            ThreadLocalTest1.localVar1.set("t2-local1");
            ThreadLocalTest1.localVar2.set("t2_local2");
            print(Thread.currentThread().getName());
            //打印本地变量
            System.out.println("after remove : " + localVar1.get());
            System.out.println("after remove : " + localVar2.get());
        };

        new Thread(r1,"t1").start();
        new Thread(r2,"t2").start();
    }
}
