package study.concurrent.producerAndConsumer;

import lombok.SneakyThrows;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @ClassName ObjectWaitTest
 * @Description
 * @Author linshengyan
 * @Date 2022/7/26 9:10 下午
 */
public class ObjectWaitTest {
    public static void main(String[] args) throws InterruptedException {
        BlockingQueue blockingQueue = new BlockingQueue();

        new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    String take = blockingQueue.take();
                    System.out.println("取出： " + take);
                }
            }
        }).start();

        Thread.sleep(1000);


        new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                for (int i = 1; i <= 10; i++) {
                    System.out.println("放入第" + i + "个数据");
                    blockingQueue.give("第" + i + "个数据");
                    Thread.sleep(1000);
                }
            }
        }).start();
    }
}

class BlockingQueue {
    Queue<String> queue = new LinkedList<String>();

    public void give(String data) {
        //这里的this就是 BlockingQueue对象。notify()，wait必须是对同一个对象
        synchronized (this) {
            queue.add(data);
            notify();
        }

    }

    public String take() throws InterruptedException {
        //这里的this就是 BlockingQueue对象，notify()，wait必须是对同一个对象
        synchronized (this) {
            while (queue.isEmpty()) {
                System.out.println("队列里没有数据，等待数据放入");
                wait();
                System.out.println("对列里有数据了，取第一个数据");
            }
            return queue.remove();

        }
    }

}
