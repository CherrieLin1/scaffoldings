package study.concurrent.start;

import lombok.SneakyThrows;

/**
 * 龟兔赛跑，只有一个胜利者，先跑到100的胜利
 * @author create by lsy on 2021/11/18 3:48 下午
 */
public class Race implements Runnable {
    /**
     * 目前距离
     */
    private static final int target = 100;

    /**
     * 胜利者
     */
    private String winner;

    @SneakyThrows
    @Override
    public void run() {
        int step = 0;
        for (int i = 1; i <= target; i++) {
            step++;
            //如果是兔子，跑了50步以后休眠100毫秒
            if (Thread.currentThread().getName().equals("兔子") && i % 50 == 0) {
                Thread.sleep(100);
            }
            System.out.println(Thread.currentThread().getName() + "跑了：" + step + " 步");
            if (gameOver(step)) {
                break;
            }
        }
    }


    /**
     * 根据已经跑完的步数，判断当前的比赛有没有结束
     *
     * @param step
     * @return
     */
    public boolean gameOver(int step) {
        if (winner != null) {
            return true;
        }
        if (step >= 100) {
            winner = Thread.currentThread().getName();
            System.out.println("获胜者是" + winner);
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Race race = new Race();
        new Thread(race,"兔子").start();
        new Thread(race,"乌龟").start();
    }

}
