package study.concurrent.start;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 测试集合不安全的问题
 *
 * @author create by lsy on 2021/11/18 4:05 下午
 */
public class NotSafeThread {
    static ExecutorService executorService = Executors.newFixedThreadPool(1000);
    static List<Integer> list = new ArrayList<>();
    static ConcurrentLinkedQueue<Integer> concurrentLinkedQueue = new ConcurrentLinkedQueue<>();


    public static void main(String[] args) {
        //提交10万个任务
        for (int i = 0; i < 100000; i++) {
            executorService.submit(new Task(i));
            System.out.println("成功提交第" + i + "个任务");
        }
        //关闭线程池，线程只要调用了shutdown，isShutDown就会返回true
        executorService.shutdown();

        while (true) {
            //判断任务有没有执行完
            if (executorService.isTerminated()){
                //判断线程池有没有关闭，这个放在第二个判断，因为有时候即便是线程池返回已经关闭了，但是还有线程没有执行完
                    System.out.println("线程池已经被成功关闭");
                    System.out.println("list.size() = " + list.size());
                    System.out.println("concurrentLinkedQueue.size() = " + concurrentLinkedQueue.size());
                    break;
            }
             else {
                System.out.println("任务未执行完，休眠1秒，再判断");
                try {
                    Thread.sleep(1000);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }


    }

    static class Task implements Runnable {
        private int i;

        public Task(int i) {
            this.i = i;
        }

        @Override
        public void run() {
            list.add(i);
            concurrentLinkedQueue.add(i);
            System.out.println("当前任务:" + Thread.currentThread().getName() + "，编号为：" + i + "，被成功执行");


        }
    }
}
