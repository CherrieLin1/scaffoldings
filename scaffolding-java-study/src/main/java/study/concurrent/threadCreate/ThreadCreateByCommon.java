package study.concurrent.threadCreate;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @ClassName CreateThread
 * @Description 普通的方式创建多线程
 * @Author linshengyan
 * @Date 2022/4/7 5:40 下午
 */
public class ThreadCreateByCommon {
    /**
     * 1.继承Thread
     * 2.实现Runnable接口
     * 3.实现Callable接口
     * 4.使用线程池 --- 这个单独开一个文件写，还有很多种方式
     * 无论那个方式，最终的结果结果都是thread.start()方式启动，这个可以看源码验证
     */

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //1.通过继承 Thread 类实现多线程
        NewThreadByThread thread0 = new NewThreadByThread();
        thread0.start();
        NewThreadByThread thread1 = new NewThreadByThread();
        thread1.start();



        //2.通过继承 Runnable 接口实现多线程
        NewThreadByRunnable newThreadByRunnable = new NewThreadByRunnable();
        Thread thread2 = new Thread(newThreadByRunnable, "runnable-1");
        Thread thread22 = new Thread(newThreadByRunnable, "runnable-2");
        thread2.start();
        thread22.start();


        //3.通过继承 Callable 接口实现多线程
        FutureTask<String> futureTask = new FutureTask<>(new NewThreadByCallable());
        Thread thread3 = new Thread(futureTask, "futureTask-1");
        thread3.start();
        System.out.println(futureTask.get());

        FutureTask<String> futureTask1 = new FutureTask<>(new NewThreadByCallable());
        Thread thread33 = new Thread(futureTask1, "futureTask-2");
        thread33.start();
        System.out.println(futureTask1.get());
    }
}

class NewThreadByThread extends Thread {
    @Override
    public void run() {
        System.out.println("继承 【Thread】 类实现线程,当前线程的名字：" + Thread.currentThread().getName());
    }
}

class NewThreadByRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println("实现 【Runnable】 接口实现线程,当前线程的名字：" + Thread.currentThread().getName());
    }
}

class NewThreadByCallable implements Callable<String> {

    @Override
    public String call() throws Exception {
        return "实现 【callable】 接口实现线程,当前线程的名字：" + Thread.currentThread().getName();
    }
}
