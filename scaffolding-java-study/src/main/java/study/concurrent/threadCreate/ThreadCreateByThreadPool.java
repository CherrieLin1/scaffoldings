package study.concurrent.threadCreate;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.SneakyThrows;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName CreateByThreadPool
 * @Description 利用线程池创建多线程
 * @Author linshengyan
 * @Date 2022/4/7 7:25 下午
 */
public class ThreadCreateByThreadPool {
    /*
     * 1.利用JDK的API创建对应类型的线程池 -- 不推荐
     * 2.利用guava cache创建对应类型的线程池 --推荐
     * 3.利用Spring的ThreadPoolTaskExecutor类提供的Api创建线程池 -- 用得少
     *   以上是三种比较常见的创建线程池的方法
     * */


    public static void main(String[] args) {
        //1.测试JdK线程池
        createThreadPoolByJDK();

        //2.测试guava线程池
        createThreadPoolByGuava();

        //3.测试spring 自带的创建线程的工具类
        createThreadPoolBySpring();

        //以下方法只是为了测试线程池提交FutureTask的方法
        testFutureTask();

    }




    /**
     * 1.利用JDK的API创建对应类型的线程池
     */
    public static void createThreadPoolByJDK() {
        //通过Executors创建线程池，标红提示尽量手动创建线程池，避免资源耗尽
        ExecutorService pool = Executors.newFixedThreadPool(10);
        //100个任务提交给固定任务线程池
        for (int i = 0; i < 100; i++) {
            pool.execute(new Task());
        }
        //关闭线程池
        pool.shutdown();
    }





    /**
     * 2.测试guava线程池
     */
    public static void createThreadPoolByGuava() {
        //1.用guava api创建线程工厂
        ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat("thread-task-%d").build();

        //2.设定线程池的拒绝策略,ThreadPoolExecutor类里面有四种拒绝策略，都是以静态内部类的方式存在于ThreadPoolExecutor类中，可以修改线程池的拒绝策略，查看线程提交的效果
        //这是最完善的一种处理方式，会把线程交给任务的提交线程来执行
        ThreadPoolExecutor.CallerRunsPolicy callerRunsPolicy = new ThreadPoolExecutor.CallerRunsPolicy();
        //会抛出RejectedExecutionException异常，可以根据业务手动选择重试或者直接丢弃，抛出异常了以后，线程池就不会再继续提交新任务了，即便已经提交的任务中有已经执行完的线程，线程空闲了
        ThreadPoolExecutor.AbortPolicy abortPolicy = new ThreadPoolExecutor.AbortPolicy();
        //直接丢弃新提交的任务，不会有任何提示，有数据丢失的风险
        ThreadPoolExecutor.DiscardPolicy discardPolicy = new ThreadPoolExecutor.DiscardPolicy();
        //直接丢弃队列中已提交未执行，等待时间最久的任务，有数据丢失的风险
        ThreadPoolExecutor.DiscardOldestPolicy discardOldestPolicy = new ThreadPoolExecutor.DiscardOldestPolicy();

        //3.利用JDK的ThreadPoolExecutor创建线程池,有以下两种方式：建议用ThreadPoolExecutor
        //可以看出以下new出来的对象是一个父子的关系，ThreadPoolExecutor是子类，功能最为强大，可以获取当前线程池的核心线程数，队列信息，等等等
//        ExecutorService executor = new ThreadPoolExecutor(3, 4, 2, TimeUnit.MINUTES, new LinkedBlockingQueue<>(20), threadFactory, discardPolicy);
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(3, 4, 2, TimeUnit.MINUTES, new LinkedBlockingQueue<>(20), threadFactory, callerRunsPolicy);

        //4.100个任务提交给固定任务线程池，核心线程数只有3，最大线程数4，队列长度只有20，这里要注意线程池处理不过来，会抛出异常，根据不同的拒绝策略线程池会有不同的处理方案
        for (int i = 1; i < 101; i++) {
            System.out.println("准备提交第" + i + "个任务，" +
                    "当前核心线程数为:" + threadPoolExecutor.getCorePoolSize() +
                    "，当前活动线程数为:" + threadPoolExecutor.getActiveCount() +
                    "，当最大线程数为:" + threadPoolExecutor.getMaximumPoolSize() +
                    "，当前线程池活跃度数为:" + divide(threadPoolExecutor.getActiveCount(), threadPoolExecutor.getMaximumPoolSize()) +
                    "，当前任务完成数为:" + threadPoolExecutor.getCompletedTaskCount() +
                    "，当前队列大小为:" + threadPoolExecutor.getQueue().size() + threadPoolExecutor.getQueue().remainingCapacity() +
                    "，当前队列剩余大小为:" + (threadPoolExecutor.getQueue()).remainingCapacity() +
                    "，当前队列使用度为:" + divide(threadPoolExecutor.getQueue().size(), threadPoolExecutor.getQueue().size() + threadPoolExecutor.getQueue().remainingCapacity()) +
                    "，当前排队线程数为:" + threadPoolExecutor.getQueue().size())
            ;
            threadPoolExecutor.execute(new Task());
        }

        //5.关闭线程池
        threadPoolExecutor.shutdown();
    }



    /**
     * 3.测试spring 自带的创建线程的工具类
     */
    public static void createThreadPoolBySpring() {
        //1.常见线程池，除此之外还可以通过xml配置方式创建，或者java的配置类+@Bean方式创建
        ThreadPoolTaskExecutor threadPoolExecutor = new ThreadPoolTaskExecutor();
        threadPoolExecutor.setCorePoolSize(2);
        threadPoolExecutor.setMaxPoolSize(4);
        threadPoolExecutor.setKeepAliveSeconds(120);
        threadPoolExecutor.setQueueCapacity(20);
        threadPoolExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        //‼️‼️重要‼️ JDK的ThreadPoolExecutor类的shutDown方法默认是等待所有的任务【包括正在执行，或者再阻塞队列中的】执行完再关闭线程池，spring的默认不执行完
        //所以此处不设置为true，很有可能任务就中断了，导致整个线程池异常退出
        //详细解释参考：https://www.136.la/nginx/show-184603.html
        threadPoolExecutor.setWaitForTasksToCompleteOnShutdown(true);
        //初始化不能丢
        threadPoolExecutor.initialize();

        //2.100个任务提交给固定任务线程池，核心线程数只有3，最大线程数4，队列长度只有20，这里要注意线程池处理不过来，会抛出异常，根据不同的拒绝策略线程池会有不同的处理方案
        for (int i = 1; i < 101; i++) {
            System.out.println("准备提交第" + i + "个任务，" +
                            "当前核心线程数为:" + threadPoolExecutor.getCorePoolSize() +
                            "，当前活动线程数为:" + threadPoolExecutor.getActiveCount() +
                            "，当最大线程数为:" + threadPoolExecutor.getMaxPoolSize() +
                            "，当前线程池活跃度数为:" + divide(threadPoolExecutor.getActiveCount(), threadPoolExecutor.getMaxPoolSize()));
            threadPoolExecutor.execute(new Task());
        }

        //3.关闭线程池
        threadPoolExecutor.shutdown();
    }


    /**
     * 线程池执行有返回值的任务
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static void testFutureTask() {
        ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat("lin-test-%s").build();
        ExecutorService executorService = new ThreadPoolExecutor(3, 500, 2, TimeUnit.MINUTES, new LinkedBlockingQueue(2000), threadFactory);


        Future<String> submit = executorService.submit(new FutureTaskClass());
        try {
            System.out.println("submit.get() = " + submit.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        //关闭线程池，平时测试时要养成好习惯，项目中一般都是常驻
        executorService.shutdown();
    }




    /**
     * 计算比例
     *
     * @param num1
     * @param num2
     * @return
     */
    private static String divide(Integer num1, Integer num2) {
        return String.format("%1.2f%%", Double.parseDouble(num1.toString()) / Double.parseDouble(num2.toString()) * 100);
    }

}



class Task implements Runnable {

    @SneakyThrows
    @Override
    public void run() {
        int timeCount = new Random().nextInt(1000);
        System.out.println("线程：" + Thread.currentThread().getName() + "，开始执行一个耗时为：" + timeCount + "毫秒的任务。");
        //睡的时间为毫秒
        Thread.sleep(timeCount);
        System.out.println("线程：" + Thread.currentThread().getName() + " - 任务执行完成。");
    }
}


class FutureTaskClass implements Callable<String> {
    @Override
    public String call() throws Exception {
        //模拟业务处理时长
        double random = Math.random() * 100000;
        long time = new Double(random).longValue();
        System.out.println("time = " + time);
        Thread.sleep(time);
        return Thread.currentThread().getName();
    }
}
