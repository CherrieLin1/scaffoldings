package study.concurrent.ConCharact;

import java.util.ArrayList;

/**
 * @ClassName Test
 * 原子性 n++多指令，由于线程切换，导致问题
 * @Description while循环迟迟不退出
 * @Author linshengyan
 * @Date 2022/7/26 7:19 下午
 */
public class TestAotomic {
    static Integer num = 0;

    public static void main(String[] args) throws InterruptedException {
        ArrayList<Thread> list = new ArrayList<>(2000);

        for (int i = 0; i < 2000; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    num++;
                }
            });
            thread.start();
            list.add(thread);

        }

        for (Thread thread : list) {
            thread.join();
        }

        System.out.println(num);


    }

}