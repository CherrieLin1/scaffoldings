package study.concurrent.ConCharact;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;

/**
 * @ClassName Testorderly
 * @Description 顺序性  原子性涉及了一个操作多条指令，顺序性是由于指令重排，会带来代码执行顺序的问题
 * @Author linshengyan
 * @Date 2022/7/26 7:31 下午
 */
public class TestOrderly {
    static int x;
    static int y;

    public static void main(String[] args) throws InterruptedException {
        Set<String> hashSet = new HashSet<String>();
        Map<String, Integer> concurrentHashMap = new ConcurrentHashMap<>();

        for (int i = 0; i < 10000000; i++) {
            x = 0;
            y = 0;
            //清除之前记录的结果
            hashSet.clear();
            concurrentHashMap.clear();

            Thread t1 = new Thread(() -> {
                int v1 = y;// Step1
                x = 1; //Step2
                concurrentHashMap.put("v1", v1);//Step3
            }, "A");

            Thread t2 = new Thread(() -> {
                int v2 = x;//Step4
                y = 1;//Step5
                concurrentHashMap.put("v2", v2);//Step6
            }, "B");

            t1.start();
            t2.start();
            //等待线程 t1 t2 执行完成
            t1.join();
            t2.join();


            // if(concurrentHashMap.get("v1") == 1 && concurrentHashMap.get("v2") == 1){
            hashSet.add("(v1=" + concurrentHashMap.get("v1") + ",v2=" + concurrentHashMap.get("v2") + ")");
            System.out.println(hashSet);
            // }

        }
    }
}
