package study.concurrent.ConCharact;

/**
 * @ClassName 可见性  工作内存和主内存的关系
 * @Description while循环迟迟不退出
 * @Author linshengyan
 * @Date 2022/7/26 7:19 下午
 */
public class TestVisible {
    //    static  Boolean flag = false;
    static volatile Boolean flag = false;

    public static void main(String[] args) throws InterruptedException {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (!flag){
                    System.out.println("======循环中======");
                };
            }
        }, "A").start();

        Thread.sleep(500);
        flag = true;
    }
}
