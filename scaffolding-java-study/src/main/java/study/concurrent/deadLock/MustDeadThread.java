package study.concurrent.deadLock;

import lombok.SneakyThrows;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.concurrent.locks.ReentrantReadWriteLock;


/**
 * @ClassName 必然死锁线程
 * @Description
 * @Author linshengyan
 * @Date 2022/8/3 7:43 下午
 */
public class MustDeadThread implements Runnable {
    private int flag;
    /**
     * 这两把锁一定是常量池里面的锁，否则不会形成死锁
     */
    private static Object lock1 = new Object();
    private static Object lock2 = new Object();

    public MustDeadThread(int flag) {
        this.flag = flag;
    }

    @SneakyThrows
    @Override
    public void run() {
        if (flag == 1) {

            synchronized (lock1) {
                System.out.println("获取了lock1锁");
                Thread.sleep(500L);
                synchronized (lock2) {
                    System.out.println("获取了lock2锁");
                }
            }

        } else {
            synchronized (lock2) {
                System.out.println("获取了lock2锁");
                Thread.sleep(500L);
                synchronized (lock1) {
                    System.out.println("获取了lock2锁");
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        new ReentrantReadWriteLock();

        Runnable thread1 = new MustDeadThread(1);
        Runnable thread2 = new MustDeadThread(2);
        new Thread(thread1, "线程1").start();
        new Thread(thread2, "线程2").start();
        thread1.wait();

        Thread.sleep(1000);

        //利用ManagementFactory查看死锁的线程
        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        long[] deadlockedThreads = threadMXBean.findDeadlockedThreads();
        if (null != deadlockedThreads && deadlockedThreads.length > 0) {
            for (int i = 0; i < deadlockedThreads.length; i++) {
                ThreadInfo threadInfo = threadMXBean.getThreadInfo(deadlockedThreads[i]);
                System.out.println("线程名为：" + threadInfo.getThreadName() + "，线程ID为：" + threadInfo.getThreadId() + "线程发生了死锁，需要的锁资源被" + threadInfo.getLockOwnerName() + "所持有。");
            }
        }

    }
}
