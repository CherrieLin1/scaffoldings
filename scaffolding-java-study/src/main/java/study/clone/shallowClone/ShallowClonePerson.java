package study.clone.shallowClone;

import lombok.AllArgsConstructor;
import lombok.Data;
import study.clone.Address;

/**
 * @ClassName Person
 * @Description
 * @Author linshengyan
 * @Date 2022/3/28 6:18 下午
 */
@AllArgsConstructor
@Data
public class ShallowClonePerson implements Cloneable {
    public static void main(String[] args) {
        ShallowClonePerson person = new ShallowClonePerson(new Address("南京"));
        ShallowClonePerson clone = person.clone();
        //因为是浅克隆，所以person里面的address其实是一样的
        System.out.println("(clone.getAddress() == person.getAddress()) = " + (clone.getAddress() == person.getAddress()));
    }


    private Address address;

    @Override
    public ShallowClonePerson clone() {
        try {
            ShallowClonePerson person = (ShallowClonePerson) super.clone();
            return person;
        } catch (Exception e) {
            throw new AssertionError();
        }

    }
}
