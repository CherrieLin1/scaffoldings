package study.clone.deepClone;

import lombok.AllArgsConstructor;
import lombok.Data;
import study.clone.Address;

/**
 * @ClassName Person
 * @Description
 * @Author linshengyan
 * @Date 2022/3/25 5:19 下午
 */
@Data
@AllArgsConstructor
public class DeepClonePerson implements Cloneable {
    private Address address;

    public static void main(String[] args) {
        DeepClonePerson person = new DeepClonePerson(new Address("南京"));
        DeepClonePerson clone = person.clone();
        //因为是深克隆，所以person里面的address其实是一样的
        System.out.println("(clone.getAddress() == person.getAddress()) = " + (clone.getAddress() == person.getAddress()));
    }

    /**
     * 深度克隆，里面的对象也需要拷贝一份
     *
     * @return
     */
    @Override
    public DeepClonePerson clone() {
        try {
            DeepClonePerson person = (DeepClonePerson) super.clone();
            //注意，这里要取当前已经clone出来的person对象的address来克隆，而不能使用类中的address
            Address address = person.getAddress();
            Address clone = address.clone();
            person.setAddress(clone);
            return person;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }

    }
}
