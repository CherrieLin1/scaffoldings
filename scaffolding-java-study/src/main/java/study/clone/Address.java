package study.clone;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @ClassName Address
 * @Description
 * @Author linshengyan
 * @Date 2022/3/25 5:22 下午
 */
@Data
@AllArgsConstructor
public class Address implements Cloneable {
    private String name;

    public static void main(String[] args) {
        Address address = new Address("南京");
        Address clone = address.clone();
    }


    @Override
    public Address clone() {

        try {
            Address address = (Address) super.clone();
            return address;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }


    }
}
