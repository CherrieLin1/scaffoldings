package study.designPattern;

/**
 * 可以参考https://www.cnblogs.com/binaway/p/8889184.html
 * 有饿汉模式、延迟加载 / “懒汉模式”、线程安全的“懒汉模式”、DCL双检查锁机制（DCL：double checked locking）
 * @author create by lsy on 2021/11/5 5:54 下午
 */
public class Singleton {
    /**
     * 静态实例
     */
    private static  Singleton INSTANCE = null;

    /**
     * 构造方法私有化
     */
    private Singleton() {
    }

    /**
     * 提供一个公共的获取单利的方法
     * @return
     */
    public Singleton getInstance(){
        if (INSTANCE == null){
            synchronized (this){
                if (INSTANCE == null){
                    INSTANCE = new Singleton();

                }
            }
        }
        return INSTANCE;
    }


}
