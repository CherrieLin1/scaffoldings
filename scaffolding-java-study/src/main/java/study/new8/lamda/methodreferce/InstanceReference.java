package study.new8.lamda.methodreferce;

/**
 * @ClassName InstanceRefence
 * @Description 实例方法引入，和对象方法引入的区别是需要new一个对象
 * @Author linshengyan
 * @Date 2022/5/7 2:28 下午
 */
public class InstanceReference {
    public static void main(String[] args) {
        InstanceReference instanceReference = new InstanceReference();
        //实例方法引入
        InstanceInterface instanceInterface = instanceReference::test;
        instanceInterface.test(1);
    }

    public void test(Integer integer){
        System.out.println("我是实例方法引入的例子，参数值为：" + integer);

    }

}


@FunctionalInterface
interface InstanceInterface {
    void test(Integer integer);

}