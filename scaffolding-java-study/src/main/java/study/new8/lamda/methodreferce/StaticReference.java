package study.new8.lamda.methodreferce;

/**
 * @ClassName 静态方法引入
 * @Description
 * @Author linshengyan
 * @Date 2022/5/7 2:24 下午
 */
public class StaticReference {
    public static void main(String[] args) {
        StaticInterface staticInterface = StaticReference::test;
        staticInterface.test(1);

    }
    public static void test(Integer integer) {
        System.out.println("我是演示静态方法引入的例子，参数值为：" + integer);
    }

}

@FunctionalInterface
interface StaticInterface {
    public void test(Integer integer);

}
