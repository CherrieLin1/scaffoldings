package study.new8.lamda.methodreferce;

/**
 * @ClassName ObjectReference
 * @Description 对象方法引入
 * @Author linshengyan
 * @Date 2022/5/7 2:35 下午
 */
public class ObjectReference {
    public static void main(String[] args) {
        //对象引入，和实例引入的区别是不需要创建一个实例，这个实例是接口中定义好的
        ObjectInterface objectInterface = ObjectReference::print;
        objectInterface.test(new ObjectReference());

    }

    public void print(){
        System.out.println("我是演示静态方法引入的例子");
    }


}

@FunctionalInterface
interface ObjectInterface{
    void test(ObjectReference objRef);
}
