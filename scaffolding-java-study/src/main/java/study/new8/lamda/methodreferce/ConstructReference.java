package study.new8.lamda.methodreferce;

/**
 * @ClassName ConstructReference
 * @Description 构造方法引入
 * @Author linshengyan
 * @Date 2022/5/7 2:42 下午
 */
public class ConstructReference {
    public static void main(String[] args) {
        ConstructWithNoneArguments constructWithNoneArguments = Person::new;
        Person person = constructWithNoneArguments.getPerson();
        System.out.println("person = " + person);

        ConstructWithAllArguments constructWithAllArguments = Person::new;
        Person shengyan = constructWithAllArguments.getPerson("生燕", 10);
        System.out.println("shengyan = " + shengyan);
    }
}


@FunctionalInterface
interface ConstructWithNoneArguments {
    Person getPerson();

}

@FunctionalInterface
interface ConstructWithAllArguments {
    /**
     * 参数要和person的构造方法一致
     * @param name
     * @param age
     * @return
     */
    Person getPerson(String name, Integer age);
}

class Person {
    String name;
    Integer age;

    public Person() {
    }

    public Person(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Person{");
        sb.append("name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append('}');
        return sb.toString();
    }
}
