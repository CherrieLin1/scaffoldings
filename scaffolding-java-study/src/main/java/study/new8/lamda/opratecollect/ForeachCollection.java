package study.new8.lamda.opratecollect;

import study.new8.CommonUtils;
import study.new8.Person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @ClassName ForeachList
 * @Description
 * @Author linshengyan
 * @Date 2022/5/9 2:19 下午
 */
public class ForeachCollection {
    public static void main(String[] args) {
        List<Person> person = CommonUtils.getPerson();
        //遍历一个集合以前写法
        for (Person person1 : person) {
            System.out.println("person1 = " + person1);
        }

        //遍历一个集合优化写法
        person.forEach(x-> System.out.println(x));


        //输出的优化写法
        List<Integer> list =new ArrayList<>();
        Collections.addAll(list,1,2,3,4,5,6,7);
        list.forEach(System.out::println);

        //还可以在foreEach中做一些筛选
        person.forEach(x->{
            //如果这个人的年龄大于10，输出改人
            if (x.getAge()>10){
                System.out.println(x);
            }
        });


        //还可以对集合做一个操作,将集合中粘连大于70岁的人移除
        person.removeIf(x->x.getAge()>70);
        System.out.println("person = " + person);
    }


}
