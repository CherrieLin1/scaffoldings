package study.new8.lamda.opratecollect;

import study.new8.CommonUtils;
import study.new8.Person;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @ClassName SortCollection
 * @Description 排序
 * @Author linshengyan
 * @Date 2022/5/9 2:49 下午
 */
public class SortCollection {
    public static void main(String[] args) {
        System.out.println("============list==========");
        List<Person> person = CommonUtils.getPerson();
        //按照人的年龄倒叙排列   如果需要正序，就 o1.getAge() -  o2.getAge()
        person.sort((o1, o2) -> o2.getAge() - o1.getAge());
        person.forEach(System.out::println);


        System.out.println("==========set===========");

        //这种写法会丢失一个数据
        Set setLostparam = new TreeSet<Person>((o1, o2) -> o1.getAge() - o2.getAge());
        setLostparam.add(new Person("张三", 10));
        setLostparam.add(new Person("李四", 12));
        setLostparam.add(new Person("王五", 40));
        setLostparam.add(new Person("jenny", 80));
        setLostparam.add(new Person("danny", 60));
        setLostparam.add(new Person("liming", 16));
        setLostparam.add(new Person("liming", 16));
        setLostparam.forEach(System.out::println);


        System.out.println("=============set no lost param ========");
        //不会丢失数据，因为返回是0的时候，会认为是相等的
        Set setNoneLostParam = new TreeSet<Person>((o1,o2) -> {
            if (o1.getAge() >= o2.getAge()) {
                return 1;
            }else {
                return -1;
            }

        });
        setNoneLostParam.add(new Person("张三", 10));
        setNoneLostParam.add(new Person("李四", 12));
        setNoneLostParam.add(new Person("王五", 40));
        setNoneLostParam.add(new Person("jenny", 80));
        setNoneLostParam.add(new Person("danny", 60));
        setNoneLostParam.add(new Person("liming", 16));
        setNoneLostParam.add(new Person("liming", 16));
        setNoneLostParam.forEach(System.out::println);




    }
}
