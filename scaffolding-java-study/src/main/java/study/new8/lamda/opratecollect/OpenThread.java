package study.new8.lamda.opratecollect;

/**
 * @ClassName OpenThread
 * @Description
 * @Author linshengyan
 * @Date 2022/5/9 3:12 下午
 */
public class OpenThread {
    public static void main(String[] args) {
        new Thread(() -> System.out.println(Thread.currentThread().getName())).start();
    }
}
