package study.new8.lamda.service;

/**
 * @ClassName JDK8Interface
 * @Description jdk8接口
 * @Author linshengyan
 * @Date 2022/5/6 3:40 下午
 */
public interface JDK8Interface {
    String name = "lin";

    /**
     * 抽象方法
     */
    void add();

    /**
     * default
     */
    default void defaultAdd() {
        System.out.println("我是默认的方法");
    }

    /**
     * static
     */
    static void staticAdd() {
        System.out.println("我是静态方法");
    }

    /**
     * Object类中的方法
     * @return
     */
    @Override
    String toString();
}
