package study.new8.lamda.service;

/**
 * @ClassName JDK8InterfaceImpl
 * @Description
 * @Author linshengyan
 * @Date 2022/5/6 3:44 下午
 */
public class JDK8InterfaceImpl implements JDK8Interface {
    /**
     * idea只会强制实现抽象方法
     */
    @Override
    public void add() {
        System.out.println("我是抽象方法");
    }
}
