package study.new8.lamda.service;

/**
 * @ClassName JDKInterfaceTest
 * @Description
 * @Author linshengyan
 * @Date 2022/5/6 3:48 下午
 */
public class JDKInterfaceTest {
    public static void main(String[] args) {
        //1.实现类
        JDK8Interface jdk8Interface = new JDK8InterfaceImpl();
        jdk8Interface.add();

        //2.匿名内部类
        JDK8Interface jdk8Interface1 = new JDK8Interface() {
            @Override
            public void add() {
                System.out.println("我是匿名内部类");
            }
        };
        jdk8Interface1.add();


        //3.lamda表达式
        JDK8Interface jdk8Interface2 = () -> {
            System.out.println("我是lamda表达式写法");
        };
        jdk8Interface2.add();
        //简化写法
        JDK8Interface jdk8Interface3 = () -> System.out.println("我是lamda表达式简化版写法");
        jdk8Interface3.add();
    }
}
