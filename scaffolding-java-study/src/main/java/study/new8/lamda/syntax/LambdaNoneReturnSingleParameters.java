package study.new8.lamda.syntax;

/**
 * @ClassName LambdaNoneReturnSingleParameters
 * @Description
 * @Author linshengyan
 * @Date 2022/5/6 4:14 下午
 */
public interface LambdaNoneReturnSingleParameters {
    void test(int a);
}
