package study.new8.lamda.syntax;

/**
 * @ClassName LambdaSyntaxTest
 * @Description
 * @Author linshengyan
 * @Date 2022/5/6 4:16 下午
 */
public class LambdaSyntaxTest {
    public static void main(String[] args) {
        //无入参，无返回
        LambdaNoneReturnNoneParameters lambda1 = () -> {
            System.out.println("我是无入参，无返回的lambda表达式");
        };
        lambda1.test();

        //单入参，无返回
        LambdaNoneReturnSingleParameters lambda2 = a -> {
            System.out.println("我是单入参，无返回lambda表达式，我的值为：" + a);
        };
        lambda2.test(2);

        //多入参，无返回
        LambdaNoneReturnMultipleParameters lambda3 = (a, b) -> {
            System.out.println("我是多入参，无返回lambda表达式，我的值的和为：" + (a + b));
        };
        lambda3.test(1, 2);


        //无入参，单返回
        LambdaSingleReturnNoneParameters lambda4 = () -> 1;
        System.out.println("无入参，单返回,lambda4.test() = " + lambda4.test());


        //单入参，单返回
        LambdaSingleReturnSingleParameters lambda5 = (int a) -> {
            return a;
        };
        System.out.println("单入参，单返回,lambda5.test(5) = " + lambda5.test(5));

        //多入参，单返回
        LambdaSingleReturnMultipleParameters lambda6 = (int a, int b) -> {
            return a + b;
        };
        System.out.println("多入参，单返回,lambda6.test() = " + lambda6.test(5, 6));


    }


}
