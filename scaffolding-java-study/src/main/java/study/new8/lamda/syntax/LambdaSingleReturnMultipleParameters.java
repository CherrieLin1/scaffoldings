package study.new8.lamda.syntax;

/**
 * @ClassName LamdaMultipleReturnxMultipleParameters
 * @Description
 * @Author linshengyan
 * @Date 2022/5/6 4:11 下午
 */
public interface LambdaSingleReturnMultipleParameters {
    int test(int a,int b);

}
