package study.new8.lamda.syntax;

/**
 * @ClassName LambdaSingleReturnSingleParameters
 * @Description
 * @Author linshengyan
 * @Date 2022/5/6 4:13 下午
 */
public interface LambdaSingleReturnSingleParameters {
    int test(int a);
}
