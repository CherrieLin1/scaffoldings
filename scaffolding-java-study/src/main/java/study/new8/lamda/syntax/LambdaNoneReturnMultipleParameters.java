package study.new8.lamda.syntax;

/**
 * @ClassName LambdaNoneReturnMultipleParameters
 * @Description
 * @Author linshengyan
 * @Date 2022/5/6 4:13 下午
 */
public interface LambdaNoneReturnMultipleParameters {
    void test(int a,int b);
}
