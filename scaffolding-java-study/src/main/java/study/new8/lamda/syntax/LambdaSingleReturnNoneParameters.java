package study.new8.lamda.syntax;

/**
 * @ClassName LambdaSingleReturnNoneParameters
 * @Description
 * @Author linshengyan
 * @Date 2022/5/6 4:13 下午
 */
public interface LambdaSingleReturnNoneParameters {
    int test();
}
