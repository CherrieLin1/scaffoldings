package study.new8.lamda.closure;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * @ClassName CloseureDemo
 * @Description
 * @Author linshengyan
 * @Date 2022/5/7 3:26 下午
 */
public class ClosureDemo {
    public static void main(String[] args) {
        System.out.println("getNumber().get() = " + getNumber().get());
    }

    /**
     * 无入参，指定返回值，也是一个闭包
     * @return
     */
    private static Supplier<Integer> getNumber(){
        //1.闭包提升局部变量（包围）的生命周期，方法结束后，变量不会被销毁
        //2.lambda表达式中的变量默认是static final的，不可以被修改
        int num = 10;
        return ()->{
            return num;
        };

    }

}
