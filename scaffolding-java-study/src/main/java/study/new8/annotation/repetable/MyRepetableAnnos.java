package study.new8.annotation.repetable;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @ClassName MyRepetableAnnos
 * @Description 重复注解容器
 * @Author linshengyan
 * @Date 2022/5/11 5:02 下午
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface MyRepetableAnnos {
    MyRepetableAnno[] value();
}
