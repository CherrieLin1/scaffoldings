package study.new8.annotation.repetable;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @ClassName MyRepetableAnno
 * @Description 重复注解类
 * @Author linshengyan
 * @Date 2022/5/11 5:01 下午
 */
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(MyRepetableAnnos.class)
public @interface MyRepetableAnno {
    String value();
}
