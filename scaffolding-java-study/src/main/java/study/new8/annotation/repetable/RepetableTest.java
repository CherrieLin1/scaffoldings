package study.new8.annotation.repetable;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedType;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @ClassName RepetableTest
 * @Description
 * @Author linshengyan
 * @Date 2022/5/11 5:07 下午
 */
@MyRepetableAnno("测试类注解")
@MyRepetableAnno("测试类注解，测试很多东西")
public class RepetableTest {

    private Integer age;

    public String name;

    @MyRepetableAnno("测试一下？")
    @MyRepetableAnno("确定要测试一下？")
    @MyRepetableAnno("真的要测试一下的！")
    public void test() {
        System.out.println("test~");
    }


    private void test1() {

    }

    public static void main(String[] args) throws NoSuchMethodException {
        Class<RepetableTest> repetableTestClass = RepetableTest.class;

        System.out.println("=========get public method ,include object class========");
        Method[] methods = repetableTestClass.getMethods();
        for (Method method : methods) {
            System.out.println("getMethods = " + method.getName());
        }


        System.out.println("===============get all method,not include object class=======");

        Method[] declaredMethods = repetableTestClass.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            System.out.println("getDeclaredMethods = " + declaredMethod.getName());
        }



        System.out.println("===========get public filed==========");
        for (Field field : repetableTestClass.getFields()) {
            System.out.println("getFields = " + field.getName());
        }


        System.out.println("========get all filed=========");
        for (Field declaredField : repetableTestClass.getDeclaredFields()) {
            System.out.println("getDeclaredFields = " + declaredField.getName());
        }


        System.out.println("======get class annotation by type=======");
        //获取类的注解并打印
        MyRepetableAnno[] classAnnotations = repetableTestClass.getAnnotationsByType(MyRepetableAnno.class);
        for (MyRepetableAnno annotation : classAnnotations) {
            System.out.println(annotation.value());
        }


        System.out.println("======get method annotation by type========");
        //获取方法的注解并打印
        MyRepetableAnno[] methodAnnotations = repetableTestClass.getMethod("test").getAnnotationsByType(MyRepetableAnno.class);
        for (MyRepetableAnno methodAnnotation : methodAnnotations) {
            System.out.println(methodAnnotation.value());
        }


    }
}
