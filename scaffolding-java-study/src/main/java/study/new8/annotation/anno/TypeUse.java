package study.new8.annotation.anno;

/**
 * @ClassName TypeUse
 *
 * @Description
 * @Author linshengyan
 * @Date 2022/5/11 5:45 下午
 */
public class TypeUse {
    public @TestTypeUse Integer age;

    public void test(@TestTypeUse Integer age,@TestTypeUse String name){


    }
}
