package study.new8.annotation.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @ClassName TestTypeParameter  表示该注解能写在类型参数的声明语句中,包括泛型
 * @Description
 * @Author linshengyan
 * @Date 2022/5/11 5:38 下午
 */
@Retention(RetentionPolicy.RUNTIME)
//表示该注解能写在类型参数的声明语句中，包括泛型
@Target(ElementType.TYPE_PARAMETER)
public @interface TestTypeParameter {
}
