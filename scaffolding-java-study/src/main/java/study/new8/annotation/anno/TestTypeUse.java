package study.new8.annotation.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @ClassName TestTypeUse
 * @Description
 * @Author linshengyan
 * @Date 2022/5/11 5:38 下午
 */
@Retention(RetentionPolicy.RUNTIME)
//表示注解可以在任何用到类型的地方使用
@Target(ElementType.TYPE_USE)
public @interface TestTypeUse {
}
