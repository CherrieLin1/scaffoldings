package study.new8.annotation.anno;

/**
 * @ClassName TypeParameter
 * @Description
 * @Author linshengyan
 * @Date 2022/5/11 5:42 下午
 */
public class TypeParameter<@TestTypeParameter T> {
    public <@TestTypeParameter T> void test(Integer age) {
        return;
    }
}
