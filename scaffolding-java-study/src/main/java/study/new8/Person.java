package study.new8;

import com.google.common.base.Objects;

/**
 * @ClassName StreamPerson
 * @Description
 * @Author linshengyan
 * @Date 2022/5/9 2:15 下午
 */
public class Person {
    private String name;
    private Integer age;

    public Person(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Person{");
        sb.append("name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return Objects.equal(getName(), person.getName()) && Objects.equal(getAge(), person.getAge());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getName(), getAge());
    }
}
