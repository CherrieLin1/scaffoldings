package study.new8.datetime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName DateUtils
 * @Description jdk8时间操作类的新特性
 * @Author linshengyan
 * @Date 2022/5/11 10:57 上午
 */
public class DateUtils {


    /**
     * 1.测试老api
     *
     * @throws ParseException
     */
    public static void testOld() throws ParseException {
        //Date位置不合理，即在util包中有，在sql的包中也有
        Date date = new Date(2010, 12, 21);
        System.out.println("date.toString() = " + date.toString());
        System.out.println("date = " + date);


        //format(Date date)
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
        String format = simpleDateFormat.format(date);
        System.out.println("format = " + format);

        //parse(String str)
        Date parse = simpleDateFormat.parse("2021-12-1");
        System.out.println("parse = " + parse);

        //线程不安全,会有异常的出现
        for (int i = 0; i < 50; i++) {
            new Thread(() ->
            {
                try {
                    Date parse1 = simpleDateFormat.parse("2021-12-1");
                    System.out.println("parse1 = " + parse1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            ).start();
        }
    }


    /**
     * 2.测试新API中的日期类 LocalDate
     */
    public static void testDate() {
        //获取当前的日期
        LocalDate localDate = LocalDate.now();
        System.out.println("localDate = " + localDate);

        //获取指定的日期
        LocalDate definite = LocalDate.of(2012, 12, 31);
        System.out.println("definite = " + definite);

        //通过LocalDate获取对应的日期
        System.out.println("definite.getDayOfMonth() = " + definite.getDayOfMonth());
        System.out.println("definite.getDayOfWeek() = " + definite.getDayOfWeek().getValue());
        System.out.println("definite.getDayOfYear() = " + definite.getDayOfYear());
        System.out.println("definite.getMonth() = " + definite.getMonth());

    }

    /**
     * 3.测试新API中的时间类localTime
     */
    public static void testTime() {
        //获取当前的时间
        LocalTime now = LocalTime.now();

        //获取指定的时间
        LocalTime localTime = LocalTime.of(12, 23, 34, 345);

        //通过LocalTime获取时间相关的信息
        System.out.println("localTime.getHour() = " + localTime.getHour());
        System.out.println("localTime.getMinute() = " + localTime.getMinute());
        System.out.println("localTime.getSecond() = " + localTime.getSecond());
        System.out.println("localTime.getNano() = " + localTime.getNano());

    }

    /**
     * 4.测试新API中的日期时间类
     */
    public static void testDateTime() {
        //获取当前的时间日期
        LocalDateTime localDateTime = LocalDateTime.now();

        //获取指定的时间日期
        LocalDateTime localDateTime1 = LocalDateTime.of(2123, 12, 23, 12, 12, 12, 122);

        //通过时间日期类获取相关的信息
        System.out.println("localDateTime.getDayOfMonth() = " + localDateTime.getDayOfMonth());
        System.out.println("localDateTime.getHour() = " + localDateTime.getHour());
    }


    /**
     * 5.测试新API中日期的修改,不会改变原对象，每一个修改结果都会返回一个新的对象
     */
    public static void testDateModify() {
        LocalDate localDate = LocalDate.now();
        System.out.println("localDate = " + localDate);

        //利用.with()修改对应的日期
        System.out.println("localDate.withYear(2013) = " + localDate.withYear(2013));
        System.out.println("localDate.withDayOfMonth(12) = " + localDate.withDayOfMonth(12));
        System.out.println("localDate.withMonth(2) = " + localDate.withMonth(2));
        System.out.println("localDate.withDayOfYear(45) = " + localDate.withDayOfYear(45));
        System.out.println("after with modify -->localDate = " + localDate);
        System.out.println("=============");


        //利用.plus()增加对应的时间
        System.out.println("localDate.plusDays(3) = " + localDate.plusDays(3));
        System.out.println("localDate.plusYears(2) = " + localDate.plusYears(2));
        System.out.println("localDate.plusMonths(4) = " + localDate.plusMonths(4));
        System.out.println("localDate.plusWeeks(3) = " + localDate.plusWeeks(3));
        System.out.println("after plus modify localDate = " + localDate);
        System.out.println("==============");


        //利用.minus()减去对应的时间
        System.out.println("localDate.minusDays(12) = " + localDate.minusDays(12));
        System.out.println("localDate.minusMonths(10) = " + localDate.minusMonths(10));
        System.out.println("localDate.minusWeeks(3) = " + localDate.minusWeeks(3));
        System.out.println("localDate.minusYears(12) = " + localDate.minusYears(12));
        System.out.println("after minus modify localDate = " + localDate);

    }


    /**
     * 6.测试新API中时间的修改，不会改变原对象，每一个修改结果都会返回一个新的对象
     */
    public static void testTimeModify() {
        LocalTime localTime = LocalTime.now();
        System.out.println("localTime = " + localTime);

        //with
        System.out.println("localTime.withHour(12) = " + localTime.withHour(12));

        //plus
        System.out.println("localTime.plusHours(23) = " + localTime.plusHours(23));

        //minus
        System.out.println("localTime.minusHours(12) = " + localTime.minusHours(12));

    }

    /**
     * 7.测试新API中日期时间的修改，不会改变原对象，每一个修改结果都会返回一个新的对象
     */
    public static void testDateTimeModify() {
        LocalDateTime now = LocalDateTime.now();
        System.out.println("now = " + now);
        // 修改日期时间  对日期时间的修改，对已存在的LocalDate对象，创建了它模板,并不会修改原来的信息
        System.out.println("now.withYear(1998) = " + now.withYear(1998));
        System.out.println("now.withMonth(10) = " + now.withMonth(10));
        System.out.println("now.withDayOfMonth(6) = " + now.withDayOfMonth(6));
        System.out.println("now.withHour(8) = " + now.withHour(8));
        System.out.println("now.withMinute(15) = " + now.withMinute(15));

        // 在当前日期时间的基础上 加上或者减去指定的时间
        System.out.println("now.plusDays(2) = " + now.plusDays(2));
        System.out.println("now.plusYears(10) = " + now.plusYears(10));
        System.out.println("now.plusMonths(6) = " + now.plusMonths(6));
        System.out.println("now.minusYears(10) = " + now.minusYears(10));
        System.out.println("now.minusMonths(6) = " + now.minusMonths(6));
        System.out.println("now.minusDays(7) = " + now.minusDays(7));
    }


    /**
     * 8.日期时间的比较
     */
    public static void testDateTimeCompare() {
        LocalDate now = LocalDate.now();
        LocalDate date = LocalDate.of(2020, 1, 3);
        // 在JDK8中要实现 日期的比较 isAfter  isBefore isEqual 通过这几个方法来直接比较
        System.out.println("now.isAfter(date) = " + now.isAfter(date));
        System.out.println("now.isBefore(date) = " + now.isBefore(date));
        System.out.println("now.isEqual(date) = " + now.isEqual(date));

    }


    /**
     * 日期时间的格式化
     */
    public static void testDateTimeFormat() {
        LocalDateTime now = LocalDateTime.now();
        System.out.println("now = " + now);

        //利用自带的format格式化日期---日期转为字符串
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        System.out.println("now.format(dateTimeFormatter) = " + now.format(dateTimeFormatter));

        //ofPattern()自定义format---日期转为字符串
        //⚠️这个自定义的格式：MM和HH一定得是大写，否则会报异常
        DateTimeFormatter dateTimeFormatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        System.out.println("now.format(dateTimeFormatter) = " + now.format(dateTimeFormatter1));


        //字符串转为日期"1997-05-06 22:45:16
        LocalDateTime parse = LocalDateTime.parse("1997-05-06 22:45:16", dateTimeFormatter1);
        System.out.println("parse = " + parse);

    }


    /**
     * 测试时间戳
     */
    public static void testInstant() throws InterruptedException {
        Instant now = Instant.now();
        //获取的时间和北京时间相差8小时（晚于北京时间）通过查看源代码发现Instant.now()使用等是UTC时间Clock.systemUTC().instant()
        System.out.println("now.toString() = " + now.toString());

        //修正为北京时间
        System.out.println("now.plusMillis(TimeUnit.HOURS.toMillis(8)) = " + now.plusMillis(TimeUnit.HOURS.toMillis(8)));

        //获取秒数
        System.out.println("now.getEpochSecond() = " + now.getEpochSecond());

        //获取毫秒
        System.out.println("now.toEpochMilli() = " + now.toEpochMilli());

        //测量从getEpochSecond()返回的第二个值起的纳秒总数。例如当前时间为2022-05-11T15:25:40.204500Z，那么其返回值就是204500000
        System.out.println("now.getNano() = " + now.getNano());


        Thread.sleep(2000);
        Instant end = Instant.now();


        //测量时间长度
        System.out.println("(end-now) = " + (end.getEpochSecond() - now.getEpochSecond()));

    }


    /**
     * Duration:时间日期差可以计算，看源码可以发现（LocalTime LocalDate LocalTimeDate）
     * Period:只能计算日期差(LocalDate)
     */
    public static void testDurationAndPeriod() {
        //Duration 计算日期时间差， Duration.between（LocalDate/LocalTime/LocalDateTime）
        LocalDateTime now = LocalDateTime.now();
        System.out.println("now = " + now);
        LocalDateTime end = LocalDateTime.of(2022, 5, 12, 2, 2, 2);
        System.out.println("end = " + end);
        Duration duration = Duration.between(now, end);
        System.out.println("duration.toString() = " + duration.toString());
        System.out.println("duration.toMillis() = " + duration.toMillis());
        System.out.println("between.getSeconds() = " + duration.getSeconds());
        System.out.println("duration.toMinutes() = " + duration.toMinutes());
        System.out.println("duration.toHours() = " + duration.toHours());
        System.out.println("duration.toDays() = " + duration.toDays());

        System.out.println("===========");
        //Period计算日期差,Period.between(LocalDate),只能是LocalDate
        LocalDate dateStart = LocalDate.now();
        LocalDate dateEnd = LocalDate.of(2022, 5, 12);
        Period period = Period.between(dateStart, dateEnd);
        System.out.println("period.getDays() = " + period.getDays());
        System.out.println("period.getMonths() = " + period.getMonths());
        System.out.println("period.getYears() = " + period.getYears());
    }


    /**
     * 测试TemporalAdjuster，时间校正器类
     */
    public static void testTemporalAdjuster() {
        LocalDateTime now = LocalDateTime.now();
        System.out.println("now = " + now);

        //利用匿名内部类实现一个接口，now.with(TemporalAdjuster)，TemporalAdjuster是一个接口，实现其的方式有三种，最方便的就是lambda表达式
        LocalDateTime with = now.with((temporal) -> {
            LocalDateTime localDateTime = (LocalDateTime) temporal;
            LocalDateTime localDateTime1 = localDateTime.plusMonths(1).withDayOfMonth(1);
            return localDateTime1;


        });
        System.out.println("with = " + with);


        //或者直接用TemporalAdjusters的静态方法实现,实现下个月的第一天
        LocalDateTime with1 = now.with(TemporalAdjusters.firstDayOfNextMonth());
        System.out.println("with1 = " + with1);

        //实现今年的第一天
        LocalDateTime with2 = now.with(TemporalAdjusters.firstDayOfYear());
        System.out.println("with2 = " + with2);

        System.out.println("校正很多次后值不变 now= " + now);
    }


    public static void main(String[] args) {
        testZoneDate();
    }


    /**
     * 测试时区
     */
    public static void testZoneDate() {
        //ZoneId获取JDK提供的所有的时区
        Set<String> availableZoneIds = ZoneId.getAvailableZoneIds();
//        availableZoneIds.forEach(System.out::println);

        //获取本地的时间
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println("localDateTime = " + localDateTime);

        //获取标准的时间
        ZonedDateTime defaultTime = ZonedDateTime.now(Clock.systemUTC());
        System.out.println("defaultTime = " + defaultTime);

        //通过ZonedDateTime获取本地时间，和计算机所在的时区有关
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        System.out.println("zonedDateTime = " + zonedDateTime);

        //zoneShanghai
        ZonedDateTime zoneShanghai = ZonedDateTime.now(ZoneId.of("Asia/Shanghai"));
        System.out.println("zoneShanghai = " + zoneShanghai);

        //ZoneNanjing
        ZonedDateTime zoneNanjing = ZonedDateTime.now(ZoneId.of("Asia/Nanjing"));
        System.out.println("zoneNanjing = " + zoneNanjing);

    }


}
