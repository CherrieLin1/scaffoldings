package study.new8.stream;

import org.apache.commons.collections.CollectionUtils;
import study.bean.Dog;
import study.bean.Resource;
import study.new8.Person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @ClassName SteamUtils
 * @Description
 * @Author linshengyan
 * @Date 2022/5/9 3:17 下午
 */
public class SteamUtils {

    public static void main(String[] args) {
        //1.list to set,set本来就是不存相同的数据
        listToSet();

        //2.list to map
        listToMap();


        //3.Stream<Integer> count
        simpleCount();

        //4.Stream<Object> count
        objectCount();

        //5.streamMax
        findMax();

        //6.streamMin
        findMin();

        //7.所有的都匹配
        allMatch();

        //8.任意一个匹配
        anyMatch();

        //9.所有的都不匹配
        noneMatch();

        //10.stream filter
        filter();

        //11.stream forEach
        forEach();

        //12.stream sorted
        sorted();

        //13.stream limit and skip
        paged();

        //14.列表根据某一个字段去重
        getDistinctParams();

        //15.获取嵌套对象
        getFlatMap();

        //16.groupBy的用法
        groupByTemplateId();

        //17.获取对象列表某一个字段的列表
        getObjectKeyList();

       //18.map 和foreach的用法，工作中遇到的坑
        compareMapAndForeach();




    }


    /**
     * 获取列表对象中的某一个字段的不重复的值
     * peek()是调试功能，一般接在stream()后面
     */
    public static void getDistinctParams() {
        List<Resource> list = getResourceList();
        //获取不重复的指定字段的值
        List<Integer> collect = list.stream()
                .peek(x -> System.out.println("模版id:" + x.getResourceTemplateId()))
                .map(x -> x.getResourceTemplateId())
                .distinct()
                .collect(Collectors.toList());
        System.out.println("collect.size() = " + collect.size());
    }


    /**
     * 从列表中获取嵌套列表对象并统计数量
     */
    public static void getFlatMap() {
        List<Resource> list = getResourceList();
        List<Dog> dogList = list.stream()
                .filter(resource -> null != resource && CollectionUtils.isNotEmpty(resource.getDogs()))
                .flatMap(resource -> resource.getDogs().stream())
                .collect(Collectors.toList());

        System.out.println("dogList的长度为：" + dogList.size());


        //打印出每个狗狗的名字
        list.stream()
                //流过滤
                .filter(resource -> null != resource && CollectionUtils.isNotEmpty(resource.getDogs()))
                //返回流
                .flatMap(resource -> resource.getDogs().stream())
                .collect(Collectors.toList())
                .forEach(x -> System.out.println(x.getName()));
    }


    /**
     * 根据某个字段分组
     *
     * @return
     */
    public static Map<Integer, List<Resource>> groupByTemplateId() {
        List<Resource> list = getResourceList();
        //流收集
        Map<Integer, List<Resource>> map = list.stream().collect(Collectors.groupingBy(Resource::getResourceTemplateId));
        return map;
    }


    /**
     * 获取某一个字段的所有值的列表（重复，不重复）
     *
     * @return
     */
    public static void getObjectKeyList() {
        List<Resource> list = getResourceList();
        //不去重
        List<Long> collect = list.stream().map(x -> x.getId()).collect(Collectors.toList());
        System.out.println(collect);
        //去重
        Set<Long> set = list.stream().map(x -> x.getId()).collect(Collectors.toSet());
        System.out.println("set = " + set);
    }





    /**
     * .map():方法的用途是将旧数据转换后变为新数据，是一种 1:1 的映射，每个输入元素按照规则转换成另一个元素
     * .forEach():它只接收参数，没有返回值,然后在 Stream 的每一个元素上执行该表达式
     */
    public static void compareMapAndForeach() {
        List<Resource> list = getResourceList();

        Map<String, Resource> map = new HashMap<>();
        //map.size == 0,map里面还是空的
        list.stream().map(x -> map.put(x.getId() + "_", x));
        System.out.println("map.size() = " + map.size());

        //map.size = list.size()，map里面有值
        list.stream().forEach(x -> map.put(x.getId() + "_", x));
        System.out.println("map.size() = " + map.size());


        //利用这种方式写
        Map<String, Resource> collect = list.stream().collect(Collectors.toMap(x -> x.getId() + "_", x -> x));
        System.out.println("collect.size() = " + collect.size());


    }


    /**
     * 对流进行过滤，剔除不满足条件的
     */
    public static void filter() {
        List<Person> personList = getPersonList();
        Stream<Person> personStream = personList.stream().filter(x -> x.getAge() > 20);
        personStream.forEach(System.out::println);
    }

    /**
     * 遍历，这个方法就是将每个人的名字加上一个前缀
     */
    public static void forEach() {
        List<Person> personList = getPersonList();
        personList.stream().forEach(x -> x.setName("ex:" + x.getName()));
        personList.forEach(System.out::println);
    }


    /**
     * 排序 例子是正序
     */
    public static void sorted() {
        List<Person> personList = getPersonList();
        personList.stream().sorted((person1, person2) -> person1.getAge() - person2.getAge()).forEach(System.out::println);
    }


    /**
     * 跳过前两个，打印三个
     */
    public static void paged() {
        List<Person> personList = getPersonList();
        personList.stream().skip(2).limit(3).forEach(System.out::println);
    }



    /**
     * 所有的都匹配
     */
    public static void allMatch() {
        List<Person> personList = getPersonList();
        boolean b = personList.stream().allMatch(x -> x.getAge() > 20);
        System.out.println("allMatch:" + b);

    }

    /**
     * 任意一个匹配
     */
    public static void anyMatch() {
        List<Person> personList = getPersonList();
        boolean b = personList.stream().anyMatch(x -> x.getName().equals("张三"));
        System.out.println("anyMatch:" + b);
    }


    /**
     * 一个都不匹配
     */
    public static void noneMatch() {
        List<Person> personList = getPersonList();
        boolean b = personList.stream().noneMatch(x -> x.getName().equals("张三"));
        System.out.println("anyMatch:" + b);

    }

    /**
     * 寻找最大值
     */
    public static void findMax() {
        List<Person> personList = getPersonList();
        Optional<Person> max = personList.stream().max((person1, person2) -> person1.getAge() - person2.getAge());
        System.out.println("max.get() = " + max.get());
    }

    /**
     * x寻找最小值
     */
    public static void findMin() {
        List<Person> personList = getPersonList();
        Optional<Person> min = personList.stream().min((person1, person2) -> person1.getAge() - person2.getAge());
        System.out.println("min.get() = " + min.get());

    }

    /**
     * 对象求和
     */
    public static void objectCount() {
        List<Person> personList = getPersonList();
        Optional<Person> reduce = personList.stream().reduce((person1, person2) -> {
            Person person = new Person("sum", 0);
            person.setAge(person1.getAge() + person2.getAge());

            return person;
        });
        System.out.println("reduce.get() = " + reduce.get());
    }


    /**
     * 单个数字的求和
     */
    public static void simpleCount() {
        //两种快速创建一个list的方式
//        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);//这种方式创建的ArrayList不能急促操作了
        List<Integer> list = new ArrayList<>();
        list.add(1);
        Collections.addAll(list, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        Stream<Integer> stream = list.stream();
        Optional<Integer> reduce = stream.reduce((x, y) -> x + y);
        System.out.println("reduce.get() = " + reduce.get());
    }


    /**
     * list to map
     * 特别要注意，list转map的过程中，key的值一定要是唯一的，不然会有java.lang.IllegalStateException: Duplicate key liming
     */
    public static void listToMap() {
        List<Person> person = getPersonList();
        Map<String, Person> collect = person.stream().collect(Collectors.toMap(x -> x.getName(), x -> x));
        collect.forEach((x, y) -> System.out.println("key = " + x + ",value =" + y));
    }


    /**
     * list 转 set
     * 注意，list中重复的数据会被覆盖掉，这个person重写了equals和hashcode方法
     */
    public static void listToSet() {
        List<Person> person = getDuplicatePersonList();
        Set<Person> collect = person.stream().collect(Collectors.toSet());
        collect.forEach(System.out::println);
    }

    /**
     * 有重复的数据
     *
     * @return
     */
    public static List<Person> getDuplicatePersonList() {
        List personList = new ArrayList();
        personList.add(new Person("张三", 10));
        personList.add(new Person("李四", 12));
        personList.add(new Person("王五", 40));
        personList.add(new Person("jenny", 80));
        personList.add(new Person("danny", 60));
        personList.add(new Person("liming", 16));
        personList.add(new Person("liming", 16));
        return personList;
    }

    /**
     * 无重复的数据
     *
     * @return
     */
    public static List<Person> getPersonList() {
        List personList = new ArrayList();
        personList.add(new Person("张三", 10));
        personList.add(new Person("李四", 12));
        personList.add(new Person("王五", 40));
        personList.add(new Person("jenny", 80));
        personList.add(new Person("danny", 60));
        personList.add(new Person("liming", 16));
        return personList;
    }

    /**
     * 构造数据，实例的主键Id唯一，嵌套对象
     */
    public static List<Resource> getResourceList() {
        List<Resource> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Resource resourceDO = new Resource();
            resourceDO.setId(i + 0L);
            resourceDO.setCode(UUID.randomUUID().toString());
            resourceDO.setBosId(100000L);
            resourceDO.setAmountType(1);
            resourceDO.setMerchantId(200000L);
            resourceDO.setVid(300000L);
            //产生一个0-10内的整数
            resourceDO.setResourceTemplateId(new Random().nextInt(10));
            System.out.println("模版id的值:" + resourceDO.getResourceTemplateId());
            List<Dog> dogs = new ArrayList<>(2);
            resourceDO.setDogs(dogs);
            for (int j = 0; j < 2; j++) {
                Dog dog = new Dog();
                dog.setName("狗狗:" + i + "-" + j);
                dog.setAge(i);
                dogs.add(dog);
            }
            list.add(resourceDO);
        }
        return list;
    }
}
