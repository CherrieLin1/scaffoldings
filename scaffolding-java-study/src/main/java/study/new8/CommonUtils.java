package study.new8;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @ClassName CommonUtils
 * @Description
 * @Author linshengyan
 * @Date 2022/5/9 2:15 下午
 */
public class CommonUtils {
    public static List<Person> getPerson() {
        List list = new ArrayList();
        list.add(new Person("张三", 10));
        list.add(new Person("李四", 12));
        list.add(new Person("王五", 40));
        list.add(new Person("jenny", 80));
        list.add(new Person("danny", 60));
        list.add(new Person("liming", 16));
        return list;
    }


    /**
     * 或者一个正序排列的set,当
     * @return
     */
    public static Set<Person> getPersonSetOnLost() {
        //这里new一个treeSet，他是一个自动排序的set，可以在new的时候自定义
        Set set = new TreeSet<Person>((o1, o2) -> o1.getAge() - o2.getAge());
        set.add(new Person("张三", 10));
        set.add(new Person("李四", 12));
        set.add(new Person("王五", 40));
        set.add(new Person("liming", 16));
        set.add(new Person("jenny", 80));
        set.add(new Person("danny", 60));
        set.add(new Person("liming", 16));
        return set;
    }


}
