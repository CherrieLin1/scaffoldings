package study.new8.optional;


import java.util.Optional;

/**
 * @ClassName OptionTestInstance
 * @Description 利用option创建一个对象
 * @Author linshengyan
 * @Date 2022/5/9 12:19 下午
 */
public class OptionTestInstance {
    private static OptionPerson person = null;

    public static void main(String[] args) {
        System.out.println("OptionTestInstance.getPerson() = " + OptionTestInstance.getPerson());
        System.out.println("OptionTestInstance.person = " + OptionTestInstance.person);
    }

    /**
     * 获取person
     *
     * @return
     */
    public static OptionPerson getPerson() {
        //一般写法
//        if (null ==person){
//            return createPerson();
//        }else {
//            return person;
//        }

        /**优化写法 orElse()-->,直接传递默认值**/
//        return Optional.ofNullable(person).orElse(createPerson());

        /**或者 orElseGet()-> 函数接口的形式，方法调用**/
        return Optional.ofNullable(person).orElseGet(OptionTestInstance::createPerson);
    }


    /**
     * 创建一个person对象
     *
     * @return
     */
    public static OptionPerson createPerson() {
        person = new OptionPerson();
        return person;
    }

}
