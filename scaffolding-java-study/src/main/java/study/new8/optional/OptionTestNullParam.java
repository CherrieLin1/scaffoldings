package study.new8.optional;

import java.util.Optional;

/**
 * @ClassName OptionForTestNull 测试
 * @Description
 * @Author linshengyan
 * @Date 2022/5/9 11:49 上午
 */
public class OptionTestNullParam {
    public static void main(String[] args) {
        String name = "shengyan";
        //通常这么写
        if (null != name) {
            System.out.println("name = " + name);
        }

        //用option优化
        Optional.ofNullable(name).ifPresent(x -> System.out.println(x));
        //再优化
        Optional.ofNullable(name).ifPresent(System.out::println);

        //Optional.of不能接收一个null值
        String nullName = null;
        //编辑不报错，运行会报java.lang.NullPointerException
        Optional<String> nullName2 = Optional.of(nullName);
        //运行也不会报错
        Optional<String> nullName1 = Optional.ofNullable(nullName);

        //isPresent(),返回值是true或者false
        boolean present = Optional.ofNullable(nullName).isPresent();

        //ifPresent()，接收一个函数式接口（通常用lambda表示），如果存在可以对数据进行相应的操作
        Optional.ofNullable(nullName).ifPresent(System.out::println);

        //orElse(),当参数为空时可以设置默认值
        String s = Optional.ofNullable(nullName).orElse("orElse(),设置一个默认的值");
        System.out.println("s = " + s);
    }
}
