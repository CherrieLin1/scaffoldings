package study.new8.optional;


import java.util.Locale;
import java.util.Optional;

/**
 * @ClassName OptionTestFilter
 * @Description 测试过滤器
 * @Author linshengyan
 * @Date 2022/5/9 1:00 下午
 */
public class OptionTestFilter {
    public static void main(String[] args) {
        //新建一个对象
        OptionPerson optionPerson = new OptionPerson("shenghvbjb", 12);

        //一般写法，判断对象不为null，将对象的名字转换为大写
        if (null != optionPerson) {
            if (null != optionPerson.getName()) {
                System.out.println(optionPerson.getName().toUpperCase(Locale.ROOT));
            }
        }

        System.out.println(Optional.ofNullable(optionPerson).map(x -> x.getName()).map(x -> x.toUpperCase(Locale.ROOT)).orElse(null));

    }
}
