package study.proxy;

/**
 * 租房接口 -- 抽象的角色
 * @author create by lsy on 2021/11/16 3:05 下午
 */
public interface Rent {
    /**
     * 租房
     */
    public void rent();

    public void getMoney();
}
