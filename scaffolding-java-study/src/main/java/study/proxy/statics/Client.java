package study.proxy.statics;


import study.proxy.Host;

/**
 * 客户找中介租房
 * @author create by lsy on 2021/11/16 3:11 下午
 */
public class Client {
    /**
     * 客户找中介租房，中介代理房东，客户无需和房东打交道
     * @param args
     */
    public static void main(String[] args) {
        Proxy proxy = new Proxy(new Host());
        proxy.rent();
        proxy.getMoney();

    }
}
