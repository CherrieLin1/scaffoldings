package study.proxy.statics;


import study.proxy.Host;
import study.proxy.Rent;

/**
 * @author create by lsy on 2021/11/16 3:07 下午
 */
public class Proxy implements Rent {

    /**
     * 房东
     */
    Host host;

    public Proxy() {
    }

    public Proxy(Host host) {
        this.host = host;
    }


    @Override
    public void rent() {
        seeHouse();
        host.rent();
        fare();

    }

    @Override
    public void getMoney() {
        beforeMoney();
        host.getMoney();
        afterMoney();
    }


    /**
     * 看房
     */
    private void seeHouse(){
        System.out.println("中介带人看房");
    }


    /**
     * 收中介费
     */
    private void fare(){
        System.out.println("收取中介费");
    }

    /**
     * 房东收钱前的操作
     */
    private void beforeMoney(){
        System.out.println("房东收钱前");
    }

    /**
     * 房东收钱后的操作
     */
    private void afterMoney(){
        System.out.println("房东收钱后");
    }
}
