package study.proxy.dynamic.jdk;


import study.proxy.Host;
import study.proxy.Rent;

/**
 * @author create by lsy on 2021/11/16 3:40 下午
 */
public class Client {
    public static void main(String[] args) {
        //真实的角色
        Host host = new Host();
        //代理实例的调用处理程序
        ProxyInvocationHandler proxyInvocationHandler = new ProxyInvocationHandler();
        //真实角色放入
        proxyInvocationHandler.setRent(host);
        //动态生成代理类
        Rent proxy = (Rent) proxyInvocationHandler.getProxy();
        //调用代理类的rent
        proxy.rent();
        proxy.getMoney();

    }
}
