package study.proxy;

/**
 * 房东-真实的角色
 * @author create by lsy on 2021/11/16 3:05 下午
 */
public class Host implements Rent {

    @Override
    public void rent() {
        System.out.println("房东租房");
    }

    @Override
    public void getMoney() {
        System.out.println("房东收钱");
    }
}

