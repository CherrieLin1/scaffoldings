package study.generics.genericsInterface;

/**
 * @ClassName GenericImplForT
 * @Description 不指定类型实现泛型类
 * @Author linshengyan
 * @Date 2022/3/25 4:56 下午
 */
public class GenericImplForT<T> implements Generic<T> {
    @Override
    public T method1() {
        return null;
    }
}
