package study.generics.genericsInterface;

/**
 * @ClassName Generic
 * @Description 泛型接口
 * @Author linshengyan
 * @Date 2022/3/25 4:53 下午
 */
public interface Generic<T> {
    /**
     * 定义一个方法，返回值为T
     *
     * @return
     */
    T method1();
}
