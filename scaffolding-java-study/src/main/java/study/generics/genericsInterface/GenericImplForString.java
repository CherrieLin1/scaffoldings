package study.generics.genericsInterface;

/**
 * @ClassName GenericImplForInteger
 * @Description 指定类型实现泛型类
 * @Author linshengyan
 * @Date 2022/3/25 4:55 下午
 */
public class GenericImplForString implements Generic<String>{
    @Override
    public String method1() {
        return "hello";
    }
}
