package study.generics;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Generic
 * @Description 泛型擦除
 * @Author linshengyan
 * @Date 2022/3/25 4:29 下午
 */
public class EraseGeneric {

    public static void main(String[] args) {
        test_list_add();
    }


    /**
     * 泛型的动态擦除
     * Java 的泛型是伪泛型，这是因为 Java 在运行期间，所有的泛型信息都会被擦掉，这也就是通常所说类型擦除 。
     *
     * @throws NoSuchMethodException
     */
    public static void test_list_add() {
        List<Integer> list = null;
        try {
            list = new ArrayList<>();
            list.add(1);
            //出错
//        list.add("la");
            Class<? extends List> aClass = list.getClass();
            Method add = aClass.getDeclaredMethod("add", Object.class);
            //但是通过反射添加是可以的
            //这就说明在运行期间所有的泛型信息都会被擦掉
            add.invoke(list, "la");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        System.out.println("list = " + list);
    }
}
