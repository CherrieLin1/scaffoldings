package study.generics.genericsClass;

/**
 * @ClassName Generic
 * @Description 泛型类
 * @Author linshengyan
 * @Date 2022/3/25 4:51 下午
 */
public class Generic<T> {
    private T key;

    public Generic(T t) {
        this.key = t;
    }

    public T getKey() {
        return key;
    }

    public static void main(String[] args) {
        //实例化一个泛型类
        Generic generic = new Generic<Integer>(5);
        System.out.println("generic.getKey() = " + generic.getKey());

        Generic generic1 = new Generic<String>("hello");
        System.out.println("generic1 = " + generic1.getKey());
    }
}
