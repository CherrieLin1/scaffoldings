package study.generics.genericsMethod;

/**
 * @ClassName GenericForMethod
 * @Description 定义泛型方法
 * @Author linshengyan
 * @Date 2022/3/25 4:58 下午
 */
public class GenericForMethod {
    public static void main(String[] args) {
        Integer[] integers = new Integer[]{1, 2, 3, 4};
        String[] strings = new String[]{"a", "b", "c"};
        printArray(integers);
        printArray(strings);
    }


    /**
     * 定义泛型方法有两个要点，第一：必须在方法申明的时候申明泛型，第二：必须在参数上申明泛型
     *
     * @param array
     * @param <E>
     */
    public static <E> void printArray(E[] array) {
        for (E element : array) {
            System.out.println(element);
        }
        System.out.println();
    }
}
