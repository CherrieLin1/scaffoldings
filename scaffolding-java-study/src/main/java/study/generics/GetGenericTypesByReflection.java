package study.generics;

import study.bean.User;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * @ClassName GenericTypes
 * @Description
 * @Author linshengyan
 * @Date 2022/7/21 1:39 下午
 */
public class GetGenericTypesByReflection {
    public void test01(Map<String, User> map, List<User> list) {

    }

    public Map<String, User> test02() {
        return null;
    }

    public static void main(String[] args) throws NoSuchMethodException {
        Class<GetGenericTypesByReflection> aClass = GetGenericTypesByReflection.class;


        System.out.println("=================获取test01的参数类型的实际类型=====================");
        Method test01 = aClass.getDeclaredMethod("test01", Map.class, List.class);
        //获取方法的参数类型
        Type[] genericParameterTypes = test01.getGenericParameterTypes();
        for (Type genericParameterType : genericParameterTypes) {
            System.out.println("genericParameterType = " + genericParameterType);
            if (genericParameterType instanceof ParameterizedType){
                Type[] actualTypeArguments = ((ParameterizedType) genericParameterType).getActualTypeArguments();
                for (Type actualTypeArgument : actualTypeArguments) {
                    System.out.println("actualTypeArgument = " + actualTypeArgument);
                }
            }
        }


        System.out.println("=================获取test02的返回类型的实际类型=====================");
        Method test02 = aClass.getMethod("test02");
        Type genericReturnType = test02.getGenericReturnType();
        System.out.println("genericReturnType = " + genericReturnType);
        if (genericReturnType  instanceof ParameterizedType){
            Type[] actualTypeArguments = ((ParameterizedType) genericReturnType).getActualTypeArguments();
            for (Type actualTypeArgument : actualTypeArguments) {
                System.out.println("actualTypeArgument = " + actualTypeArgument);
            }
        }


        /**
         * =================获取test01的参数类型的实际类型=====================
         * genericParameterType = java.util.Map<java.lang.String, study.bean.User>
         * actualTypeArgument = class java.lang.String
         * actualTypeArgument = class study.bean.User
         * genericParameterType = java.util.List<study.bean.User>
         * actualTypeArgument = class study.bean.User
         * =================获取test02的返回类型的实际类型=====================
         * genericReturnType = java.util.Map<java.lang.String, study.bean.User>
         * actualTypeArgument = class java.lang.String
         * actualTypeArgument = class study.bean.User
         */


    }
}
