package study.network;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

/**
 * @author create by lsy on 2021/11/16 11:20 上午
 */
public class TestInetAddress {
    public static void main(String[] args) {
        localHostAddress();
        getSockAddress();
    }


    public static void localHostAddress(){
        try {
            //查询本机地址三种方式
            InetAddress inetAddress1 = InetAddress.getByName("127.0.0.1");
            InetAddress inetAddress2 = InetAddress.getByName("localhost");
            InetAddress inetAddress3 = InetAddress.getLocalHost();

            System.out.println("inetAddress1 = " + inetAddress1);
            System.out.println("inetAddress2 = " + inetAddress2);
            System.out.println("inetAddress3 = " + inetAddress3);


            //查询网站ip地址
            InetAddress inetAddress4 = InetAddress.getByName("www.baidu.com");
            System.out.println("inetAddress4 = " + inetAddress4);

            //常用方法
            //规范的名字
            System.out.println("inetAddress4.getCanonicalHostName() = " + inetAddress4.getCanonicalHostName());

            //ip
            System.out.println("inetAddress4.getHostAddress() = " + inetAddress4.getHostAddress());

            //域名，或者自己电脑的名字
            System.out.println("inetAddress4.getHostName() = " + inetAddress4.getHostName());



        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }


    public static void getSockAddress(){

        InetSocketAddress socketAddress1 = new InetSocketAddress("127.0.0.1", 8080);
        InetSocketAddress socketAddress2 = new InetSocketAddress("localhost", 8080);

        System.out.println("socketAddress1 = " + socketAddress1);
        System.out.println("socketAddress2 = " + socketAddress2);

        //地址
        System.out.println("socketAddress2.getAddress() = " + socketAddress2.getAddress());
        //host名字
        System.out.println("socketAddress2.getHostName() = " + socketAddress2.getHostName());
        //端口
        System.out.println("socketAddress2.getPort() = " + socketAddress2.getPort());

    }
}
