package study.io.bio.oneClient;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;

/**
 * @ClassName Client
 * @Description
 * @Author linshengyan
 * @Date 2022/6/17 10:25 上午
 */
public class Client {
    public static void main(String[] args) {
        try {
            //1.创建Socket对象请求服务端的连接
            Socket socket = new Socket("127.0.0.1", 9999);
            //2.从Socket对象中获取一个字节输出流，客户端要和服务端联系，所以要调用自己的输出流
            OutputStream outputStream = socket.getOutputStream();
            //3.把字节输出流包装成一个打印流
            PrintStream printStream = new PrintStream(outputStream);
            printStream.println("服务端你好！");
            //4.刷新管道中的流，通过冲洗流，意味着清除流中可能存在或可能不存在的任何元素的流。它既不接受任何参数，也不返回任何值。
            printStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
