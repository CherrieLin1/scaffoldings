package study.io.bio.oneClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @ClassName Server
 * @Description
 * @Author linshengyan
 * @Date 2022/6/17 10:29 上午
 */
public class Server {
    public static void main(String[] args) {
        try {
            //1.定义一个ServerSocket对象进行服务端的端口注册
            ServerSocket serverSocket = new ServerSocket(9999);
            //2. 监听客户端的Socket连接请求
            Socket client = serverSocket.accept();
            //3.从socket管道中得到一个字节输入流对象
            InputStream inputStream = client.getInputStream();
            //4.把字节输入流包装成一个缓存字符输入流
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String msg;

            //以下只能接收一次，一次之后服务端的代码执行完了便会断开连接
            if ((msg = bufferedReader.readLine()) != null) {
                System.out.println("收到客户端的消息： " + msg);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
