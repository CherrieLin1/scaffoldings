package study.io.bio.multiClient;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

/**
 * @ClassName Client
 * @Description
 * @Author linshengyan
 * @Date 2022/6/17 10:53 上午
 */
public class Client1 {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("127.0.0.1", 9999);
            OutputStream outputStream = socket.getOutputStream();
            Scanner scanner = new Scanner(System.in);
            PrintStream printStream = new PrintStream(outputStream);
            while (true) {
                System.out.println("请说：");
                String msg = scanner.nextLine();
                printStream.println(String.format("客户端一：%s", msg));

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
