package study.io.bio.multiClient;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @ClassName Server
 * @Description
 * @Author linshengyan
 * @Date 2022/6/17 10:53 上午
 */
public class Server {
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(9999);
            while (true) {
                Socket accept = serverSocket.accept();
                //这里来一个客户端就会新建一个线程，随着客户端的增多，线程数越来越多，资源会慢慢的耗尽
                new ClientTaskThread(accept).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
