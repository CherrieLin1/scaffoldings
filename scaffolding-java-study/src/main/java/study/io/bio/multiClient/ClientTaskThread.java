package study.io.bio.multiClient;

import org.apache.commons.io.input.ReaderInputStream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

/**
 * @ClassName ClientTask
 * @Description
 * @Author linshengyan
 * @Date 2022/6/17 11:21 上午
 */
public class ClientTaskThread extends Thread {
    Socket socket;

    public ClientTaskThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            InputStream inputStream = socket.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String msg;
            while ((msg = bufferedReader.readLine()) != null) {
                System.out.println(Thread.currentThread().getName() + "-->" + String.format("收到客户端的消息：%s", msg));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
