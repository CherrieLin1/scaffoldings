package study.io.bio.threadPoolUse;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @ClassName Server
 * @Description 服务端，伪异步的思想
 * @Author linshengyan
 * @Date 2022/6/17 1:11 下午
 */
public class Server {
    public static void main(String[] args) {
        HandleSocketServerPool handleSocketServerPool = new HandleSocketServerPool(3, 10);
        try {
            ServerSocket serverSocket = new ServerSocket(9999);
            while (true) {
                System.out.println("循环");
                Socket accept = serverSocket.accept();
                handleSocketServerPool.exec(new SocketTask(accept));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
