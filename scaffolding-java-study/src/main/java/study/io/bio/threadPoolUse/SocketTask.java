package study.io.bio.threadPoolUse;

import org.apache.commons.io.input.ReaderInputStream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

/**
 * @ClassName SocketTask
 * @Description
 * @Author linshengyan
 * @Date 2022/6/17 1:06 下午
 */
public class SocketTask implements Runnable {
    Socket socket;

    public SocketTask(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            InputStream inputStream = socket.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String msg;
            while ((msg = bufferedReader.readLine()) != null) {
                System.out.println("收到消息:-->" + msg);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
