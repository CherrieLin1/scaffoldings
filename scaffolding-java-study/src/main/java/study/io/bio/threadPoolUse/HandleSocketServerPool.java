package study.io.bio.threadPoolUse;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName HandSocketPool
 * @Description
 * @Author linshengyan
 * @Date 2022/6/17 1:00 下午
 */
public class HandleSocketServerPool {
    /**
     * 创建一个线程池
     */
    ExecutorService executorService;

    public HandleSocketServerPool(int core, int max) {
        executorService = new ThreadPoolExecutor(core, max, 60, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(100));
    }

    public void exec(Runnable task) {
        executorService.execute(task);

    }
}
