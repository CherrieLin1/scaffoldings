package study.io.bio.oneClientAllContact;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @ClassName Server
 * @Description
 * @Author linshengyan
 * @Date 2022/6/17 10:46 上午
 */
public class Server {
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(9999);
            Socket client = serverSocket.accept();
            InputStream inputStream = client.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String msg;
            //循环，一直可以接收一个客户端的消息
            while ((msg = bufferedReader.readLine()) !=null){
                System.out.println("收到客户端的消息： " + msg);
            }
        } catch (IOException e) {


        }
    }
}
