package study.io.bio.fileUpload.type1;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @ClassName Server
 * @Description
 * @Author linshengyan
 * @Date 2022/6/17 1:32 下午
 */
public class Server {
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(9999);
            Socket socket = serverSocket.accept();
            InputStream inputStream = socket.getInputStream();

            String home = System.getProperty("user.home");
            FileOutputStream fileOutputStream = new FileOutputStream(new File(home + "/desktop/target/marry.jpg"));
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) != -1) {
                fileOutputStream.write(buffer, 0, len);
            }


            OutputStream outputStream = socket.getOutputStream();
            outputStream.write("照片已经传输完成了，可以断开链接了！".getBytes());

            fileOutputStream.close();
            inputStream.close();
            socket.close();
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
