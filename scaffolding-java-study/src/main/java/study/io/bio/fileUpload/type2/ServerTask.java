package study.io.bio.fileUpload.type2;

import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * @ClassName SreverTask
 * @Description
 * @Author linshengyan
 * @Date 2022/6/17 2:09 下午
 */
public class ServerTask extends Thread {
    Socket socket;

    public ServerTask(Socket socket) {
        this.socket = socket;

    }

    @Override
    public void run() {
        try {
            //1.得到一个数据输入流来读取客户端发送过来的数据
            DataInputStream dis = new DataInputStream(socket.getInputStream());

            //2.读取客户端发送过来的文件类型
            String suffix = dis.readUTF();
            System.out.println("成功接收到文件的类型:" + suffix);

            //3.定义一个字节输出管道，负责把客户端发来的文件数据写出去，注意文件的格式
            String home = System.getProperty("user.home");
            FileOutputStream fileOutputStream = new FileOutputStream(home + "/desktop/target/marry" + suffix);

            //4.从数据输入流中读取文件数据，写出到字节输出流中去
            byte[] buffer = new byte[1024];
            int len;
            while ((len = dis.read(buffer)) != -1) {
                fileOutputStream.write(buffer, 0, len);
            }

            //5.给客户端反馈，数据已经传输完毕了
            OutputStream outputStream = socket.getOutputStream();
            String msg = Thread.currentThread().getName() + "服务端接收数据完成！";
            outputStream.write(msg.getBytes());

            fileOutputStream.close();
            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
