package study.io.bio.fileUpload.type2;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.Socket;

/**
 * @ClassName Client
 * @Description
 * @Author linshengyan
 * @Date 2022/6/17 2:09 下午
 */
public class Client {
    public static void main(String[] args) {
        try {
            //1.创建socket
            Socket socket = new Socket("127.0.0.1", 9999);


            //3.字节输出流包装成数据输出流（DataOutputStream会分段传输）
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            dos.writeUTF(".jpg");

            //4.读取需要传输的文件流
            String home = System.getProperty("user.home");
            FileInputStream is = new FileInputStream(new File(home + "/desktop/target/marry.jpg"));

            //5.传输文件
            byte[] buffer = new byte[1024];
            int len;
            while ((len = is.read(buffer)) != -1) {
                dos.write(buffer, 0, len);
            }
            dos.flush();


            //6.通知服务器，自己已经传输完了
            socket.shutdownOutput();


            //7.得到服务器的反馈后，才关闭链接
            InputStream inputStream = socket.getInputStream();
            byte[] buffer2 = new byte[1024];
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int len2;
            while ((len2 = inputStream.read(buffer2)) != -1) {
                baos.write(buffer2, 0, len2);
            }
            System.out.println("收到服务器的反馈： " + baos.toString());


            //8.关闭链接
            is.close();
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
