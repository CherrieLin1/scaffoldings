package study.io.bio.fileUpload.type1;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * @ClassName Client
 * @Description
 * @Author linshengyan
 * @Date 2022/6/17 1:32 下午
 */
public class Client {
    public static void main(String[] args) {
        try {
            //1.创建连接
            Socket socket = new Socket("127.0.0.1", 9999);

            //2.创建输出流
            OutputStream outputStream = socket.getOutputStream();

            //3.读取文件
            String home = System.getProperty("user.home");
            File file = new File(home + "/desktop/source/marry.jpg");
            FileInputStream fileInputStream = new FileInputStream(file);

            //4.写出文件
            byte[] buffer = new byte[1024];
            int len;
            while ((len = fileInputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, len);
            }

            //5.socket传输完了，通知服务器
            socket.shutdownOutput();


            //6.确认服务器接收完毕
            InputStream inputStream = socket.getInputStream();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer2 = new byte[1024];
            int len2;
            while ((len2 = inputStream.read(buffer2)) != -1) {
                baos.write(buffer2, 0, len2);
            }
            System.out.println("baos.toString() = " + baos.toString());


            //7.关闭资源
            fileInputStream.close();
            baos.close();
            socket.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
