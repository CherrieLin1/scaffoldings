package study.io.bio.fileUpload.type2;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @ClassName Server
 * @Description
 * @Author linshengyan
 * @Date 2022/6/17 2:09 下午
 */
public class Server {
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(9999);
            while (true) {
                Socket accept = serverSocket.accept();
                new ServerTask(accept).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
