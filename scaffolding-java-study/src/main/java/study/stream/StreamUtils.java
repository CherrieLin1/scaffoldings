package study.stream;

import org.apache.commons.collections.CollectionUtils;
import study.bean.Dog;
import study.bean.Resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @ClassName StramUtils
 * @Description java流常用的方法
 * @Author linshengyan
 * @Date 2022/4/2 2:43 下午
 */
public class StreamUtils {

    /**
     * 测试类
     *
     * @param args
     */
    public static void main(String[] args) {
        List<Resource> list = getList();
        System.out.println("原始列表长度： " + list.size());

        //获取列表中的嵌套列表
        getFlatMap(getList());

        //获取获取列表对象中的某一个字段的不重复的值
        getDistinctParams(getList());

        compareMapAndForeach(getList());

    }

    /**
     * 构造数据，实例的主键Id唯一
     */

    public static List<Resource> getList() {
        List<Resource> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Resource resourceDO = new Resource();
            resourceDO.setId(i + 0L);
            resourceDO.setCode(UUID.randomUUID().toString());
            resourceDO.setBosId(100000L);
            resourceDO.setAmountType(1);
            resourceDO.setMerchantId(200000L);
            resourceDO.setVid(300000L);
            //产生一个0-10内的整数
            resourceDO.setResourceTemplateId(new Random().nextInt(10));
            System.out.println("模版id的值:" + resourceDO.getResourceTemplateId());
            List<Dog> dogs = new ArrayList<>(2);
            resourceDO.setDogs(dogs);
            for (int j = 0; j < 2; j++) {
                Dog dog = new Dog();
                dog.setName("狗狗:" + i + "-" + j);
                dog.setAge(i);
                dogs.add(dog);
            }
            list.add(resourceDO);
        }
        return list;
    }


    /**
     * 获取列表对象中的某一个字段的不重复的值
     * peek()是调试功能，一般接在stream()后面
     *
     * @param list
     */
    public static void getDistinctParams(List<Resource> list) {
        //获取不重复的指定字段的值
        List<Integer> collect = list.stream()
                .peek(x -> System.out.println("模版id:" + x.getResourceTemplateId()))
                .map(x -> x.getResourceTemplateId())
                .distinct()
                .collect(Collectors.toList());
        System.out.println("collect.size() = " + collect.size());
    }


    /**
     * 从列表中获取嵌套列表对象并统计数量
     *
     * @param list
     */
    public static void getFlatMap(List<Resource> list) {
        List<Dog> dogList = list.stream()
                .filter(resource -> null != resource && CollectionUtils.isNotEmpty(resource.getDogs()))
                .flatMap(resource -> resource.getDogs().stream())
                .collect(Collectors.toList());

        System.out.println("dogList的长度为：" + dogList.size());


        //打印出每个狗狗的名字
        list.stream()
                //流过滤
                .filter(resource -> null != resource && CollectionUtils.isNotEmpty(resource.getDogs()))
                //返回流
                .flatMap(resource -> resource.getDogs().stream())
                .collect(Collectors.toList())
                .forEach(x -> System.out.println(x.getName()));
    }


    /**
     * 根据某个字段分组
     *
     * @param list
     * @return
     */
    public static Map<Integer, List<Resource>> groupByTemplateId(List<Resource> list) {
        //流收集
        Map<Integer, List<Resource>> map = list.stream().collect(Collectors.groupingBy(Resource::getResourceTemplateId));
        return map;
    }


    /**
     * 获取某一个字段的所有值的列表（重复，不重复）
     *
     * @param list
     * @return
     */
    public static List<Long> getList(List<Resource> list) {
        //不去重
        List<Long> collect = list.stream().map(x -> x.getId()).collect(Collectors.toList());
        //去重
        Set<Long> set = list.stream().map(x -> x.getId()).collect(Collectors.toSet());
        return collect;
    }


    /**
     * 列表去重
     *
     * @param list
     * @return
     */
    public static List<Long> transferListToSet(List<Long> list) {
        return list.stream().collect(Collectors.toSet()).stream().collect(Collectors.toList());
    }


    /**
     * .map()map:方法的用途是将旧数据转换后变为新数据，是一种 1:1 的映射，每个输入元素按照规则转换成另一个元素
     * .forEach():它只接收不参数，没有返回值,然后在 Stream 的每一个元素上执行该表达式
     *
     * @param list
     */
    public static void compareMapAndForeach(List<Resource> list) {
        Map<String, Resource> map = new HashMap<>();
        //map.size == 0
        list.stream().map(x -> map.put(x.getId() + "_", x));
        //map.size = list.size()
        list.stream().forEach(x -> map.put(x.getId() + "_", x));

    }


}
