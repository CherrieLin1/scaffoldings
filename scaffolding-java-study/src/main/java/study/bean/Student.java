package study.bean;

import lombok.Data;
import study.reflection.FieldAnno;
import study.reflection.TableAnno;


/**
 * @ClassName Student
 * @Description
 * @Author linshengyan
 * @Date 2022/7/21 2:24 下午
 */
@TableAnno("这是一张student表")
@Data
public class Student {

    @FieldAnno(columnName = "主健", type = "int", length = 3)
    private int id;

    @FieldAnno(columnName = "姓名", type = "string", length = 10)
    private String name;
}
