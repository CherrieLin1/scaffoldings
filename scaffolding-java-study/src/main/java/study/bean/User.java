package study.bean;

import lombok.Data;

/**
 * @ClassName User
 * @Description
 * @Author linshengyan
 * @Date 2022/7/21 10:17 上午
 */
@Data
public class User {
    private String name;

    /**
     * 1，男生   2，女生
     */
    private int gender;

    private int age;

    public User(String name, int gender, int age) {
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    /**
     * 如果类中有其他的构造方法，我们必须手动设置无参构造
     * 如果没有，默认有构造方法
     */
    public User() {
    }
}
