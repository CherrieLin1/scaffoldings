package study.bean;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @ClassName Resource
 * @Description
 * @Author linshengyan
 * @Date 2022/4/2 2:44 下午
 */
@Data
public class Resource {

    public Long publicParam;
    /**
     * ID
     */
    private Long id;

    /**
     * 租户编码
     */
    private String tcode;


    /**
     * 资源码
     */
    private String code;

    /**
     * 增送人
     */
    private Long fromUser;

    /**
     * 发放渠道
     */
    private Long sendChannel;

    /**
     * 发放渠道Id
     */
    private String sendChannelId;

    /**
     * 是否可赠：0-不可赠，1-可赠
     */
    private Integer canGift;

    /**
     * 是否可分享：0-不可分享，1-可分享
     */
    private Integer canShare;

    /**
     * 资源模板名称
     */
    private String name;

    /**
     * 外部映射id
     */
    private String thirdTemplateId;

    /**
     * 优惠类型， 满减等
     */
    private Integer promotionType;

    /**
     * 资源二维码
     */
    private String qrcode;

    /**
     * 资源模板id
     */
    private Integer resourceTemplateId;

    /**
     * 来源
     */
    private String source;

    /**
     * 状态值 0：不可用，1：可用
     */
    private Integer status;

    /**
     * 库存id
     */
    private Long stockId;

    /**
     * 子名称
     */
    private String subName;

    /**
     * 资源类型，1：券，2：码，3：组合资源
     */
    private Integer type;

    /**
     * 11：线上券 12：线下券 21：一人一码 22：通用码
     */
    private Integer subType;

    /**
     * 优惠的类型   1：减钱    2:打折   3:一口价（兑换）
     */
    private Integer amountType;

    /**
     * 优惠金额
     */
    private Long useAmount;


    /**
     * 门槛的类型  1:满件     2：满金额
     */
    private Integer thresholdType;

    /**
     * 使用门槛
     */
    private Long useThreshold;

    /**
     * 使用开始时间
     */
    private Date startTime;

    /**
     * 使用结束时间
     */
    private Date endTime;
    /**
     * 买家id
     */
    private Long wid;
    /**
     * 资源描述
     */
    private String desc;

    /**
     * 扩展字段
     */
    private String ext;

    /**
     * 余额数量 比如可以多次使用的码，券等的剩余使用次数
     */
    private Long balance;

    /**
     * 初始数量
     */
    private Long initBalance;

    /**
     * 用户open_id
     */
    private String openId;
    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 商家id
     */
    private Long merchantId;

    /**
     * 产品实例id
     */
    private Long sourceProdInstanceId;
    /**
     * 数据在某产品ID下生成
     */
    private Long sourceProductId;
    /**
     * 数据在某产品的具体版本下生成
     */
    private Long sourceProductVersionId;
    /**
     * 树节点
     */
    private Long bosId;

    /**
     * 资源模板中的vid
     */
    private Long vid;

    /**
     * 创建券的节点（sellerDTO中获取）
     */
    private Long sVid;

    /**
     * 扣减
     */
    private Long deductBalance;

    /**
     * 是否删除  0 未删除  1已删除
     */
    private Integer isDeleted;

    /**
     * 预发单标识
     */
    private String applyNo;

    /**
     * 核销vid
     */
    private Long usingVid;

    /**
     * 订单id
     */
    private String orderNo;

    List<Dog> dogs;

    public Resource() {
    }

    public Resource(Long publicParam, Long id, String tcode) {
        this.publicParam = publicParam;
        this.id = id;
        this.tcode = tcode;
    }
}
