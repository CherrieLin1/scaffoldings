package study.bean;

import lombok.Data;

/**
 * @ClassName Dog
 * @Description
 * @Author linshengyan
 * @Date 2022/4/2 2:45 下午
 */
@Data
public class Dog implements Test{
    private String name;

    private int age;
}
