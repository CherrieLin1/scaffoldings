package study.string;

/**
 * @ClassName StringformatUtils
 * @Description
 * @Author linshengyan
 * @Date 2022/8/11 8:35 上午
 */
public class StringformatUtils {
    public static void main(String[] args) {
        Integer temCodeLength = 20;
        long number = 108777282828287628L;
        //%0 + temCodeLength表示格式化以后的字符串长度，如果不足，前面补0
        //d是个占位符，会被参数a所替换。
        String str = String.format("%0" + temCodeLength + "d", number);
        System.out.println("str = " + str);
        str = "CC" + str;
        System.out.println("str = " + str);
    }
}
