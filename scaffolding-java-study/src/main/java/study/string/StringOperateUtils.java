package study.string;

import org.apache.commons.lang3.StringUtils;

/**
 * @ClassName StringUtils
 * @Description string常用的方法
 * @Author linshengyan
 * @Date 2022/4/2 2:18 下午
 */
public class StringOperateUtils {
    private static String SEPARATOR = "_";

    public static void main(String[] args) {
        replaceDefaultPart();
    }


    /**
     * String由下划线连接，替换第二个下划线和第三个下划线之间的内容
     */
    public static void replaceDefaultPart() {
        String source = "1_7789_4223092_4000076828354_3165_1";
        Long wid = 66668888L;

        //获取指定字符第几次出现的位置
        int start = StringUtils.ordinalIndexOf(source,SEPARATOR,2);
        int end = StringUtils.ordinalIndexOf(source,SEPARATOR,3);

        //替换指定的字符
        StringBuilder stringBuilder = new StringBuilder(source);
        stringBuilder.replace(start + 1, end, wid.toString());
        System.out.println("替换前：" + source);
        System.out.println("替换后：" + stringBuilder.toString());
    }
}
