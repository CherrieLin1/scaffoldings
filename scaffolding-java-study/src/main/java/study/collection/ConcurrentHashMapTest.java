package study.collection;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName ConcurrentHashMapTest
 * @Description concurrentHashMap功能测试类
 * @Author linshengyan
 * @Date 2022/4/2 10:10 上午
 */
public class ConcurrentHashMapTest {
    private static ConcurrentHashMap<String, Long> map = new ConcurrentHashMap<>();
    static Map map1 = new HashMap<String,Integer>();
    static final int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }


    public static void main(String[] args) {
        int i = 5;
        if (i > 4){
            System.out.println("---1");
        }else if (i > 3){
            System.out.println("----2");
        }else {
            System.out.println("-----3");
        }
        map1.put("hello",3);
        hash("hello");

        test_basicMethod();
    }

    /**
     * 第一次put，返回为null
     * 如果key存在，返回旧不value
     */
    public static void test_basicMethod() {
        //first put ,return null
        Long firstPut = map.put("t1", 1L);
        System.out.println("firstPut = " + firstPut);
        System.out.println("map.get(\"t1\") = " + map.get("t1"));

        //覆盖key,返回旧数据
        Long sameKeyPutResult = map.put("t1", 2L);
        System.out.println("sameKeyPutResult = " + sameKeyPutResult);


        Long sameKVResult = map.put("t1", 2L);
        System.out.println("sameKVResult = " + sameKVResult);


    }
}
