package study.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @ClassName TestReflectionMethods
 * @Description Field、Method、Constructor、Superclass、Interface、Annotation
 * @Author linshengyan
 * @Date 2022/7/21 10:04 上午
 */
public class TestReflectionMethods {
    public static void main(String[] args) throws Exception {

        Class<?> c1 = Class.forName("study.bean.Resource");

        //获得包名+类名
        System.out.println(c1.getName());
        //类名
        System.out.println(c1.getSimpleName());


        System.out.println("===============获得类的属性===========");
        //只能找到public 修饰的属性
        Field[] fields = c1.getFields();
        for (Field field : fields) {
            System.out.println(field);
        }
        //获取所有的属性
        fields = c1.getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field);
        }

        //获取指定属性的值
        //Field name = c1.getField("name");报错
        Field name = c1.getDeclaredField("name");
        System.out.println(name);


        System.out.println("==============获得类的方法============");
        ////获取本类及父类的全部public 方法
        Method[] methods = c1.getMethods();
        for (Method method : methods) {
            System.out.println("getMethods（）：" + method);
        }
        System.out.println("============获取本类的所有方法==============");
        methods = c1.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println("getDeclaredMethods（）：" + method);
        }
        System.out.println("============获取指定get方法==============");
        Method getName = c1.getMethod("getName");
        System.out.println(getName);


        System.out.println("============获取指定set方法名称，需要加参数类型==============");
        Method setName = c1.getMethod("setName", String.class);
        System.out.println(setName);

        System.out.println("============getConstructors==============");
        Constructor<?>[] constructors = c1.getConstructors();
        for (Constructor<?> constructor : constructors) {
            System.out.println("getConstructors" + constructor);
        }
        System.out.println("============getDeclaredConstructors================");
        constructors = c1.getDeclaredConstructors();
        for (Constructor<?> constructor : constructors) {
            System.out.println("getDeclaredConstructors（）：" + constructor);
        }


        System.out.println("===========获取指定构造器===========");
        Constructor<?> declaredConstructor = c1.getDeclaredConstructor(Long.class, Long.class, String.class);
        System.out.println("declaredConstructor" + declaredConstructor);


    }

}
