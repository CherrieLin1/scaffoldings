package study.reflection;

/**
 * @ClassName Test07
 * @Description
 * @Author linshengyan
 * @Date 2022/7/20 9:20 下午
 */
public class TestClassLoad {
    public static void main(String[] args) throws ClassNotFoundException {
        //获取系统的类加载器
        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
        //sun.misc.Launcher$AppClassLoader@18b4aac2
        System.out.println("systemClassLoader = " + systemClassLoader);

        //获取系统类加载器的父类加载器->扩展类加载器
        ClassLoader systemClassLoaderParent = systemClassLoader.getParent();
        //sun.misc.Launcher$ExtClassLoader@76ed5528
        System.out.println("systemClassLoaderParent = " + systemClassLoaderParent);


        //获取扩展类加载器的父类加载器->根加载器（C++）
        ClassLoader systemClassLoaderParentParent = systemClassLoaderParent.getParent();
        //null,系统类加载器无法直接获取
        System.out.println("systemClassLoaderParentParent = " + systemClassLoaderParentParent);


        //测试当前类是哪个加载器加载的
        ClassLoader testO7ClassLoader = Class.forName("study.reflection.TestClassLoad").getClassLoader();
        //sun.misc.Launcher$AppClassLoader@18b4aac2
        System.out.println("testO7ClassLoader = " + testO7ClassLoader);


        //测试JDK内部哪个加载器加载的
        //null,系统类加载器无法直接获取
        ClassLoader objectClassLoad = Class.forName("java.lang.Object").getClassLoader();
        System.out.println("objectClassLoad = " + objectClassLoad);
    }
}
