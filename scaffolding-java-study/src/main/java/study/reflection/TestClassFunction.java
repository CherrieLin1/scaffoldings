package study.reflection;

import study.bean.User;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @ClassName TestClassFunction
 * @Description 测试class对象的作用
 * 1.当类有无参数构造的时候创建对象实例
 * -调用Class对象的newlnstance()方法
 * -类必须有一个无参数的构造器。
 * -类的构造器的访问权限需要足够
 * ======
 * 构造完对象以后：
 * -通过反射调用普通方法，设置属性
 * -或者通过反射直接操作属性
 *
 * @Author linshengyan
 * @Date 2022/7/21 10:18 上午
 */
public class TestClassFunction {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException, NoSuchFieldException {
        Class<?> aClass = Class.forName("study.bean.User");
        System.out.println("============newInstance通过无参构造创造User对象，并通过方法操作对象属性===========");
        //类中必须有无参构造
        User user1 = (User) aClass.newInstance();
        System.out.println("user1 = " + user1);
        Method setName = aClass.getDeclaredMethod("setName", String.class);
        setName.invoke(user1,"生燕直接通过操作方法set的值");
        System.out.println("user1.getName() = " + user1.getName());


        System.out.println("============newInstance通过无参构造创造User对象，并通过反射直接操作对象属性===========");
        //类中必须有无参构造
        User user2 = (User) aClass.newInstance();
        System.out.println("user2 = " + user2);
        Field name = aClass.getDeclaredField("name");

        name.setAccessible(true);
        name.set(user2,"生燕直接通过操作属性设置的值");
        System.out.println("user2.getName() = " + user2.getName());

        System.out.println("============通过指定构造方法创建对象，并设置对应的值===========");
        Constructor<?> declaredConstructor = aClass.getDeclaredConstructor(String.class, int.class, int.class);
        User user3 = (User) declaredConstructor.newInstance("生燕通过执行构造方法创建的对象",1,30);
        System.out.println("user3 = " + user3);


    }
}
