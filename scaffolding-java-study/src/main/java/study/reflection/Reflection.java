package study.reflection;

import study.bean.Dog;
import study.bean.Resource;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author create by lsy on 2021/10/11 7:08 下午
 */
public class Reflection {
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        //1.获取不同对象的class
        getReflect();

        //2.不同的方法获取class对象
        getClassByReflect();

        //3.通过类获取类相关的信息，包括属性，方法
        getInformationFromCLass();

        //4.通过对象获取类
        getClassByObject();

    }

    /**
     * 通过类获取类相关的信息，包括属性，方法
     */
    public static void getInformationFromCLass()  {
        //1.获取类，无论哪种方法，以下展示一种方法
        Class<? extends Resource> resourceCLass = new Resource().getClass();

        //获取public的属性
        Field[] fields = resourceCLass.getFields();
        for (int i = 0; i < fields.length; i++) {
            System.out.println("public fields  " + i + " = " + fields[i]);

        }

        //获取所有的属性
        Field[] declaredFields = resourceCLass.getDeclaredFields();
        for (int i = 0; i < declaredFields.length; i++) {
            System.out.println("declaredFields  " + i + " = " + declaredFields[i]);
        }

    }

    /**
     * 通过对象获取类
     */
    public static void getClassByObject() {
        Dog dog = new Dog();
        Class<? extends Dog> aClass = dog.getClass();
        System.out.println("aClass.getSuperclass() = " + aClass.getSuperclass());

        //获取类全限定名
        String name = aClass.getName();
        System.out.println("name = " + name);

        //获取类名
        String simpleName = aClass.getSimpleName();
        System.out.println("simpleName = " + simpleName);

    }


    /**
     * 通过反射获取类
     */
    public static void getClassByReflect() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class aClass = Class.forName("study.bean.Dog");
        Dog dog = (Dog) aClass.newInstance();
        Class superclass = aClass.getSuperclass();
        String name1 = aClass.getName();
        System.out.println("name1 = " + name1);
        Class[] interfaces = aClass.getInterfaces();
        System.out.println("interfaces = " + interfaces);


        ClassLoader classLoader = aClass.getClassLoader();
        System.out.println("classLoader = " + classLoader);

        Constructor[] constructors = aClass.getConstructors();
        System.out.println("constructors = " + constructors);

        Field[] declaredFields = aClass.getDeclaredFields();
        System.out.println("declaredFields = " + declaredFields);

        Field[] fields = aClass.getFields();
        System.out.println("fields = " + fields);

        Method[] methods = aClass.getMethods();
        System.out.println("methods = " + methods);


        Method[] declaredMethods = aClass.getDeclaredMethods();
        System.out.println("declaredMethods = " + declaredMethods);

        Annotation[] annotations = aClass.getAnnotations();
        for (Annotation annotation : annotations) {
            System.out.println("annotation.annotationType() = " + annotation.annotationType());
        }


        //获取类全限定名
        String name = aClass.getName();
        System.out.println("name = " + name);

        //获取类名
        String simpleName = aClass.getSimpleName();
        System.out.println("simpleName = " + simpleName);
    }

    /**
     * 获取不同对象的class类
     */
    public static void getReflect() {
        Class c1 = Object.class;
        Class c2 = Comparable.class;
        Class c3 = String[].class;
        Class c4 = int[][].class;
        Class c5 = Integer.class;
        Class c6 = Void.class;
        Class c7 = Override.class;
        Class c8 = Class.class;
        System.out.println("c1 = " + c1);
        System.out.println("c2 = " + c2);
        System.out.println("c3 = " + c3);
        System.out.println("c4 = " + c4);
        System.out.println("c5 = " + c5);
        System.out.println("c6 = " + c6);
        System.out.println("c7 = " + c7);
        System.out.println("c8 = " + c8);


        int[] a = new int[10];
        int[] b = new int[100];

        int[] array1 = {1, 2, 3, 4, 5};
        int[] array2 = {1, 2, 3, 4, 5};
        //true
        System.out.println(array1.getClass().hashCode() == array2.getClass().hashCode());
        //true,只要元素类型与纬度一样，底层的class就是一样的
        System.out.println(a.getClass().hashCode() == b.getClass().hashCode());

    }
}
