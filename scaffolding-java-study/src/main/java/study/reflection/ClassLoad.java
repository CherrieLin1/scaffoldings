package study.reflection;

/**
 * 模拟类的加载过程
 *
 * @author create by lsy on 2021/10/11 7:25 下午
 */
public class ClassLoad {
    public static void main(String[] args) {
        A a = new A();
        System.out.println("a.m = " + A.m);
    }

    /**
     * 1.类加载到方法区，方法区对应类的一颗基本信息，类名，变量名，方法名，静态方法，常量池，代码等等
     * 2.类加载到内存，在堆中，class对象在堆中
     * 3.链接阶段，为对象分配初始值，m=0
     * 4.new A对象在堆中
     * 5.合并static方法，这里静态方法和静态代码块的位置不同，执行的结果也不同，需要注意
     */
}

class A {

    static int m = 100;

    static {
        m = 300;
        System.out.println("A类的静态代码块");
    }



    public A() {
        System.out.println("A类的构造方法");
    }
}
