package study.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * @ClassName GetAnnotationByReflection
 * @Description
 * @Author linshengyan
 * @Date 2022/7/21 2:20 下午
 */
public class GetAnnotationByReflection {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException {

        //1.获取Class类对象
        Class<?> aClass = Class.forName("study.bean.Student");

        //2.通过反射获取注解
        Annotation[] annotations = aClass.getAnnotations();
        for (Annotation annotation : annotations) {
            System.out.println("annotation = " + annotation);
        }

        //获取注解的value值
        TableAnno annotationTableAnno = aClass.getAnnotation(TableAnno.class);
        System.out.println("annotationTableAnno.value() = " + annotationTableAnno.value());
        System.out.println("annotationTableAnno.annotationType() = " + annotationTableAnno.annotationType());

        //获取类指定的注解
        //暴力反射
        Field name = aClass.getDeclaredField("name");
        FieldAnno annotation = name.getAnnotation(FieldAnno.class);
        System.out.println("annotation.value() = " + annotation.value());
        System.out.println("annotation.columnName() = " + annotation.columnName());
        System.out.println("annotation.length() = " + annotation.length());
    }
}
