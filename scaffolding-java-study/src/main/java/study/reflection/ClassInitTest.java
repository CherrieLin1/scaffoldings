package study.reflection;

/**
 * @ClassName ClassInitTest
 * @Description 测试类什么时候初始化
 * @Author linshengyan
 * @Date 2022/4/4 4:19 下午
 */
public class ClassInitTest {
    public static void main(String[] args) throws ClassNotFoundException {

        /**
         * 1.主动初始化子类，子类和父类的静态代码块以及构造方法都被加载，顺序：父--》子
         * 打印结果：
         * 父类-静态代码块被加载
         * 子类-静态代码块被加载
         * 父类-构造方法被加载
         * 子类-构造方法被加载
         */
        Son son = new Son();


        /**
         * 2.通过class.forName记载，只加载静态方法，顺序：父--》子
         * 打印结果:
         * 父类-静态代码块被加载
         * 子类-静态代码块被加载
         * aClass.getName() = study.reflection.Son
         */
        Class<?> aClass = Class.forName("study.reflection.Son");
        System.out.println("aClass.getName() = " + aClass.getName());


        /**
         * 3.申明一个对象数组，父子类都不会被加载
         * 打印结果：
         * 无
         *
         */
        Son[] sons = new Son[1];



        /**
         * 4.调用子类的常量，父子类都不会被加载
         * 打印结果：
         * 无
         */
        System.out.println(Son.m);


        /**
         * 5.类的静态代码块在调用静态常量的时候不加载，但是调用静态变量的时候会加载？？？
         */
        System.out.println("Father.m = " + Father.m);
        System.out.println("Father.b = " + Father.b);


    }
}


class Father {
    /**
     * 静态常量
     */
    static final int b = 20;

    public Father() {
        System.out.println("父类-构造方法被加载");
    }

    static {
        System.out.println("父类-静态代码块被加载");
        m = 300;
    }

    /**
     * 父类-静态变量
     */
    static int m = 100;
}


class Son extends Father {
    static {
        System.out.println("子类-静态代码块被加载");
    }

    public Son() {
        System.out.println("子类-构造方法被加载");
    }

    /**
     * 子类，静态常量
     */
    static final int m = 50;
}
