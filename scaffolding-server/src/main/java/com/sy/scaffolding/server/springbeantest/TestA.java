package com.sy.scaffolding.server.springbeantest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @ClassName TestA
 * @Description
 * @Author linshengyan
 * @Date 2022/8/16 9:22 下午
 */
@Component
//@Order(1)
public class TestA {

    @Autowired
    private TestB testB;

    public String a1() {
        return testB.b1();
    }

    public String a2() {
        return "hello a2";
    }
}
