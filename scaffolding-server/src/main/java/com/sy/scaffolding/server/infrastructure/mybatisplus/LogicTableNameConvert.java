package com.sy.scaffolding.server.infrastructure.mybatisplus;

import com.google.common.collect.Maps;
import com.sy.scaffolding.dao.entity.ResourceCodeDO;
import com.sy.scaffolding.dao.entity.ResourceDO;
import com.sy.scaffolding.dao.entity.ResourceTemplateDO;
import org.apache.commons.lang.StringUtils;


import java.util.Map;

/**
 * 实体入库前生成DB时会根据类型来获取对应的逻辑表名
 * 用于id序列表生成所用，所以，每个实体的逻辑表名称都要注册进来
 *
 * @author wanwan.yao
 */
public class LogicTableNameConvert {
    /**
     * Class类型转换器
     */
    private final static Map<Class, String> TYPE_MAP = Maps.newHashMap();

    static {
//        TYPE_MAP.put(BuyerLimitDO.class, "buyer_limit");
//        TYPE_MAP.put(MetaElementDO.class, "meta_element");
//        TYPE_MAP.put(MetaTemplateDO.class, "meta_template");
//        TYPE_MAP.put(OperateLogsDO.class, "operate_logs");
        TYPE_MAP.put(ResourceCodeDO.class, "resource_code");
        TYPE_MAP.put(ResourceDO.class, "resource");
//        TYPE_MAP.put(ResourceSyncDO.class, "resource_sync");
        TYPE_MAP.put(ResourceTemplateDO.class, "resource_template");
//        TYPE_MAP.put(ResourceTemplateParticipateDO.class, "resource_template_participate");
//        TYPE_MAP.put(ResourceTemplateStockDO.class, "resource_template_stock");
//        TYPE_MAP.put(ResourceTemplateSyncDO.class, "resource_template_sync");
//        TYPE_MAP.put(ResourceUseLogDO.class, "resource_use_log");
//        TYPE_MAP.put(SellerLimitDO.class, "seller_limit");
//        TYPE_MAP.put(ResourceTemplateCodeSettingDO.class, "resource_template_code_setting");
//        TYPE_MAP.put(IndexResourceDO.class, "index_resource");
//        TYPE_MAP.put(IndexResourceSyncDO.class, "index_resource_sync");
//        TYPE_MAP.put(ResourceTemplateBindChannelDO.class, "resource_template_bind_channel");
//        TYPE_MAP.put(ResourceTemplateReceiveRequestInfoDO.class, "resource_template_receive_request_info");
//        TYPE_MAP.put(ResourceTemplateBasicDTO.class, "resource_template_basic");
    }


    /**
     * 获取逻辑表名称，如果没找到会报错
     *
     * @param object
     * @return
     */
    public static String getLogicTableNameByObject(Object object) {
        String logicTableName = TYPE_MAP.get(object.getClass());
        if (StringUtils.isEmpty(logicTableName)) {
            throw new RuntimeException("未找到此Object对应的逻辑表名称，请检查，class信息：" + object.getClass());
        }
        return logicTableName;
    }
}
