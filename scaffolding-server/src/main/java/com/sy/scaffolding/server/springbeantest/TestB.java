package com.sy.scaffolding.server.springbeantest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName TestB
 * @Description
 * @Author linshengyan
 * @Date 2022/8/16 9:22 下午
 */
@Component
public class TestB {
    @Autowired
    private TestA testA;

    public String  b1(){

        return testA.a2();
    }
}
