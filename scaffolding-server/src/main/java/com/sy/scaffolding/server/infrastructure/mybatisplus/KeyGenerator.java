package com.sy.scaffolding.server.infrastructure.mybatisplus;

import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;

import com.sy.scaffolding.generatorid.service.IdSequenceService;
import com.sy.scaffolding.server.infrastructure.utils.SpringContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * mybaits-plus 自定义主键生成器
 * 自定义生成器的方式很多，这是最简单的方式，申明为Bean，并被spring扫描到
 */
@Component
@Slf4j
public class KeyGenerator implements IdentifierGenerator {

    IdSequenceService idSequenceService;

    public void init() {
        if (idSequenceService == null) {
            idSequenceService = SpringContextUtils.getBean(IdSequenceService.class);
        }
    }

    @Override
    public Number nextId(Object entity) {
        init();

        String logicTableName = LogicTableNameConvert.getLogicTableNameByObject(entity);
        long primaryKey = idSequenceService.nextId(logicTableName);
        log.info(new StringBuilder("当前表：").append(primaryKey).append("，获取得到的id：").append(primaryKey).toString());
        return primaryKey;
    }
}