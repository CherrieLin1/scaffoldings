package com.sy.scaffolding.boot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @ClassName TestController
 * @Description
 * @Author linshengyan
 * @Date 2022/7/25 2:39 下午
 */
@Controller
public class TestController {
    private static AtomicInteger stackInteger = new AtomicInteger(0);
    private static AtomicInteger heapInteger = new AtomicInteger(0);

    @RequestMapping("/testHeapOut")
    public void testHeap() {
        System.out.println("test Heap");
        List<OOMobject> list = new ArrayList<OOMobject>();
        while (true) {
            list.add(new OOMobject());
            int i = heapInteger.addAndGet(1);
            System.out.println("=============heap 循环调用" + i + "：次了==========");
        }

    }


    @RequestMapping("/testStackOut")
    public void testStack() {
        System.out.println("test stack");
        testStackLoop();
    }

    public void testStackLoop() {
        System.out.println("=====testStack=====");
        int i = stackInteger.addAndGet(1);
        System.out.println("=============stack 循环调用" + i + "：次了==========");

        testStackLoop();
    }


    class Person {
        int id;
        String name;

        public Person(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public Person(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("Person{");
            sb.append("id=").append(id);
            sb.append(", name='").append(name).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }
}
