package com.sy.scaffolding.boot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Map;

/**
 * @ClassName ApplicationContext
 * @Description 启动类
 * @Author linshengyan
 * @Date 2022/3/15 10:52 上午
 */
@MapperScan(basePackages = {"com.sy.scaffolding.dao.mapper","com.sy.scaffolding.generatorid.dao"})
@SpringBootApplication(scanBasePackages = {"com.sy.scaffolding"})
public class ApplicationContext {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ApplicationContext.class, args);
        //查询环境变量，idea启动时可以在【Edit Configuration--Environment--EnvironmentVariables中设置指定的环境变量，但是单测时，这些环境变量值不会被加载>】
        //此时需要在SpringBoot的单测类中，自定义单测时的环境变量：见MySpringJUnit4ClassRunner类
        Map<String, String> getenv = System.getenv();
        for (Map.Entry<String, String> stringStringEntry : getenv.entrySet()) {
            System.out.println("环境变量：" + stringStringEntry.getKey() + "=" + stringStringEntry.getValue());
        }

        System.out.println("启动成功：" + context.getEnvironment());
    }
}
