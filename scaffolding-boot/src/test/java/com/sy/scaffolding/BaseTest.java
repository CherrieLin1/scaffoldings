package com.sy.scaffolding;

import com.sy.scaffolding.boot.ApplicationContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.UUID;

/**
 * @ClassName BaseTest
 * @Description
 * @Author linshengyan
 * @Date 2022/3/16 3:02 下午
 */
@RunWith(MySpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ApplicationContext.class)
public class BaseTest {
    static {
        System.setProperty("env", "DEV");
    }
}
