package com.sy.scaffolding.springbeantest;

import com.sy.scaffolding.BaseTest;
import com.sy.scaffolding.server.springbeantest.TestA;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @ClassName SpringBeanTest
 * @Description
 * @Author linshengyan
 * @Date 2022/8/16 9:29 下午
 */
public class SpringBeanTest extends BaseTest {

    @Autowired
    private TestA testA;

    @Test
    public void testLoop(){
        System.out.println(testA.a1());
    }


}
