package com.sy.scaffolding.elasticsearch;


import com.alibaba.fastjson.JSON;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.sy.scaffolding.BaseTest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.sy.scaffolding.elasticsearch.bean.User;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

class EsutilsTests extends BaseTest {


    @Autowired
    private RestHighLevelClient restHighLevelClient;


    @Test
    void contextLoads() {
    }


    /**
     * 创建索引
     * kuang_index
     *
     * @throws IOException
     */
    @Test
    public void testCreateIndex() throws IOException {
        CreateIndexRequest request = new CreateIndexRequest("kuang_index");
        CreateIndexResponse createIndexResponse
                = restHighLevelClient.indices().create(request, RequestOptions.DEFAULT);
        System.out.println(createIndexResponse.isAcknowledged());
    }


    /**
     * 测试索引是否存在
     */
    @Test
    public void existIndex() throws IOException {
        GetIndexRequest getIndexRequest = new GetIndexRequest("kuang_index");
        boolean exists = restHighLevelClient.indices().exists(getIndexRequest, RequestOptions.DEFAULT);
        System.out.println("exists = " + exists);
    }


    /**
     * 删除索引
     *
     * @throws IOException
     */
    @Test
    public void deleteIndex() throws IOException {
        DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest("kuang_index");
        AcknowledgedResponse response = restHighLevelClient.indices().delete(deleteIndexRequest, RequestOptions.DEFAULT);
        System.out.println("response.isAcknowledged() = " + response.isAcknowledged());

    }


    /**
     * 新增文档
     *
     * @throws IOException
     */
    @Test
    public void addDoc() throws IOException {
        User user = new User("狂神说", 3, "");
        IndexRequest indexRequest = new IndexRequest("kuang_index");
        indexRequest.id("1");
        indexRequest.timeout(TimeValue.timeValueSeconds(1));
        indexRequest.timeout("1s");
        indexRequest.source(JSON.toJSONString(user), XContentType.JSON);
        IndexResponse indexResponse = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
        System.out.println("indexResponse.status() = " + indexResponse.status());
    }


    /**
     * 判断对应文档的id是否存在
     *
     * @throws IOException
     */
    @Test
    public void indexExistDoc() throws IOException {
        GetRequest getRequest = new GetRequest("kuang_index", "1");
        //不获取source上下文
        getRequest.fetchSourceContext(new FetchSourceContext(false));
        getRequest.storedFields("_none_");

        //判断此id是否存在
        boolean exists = restHighLevelClient.exists(getRequest, RequestOptions.DEFAULT);
        System.out.println("exists = " + exists);
    }


    /**
     * 根据索引和文档id获取文档
     *
     * @throws IOException
     */
    @Test
    public void getDoc() throws IOException {
        GetRequest getRequest = new GetRequest("kuang_index", "1");
        GetResponse documentFields = restHighLevelClient.get(getRequest, RequestOptions.DEFAULT);
        long version = documentFields.getVersion();
        System.out.println("version = " + version);
        System.out.println("documentFields.getSourceAsString() = " + documentFields.getSourceAsString());
    }


    /**
     * 更新文档,并发更新一个文档
     *
     * @throws IOException
     */
    @Test
    public void updateDoc_multi() throws IOException, InterruptedException {
        CountDownLatch count = new CountDownLatch(10);
        ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat("lin-test-%s").build();
        ExecutorService executorService = new ThreadPoolExecutor(100, 500, 2, TimeUnit.MINUTES, new LinkedBlockingQueue(20000), threadFactory);
        for (int i = 1; i <= 10000; i++) {
            UpdateRequest updateRequest = new UpdateRequest("kuang_index", "1");
            updateRequest.timeout(TimeValue.timeValueSeconds(1));
            updateRequest.timeout("1s");
            User user = new User("狂神" + i, i, "南疆" + i);
            updateRequest.doc(JSON.toJSONString(user), XContentType.JSON);
            executorService.submit(new UpdateTask(user, updateRequest));
            System.out.println("任务提交了：" + i + "次。");
        }
        count.await(10, TimeUnit.MINUTES);
    }


    /**
     * 单独更新一个文档，支持部分更新
     *
     * @throws IOException
     */
    @Test
    public void update_single() throws IOException {
        UpdateRequest updateRequest = new UpdateRequest("kuang_index", "1");
        updateRequest.timeout(TimeValue.timeValueSeconds(1));
        updateRequest.timeout("1s");
        User user = new User();
//        user.setName("狂神老大");
        user.setAddress("南京");
        updateRequest.doc(JSON.toJSONString(user), XContentType.JSON);
        UpdateResponse updateResponse = restHighLevelClient.update(updateRequest, RequestOptions.DEFAULT);
        System.out.println("updateResponse.status() = " + updateResponse.status());

    }


    public class UpdateTask implements Runnable {

        private User user;
        private UpdateRequest updateRequest;

        public UpdateTask(User user, UpdateRequest updateRequest) {
            this.user = user;
            this.updateRequest = updateRequest;
        }

        @Override
        public void run() {
            try {
                UpdateResponse updateResponse = restHighLevelClient.update(updateRequest, RequestOptions.DEFAULT);
                System.out.println("updateResponse.status() = " + updateResponse.status());
            } catch (Exception e) {
                System.out.println("e.getMessage() = " + e.getMessage());
            }

        }
    }

}
