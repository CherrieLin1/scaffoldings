package com.sy.scaffolding.generatorid;

import com.sy.scaffolding.generatorid.service.IdSequenceService;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;

/**
 * @ClassName id获取任务
 * @Description
 * @Author linshengyan
 * @Date 2022/8/3 11:06 上午
 */
public class IdTask implements Runnable {

    private IdSequenceService idSequenceService;

    private volatile CountDownLatch countDownLatch;

    private ConcurrentLinkedQueue idQueue;

    private Integer loopCount;

    private String tableName;

    public IdTask(IdSequenceService idSequenceService, CountDownLatch countDownLatch, ConcurrentLinkedQueue idQueue, Integer loopCount, String tableName) {
        this.idSequenceService = idSequenceService;
        this.countDownLatch = countDownLatch;
        this.idQueue = idQueue;
        this.loopCount = loopCount;
        this.tableName = tableName;
    }

    @Override
    public void run() {
        //每个线程获取loopCount次主键id
        for (int i = 0; i < loopCount; i++) {
            idQueue.add(idSequenceService.nextId(tableName));
        }
        //线程执行完毕
        countDownLatch.countDown();
    }
}
