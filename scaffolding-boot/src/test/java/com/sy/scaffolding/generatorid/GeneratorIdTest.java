package com.sy.scaffolding.generatorid;

import com.sy.scaffolding.BaseTest;
import com.sy.scaffolding.generatorid.service.IdSequenceService;
import com.sy.scaffolding.server.infrastructure.utils.SpringContextUtils;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName GeneratorIdTest
 * @Description
 * @Author linshengyan
 * @Date 2022/8/3 11:15 上午
 */
public class GeneratorIdTest extends BaseTest {


    @Test
    public void test() throws InterruptedException {
        int threadCount = 100;
        int loopCount = 100;
        ConcurrentLinkedQueue concurrentLinkedQueue = new ConcurrentLinkedQueue();
        CountDownLatch countDownLatch = new CountDownLatch(threadCount);
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(threadCount, threadCount, 100, TimeUnit.SECONDS, new ArrayBlockingQueue<>(100), new ThreadPoolExecutor.DiscardPolicy());
        long start = System.currentTimeMillis();

        for (int i = 0; i < threadCount; i++) {
            threadPoolExecutor.execute(new IdTask(SpringContextUtils.getBean(IdSequenceService.class), countDownLatch, concurrentLinkedQueue, loopCount, "resource"));
        }

        countDownLatch.await();
        long cost = System.currentTimeMillis() - start;
        System.out.println("共耗时：" + cost);

        Set<Long> set = new HashSet<Long>(concurrentLinkedQueue);

        System.out.println("线程数：" + threadCount + "，执行次数：" + (threadCount * loopCount) + "，qps:" + ((threadCount * loopCount) / cost)*1000 + ",成功率：" + (Double.valueOf(set.size() )/ concurrentLinkedQueue.size()));


    }
}
