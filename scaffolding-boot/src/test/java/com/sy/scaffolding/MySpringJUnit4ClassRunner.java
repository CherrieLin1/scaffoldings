package com.sy.scaffolding;

import org.junit.Rule;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runners.model.InitializationError;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Map;

/**
 * BaseTest 类的 @RunWith(SpringJUnit4ClassRunner.class) 改成 @RunWith(MySpringJUnit4ClassRunner.class)
 * @ClassName SpringJUnit4ClassRunner  参考链接：https://blog.csdn.net/freewebsys/article/details/52781896
 * @Description
 * @Author linshengyan
 * @Date 2022/4/1 5:48 下午
 */
public class MySpringJUnit4ClassRunner extends SpringJUnit4ClassRunner {
    @Rule
    public final EnvironmentVariables environmentVariables
            = new EnvironmentVariables();

    public MySpringJUnit4ClassRunner(Class<?> clazz) throws InitializationError {
        super(clazz);
        Map map = System.getenv();
        System.out.println("设置前环境变量数量： " + map.size());


        //设置指定的环境变量
//        environmentVariables.set("name", "value");
//        environmentVariables.set("WOAUTH_URL", "http://woauth.qa.internal.hsmob.com");
//        environmentVariables.set("APP_ID", "chihiro-console");
//        environmentVariables.set("HOSTNAME", "qa");
        environmentVariables.set("TEST_V","lin");


        Map map1 = System.getenv();
        System.out.println("设置后环境变量数量： " + map1.size());
    }

}
