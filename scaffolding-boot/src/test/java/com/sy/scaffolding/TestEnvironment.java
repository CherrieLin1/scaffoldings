package com.sy.scaffolding;

import org.junit.Test;

/**
 * @ClassName TestEnvirment
 * @Description
 * @Author linshengyan
 * @Date 2022/4/6 4:35 下午
 */
public class TestEnvironment extends BaseTest {

    @Test
    public void test() {
        String str = "lin";
        //MySpringJUnit4ClassRunner中设置了环境变量TEST_V
        boolean test_v = str.equals(System.getenv("TEST_V"));
        System.out.println("test_v = " + test_v);
    }
}
