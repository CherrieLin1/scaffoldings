package com.sy.scaffolding.hint;

import com.sy.scaffolding.BaseTest;
import com.sy.scaffolding.dao.entity.ResourceDO;
import com.sy.scaffolding.dao.mapper.ResourceMapper;
import org.apache.shardingsphere.api.hint.HintManager;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName HintManageTest
 * @Description
 * @Author linshengyan
 * @Date 2022/8/3 4:13 下午
 */
public class HintManageTest extends BaseTest {
    @Autowired
    ResourceMapper resourceMapper;

    /**
     * 指定库表获取数据
     * 0库1表
     */
    @Test
    public void testGetAll(){
        HintManager hintManager = HintManager.getInstance();
        hintManager.addDatabaseShardingValue("resource",0);
        hintManager.addTableShardingValue("resource",0);
        List<ResourceDO> all = resourceMapper.getAll();
        System.out.println("all = " + all);
        System.out.println("all.size() = " + all.size());
        hintManager.close();
        System.out.println("all.stream().map(x ->x.getId()).collect(Collectors.toList()) = " + all.stream().map(x -> x.getId()).collect(Collectors.toList()));
    }
}
