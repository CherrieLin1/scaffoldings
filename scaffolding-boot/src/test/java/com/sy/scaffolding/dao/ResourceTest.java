package com.sy.scaffolding.dao;

import com.sy.scaffolding.BaseTest;
import com.sy.scaffolding.dao.entity.ResourceCodeDO;
import com.sy.scaffolding.dao.entity.ResourceDO;
import com.sy.scaffolding.dao.entity.ResourceTemplateDO;
import com.sy.scaffolding.dao.mapper.ResourceCodeMapper;
import com.sy.scaffolding.dao.mapper.ResourceMapper;
import com.sy.scaffolding.dao.mapper.ResourceTemplateMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * @ClassName Resourcetest
 * @Description
 * @Author linshengyan
 * @Date 2022/3/16 2:44 下午
 */
public class ResourceTest extends BaseTest {

    @Autowired
    ResourceMapper resourceMapper;

    @Autowired
    ResourceTemplateMapper resourceTemplateMapper;

    @Autowired
    ResourceCodeMapper resourceCodeMapper;

    @Test
    public void testGetResourceCode() {
        for (int i = 0; i < 10; i++) {
            ResourceCodeDO resourceCodeDO = getResourceCodeDO(i);
            int insert = resourceCodeMapper.insert(resourceCodeDO);
            System.out.println("resourceCodeDO.getId() = " + resourceCodeDO.getId());
            resourceCodeMapper.selectByIdAndCode(resourceCodeDO.getId(), resourceCodeDO.getCode());
        }


    }

    @Test
    public void testGetResourceTemplate() {
        for (int i = 0; i < 10; i++) {
            ResourceTemplateDO resourceTemplateDO = getResourceTemplateDO(i);
            System.out.println(resourceTemplateDO);
            int insert = resourceTemplateMapper.insert(resourceTemplateDO);
            System.out.println("insert = " + insert);
            ResourceTemplateDO resourceTemplateDO1 = resourceTemplateMapper.selectByIdAndBosId(Long.parseLong(insert + ""), resourceTemplateDO.getBosId());
            System.out.println("resourceTemplateDO1.getId() = " + resourceTemplateDO1.getId());
        }


    }

    @Test
    public void testGetResource() {
        for (int i = 0; i < 10; i++) {
            ResourceDO resourceDO1 = getResourceDO(i);
            int insert = resourceMapper.insert(resourceDO1);
            System.out.println("insert = " + insert);
            Long id = resourceDO1.getId();
            System.out.println("resourceDO1.getId() = " + id);
//
//            ResourceDO resourceDO = resourceMapper.selectById(id);
//            System.out.println("resourceDO = " + resourceDO);
        }

    }

    public ResourceCodeDO getResourceCodeDO(int wid) {
        ResourceCodeDO resourceCodeDO = new ResourceCodeDO();
//        resourceCodeDO.setId(obj.getId());
        resourceCodeDO.setCode("code");
        resourceCodeDO.setResourceTemplateId(11L);
        resourceCodeDO.setResourceId(22L);
        resourceCodeDO.setStatus(1);
        resourceCodeDO.setWid(Long.parseLong(wid + ""));
        resourceCodeDO.setCreateTime(new Date());
        resourceCodeDO.setUpdateTime(new Date());
        resourceCodeDO.setIsDeleted(0L);
        return resourceCodeDO;

    }


    /**
     * @return
     */
    public ResourceTemplateDO getResourceTemplateDO(int bosId) {
        ResourceTemplateDO resourceTemplateDO = new ResourceTemplateDO();
//        resourceTemplateDO.setId(1L);
        resourceTemplateDO.setName("测试模版1");
        resourceTemplateDO.setStatus(1);
        resourceTemplateDO.setCreator(222L);
        resourceTemplateDO.setType(1);
        resourceTemplateDO.setCreateTime(new Date());
        resourceTemplateDO.setUpdateTime(new Date());
        resourceTemplateDO.setIsDeleted(0);
        resourceTemplateDO.setMerchantId(2222L);
        resourceTemplateDO.setBosId(Long.parseLong(bosId + ""));
        resourceTemplateDO.setVid(4222L);
        return resourceTemplateDO;
    }

    public ResourceDO getResourceDO(int i) {
        ResourceDO resourceDO = new ResourceDO();
//            resourceDO.setId(obj.getId());
        resourceDO.setCode("code" + i);
        resourceDO.setName("测试资源1");
        resourceDO.setResourceTemplateId(111L);
        resourceDO.setStatus(1);
        resourceDO.setWid(Long.parseLong(i + ""));
        resourceDO.setCreateTime(new Date());
        resourceDO.setUpdateTime(new Date());
        resourceDO.setIsDeleted(0L);
        resourceDO.setMerchantId(111L);
        resourceDO.setBosId(111L);
        resourceDO.setVid(111L);
        return resourceDO;
    }


}
