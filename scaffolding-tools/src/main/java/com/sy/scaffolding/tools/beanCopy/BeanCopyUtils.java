package com.sy.scaffolding.tools.beanCopy;

import org.apache.commons.lang.SerializationUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

/**
 * 实现bean的copy,包括深copy和浅copy
 *
 * @author create by lsy on 2022/1/18 3:46 下午
 */
@Component
public class BeanCopyUtils {


    /**
     * 利用jdk的序列化,进行深copy，原对象还可以通过继承cloneable接口，实现深copy,例如类DeepClonePerson
     *
     * @param obj 原对象 ，原对象必须实现serialize接口
     * @param t   返回的对象类型
     * @param <T>
     * @return
     */
    public static <T> T beanCopy(BasePojo obj, Class<T> t) {
        T result = null;
        try {
            result = t.newInstance();
            result = (T) SerializationUtils.clone(obj);

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return result;

    }


    /**
     * 利用springBeanUtils拷贝，浅copy
     *
     * @param obj
     * @param tClass
     * @param <T>
     * @return
     */
    public static <T> T beanCopy(Object obj, Class<T> tClass) {
        if (obj == null) {
            System.out.println("被拷贝的对象为Null,bean copy 失败");
            return null;
        }
        T t = null;
        try {
            t = tClass.newInstance();
            BeanUtils.copyProperties(obj, t);
        } catch (Exception e) {
            System.out.println("bean copy 异常，异常信息为 = " + e.getMessage());
        }
        return t;

    }
}
