package com.sy.scaffolding.tools.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Person
 * @Description
 * @Author linshengyan
 * @Date 2022/4/6 3:31 下午
 */
@Data
public class Person {

    /**
     * 转成Json以后，三个字段的值 1：第一位 2：第二位  3:第三位
     */
    @JSONField(ordinal = 3)
    private Integer age;

    @JSONField(ordinal = 1)
    private String name;

    @JSONField(ordinal = 2)
    private String address;

    /**
     * new这个list的目的是，即便hobbies为null，转成JsonString的时候也会保留这个字段，若为null转string后不想要这个字段，不要new即可
     */
    private List<String> hobbies = new ArrayList<>();
}
