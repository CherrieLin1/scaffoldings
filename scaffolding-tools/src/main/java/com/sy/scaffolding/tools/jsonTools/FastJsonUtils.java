package com.sy.scaffolding.tools.jsonTools;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.sy.scaffolding.tools.pojo.Person;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName FastJsonUtils
 * @Description fastJson常用的API
 * @Author linshengyan
 * @Date 2022/4/6 11:28 上午
 */
public class FastJsonUtils {
    private static String str = "{19:\"ResourceFreezeAction\",20:\"ResourceReturnAction\",21:\"ResourceUseAction\",24:\"SendAction\"," +
            "25:\"CodeSendAction\",27:\"SendReturnAction\",14:\"BuyerLimitReturnAction\",15:\"PackageSendAction\"}";


    public static void main(String[] args) {
        /*  ------------> test  parseBeanToString<-------------*/
        Person person = new Person();
        person.setAddress("南京");
        person.setAge(12);
        person.setName("shengyan");
        System.out.println("parseBeanToString(person) = " + parseBeanToString(person));





        /*  ------------> test  isJsonString、parseJsonStringToMap<-------------*/
        System.out.println("str = " + str);
        //构建Map<String,String>
        String string = buildStringMap();
        System.out.println("string = " + string);


        //测试string是否是Json
        System.out.println("isJsonString(str) = " + isJsonString(str));
        System.out.println("isJsonString(string) = " + isJsonString(string));


        //将jsonString转为指定类型Map Map<Integer,String> 转 Map<Integer,String> 可以
        Map<Integer, String> map1 = parseJsonStringToMap(str, Integer.class, String.class);
        System.out.println("map1 = " + map1);


        //Map<String,String> 转 Map<Integer,String> 可以
        Map<Integer, String> map2 = parseJsonStringToMap(string, Integer.class, String.class);
        System.out.println("map2 = " + map2);

        //Map<Integer,String> 转 map<String,String> 不可以
        Map<String, String> map3 = parseJsonStringToMap(str, String.class, String.class);
        System.out.println("map3 = " + map3);

        //Map<String,String> 转 map<String,String> 可以
        Map<String, String> map4 = parseJsonStringToMap(string, String.class, String.class);
        System.out.println("map4 = " + map4);
//

    }


    /**
     * 和str区别一下，一个是Map<Integer,String> 一个是<String,String>
     *
     * @return
     */
    public static String buildStringMap() {
        Map map = new HashMap<String, String>(10);
        map.put("19", "ResourceFreezeAction");
        map.put("20", "ResourceReturnAction");
        map.put("21", "ResourceUseAction");
        map.put("24", "SendAction");
        map.put("25", "CodeSendAction");
        map.put("27", "SendReturnAction");
        map.put("14", "BuyerLimitReturnAction");
        map.put("15", "PackageSendAction");
        return JSON.toJSONString(map);
    }


    /**
     * 将Json字符串转为指定类型的Map
     *
     * @param json
     * @param kClass
     * @param vClass
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<K, V> parseJsonStringToMap(String json, Class<K> kClass, Class<V> vClass) {
        TypeReference<Map<K, V>> typeReference = new TypeReference<Map<K, V>>(kClass, vClass) {
        };
        return JSON.parseObject(json, typeReference);
    }

    /**
     * 是否是Json字符串
     *
     * @param str
     * @return
     */
    public static boolean isJsonString(String str) {
        try {
            JSONObject.parseObject(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 将对象按照指定的字段顺序转JsonString,null字段默认不保留      @JSONField(ordinal = 3)
     * 指定顺序输出：parseBeanToString(person) = {"name":"shengyan","address":"南京","age":12}
     * 默认输出：parseBeanToString(person) = {"address":"南京","age":12,"name":"shengyan"}
     *
     *
     * @param obj
     * @return
     */
    public static String parseBeanToString(Object obj) {
        try {
            return JSONObject.toJSONString(obj);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("对象转JsonString出错");
            return null;
        }
    }


    public static String parseBeanToStringWithNullParam(Object obj){
        try {
            return JSONObject.toJSONString(obj, SerializerFeature.WriteMapNullValue);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("对象转JsonString出错");
            return null;
        }
    }



}
