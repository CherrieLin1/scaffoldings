# 1.说明

目前json转换工具类主流的主要有三种,fastJson，JackSon，Gson。目前没有说非要用哪种，常用的转换在对应的 *Utils.java 类中。


# 2.fastJson
fastJson是阿里爸爸开源的json工具类，正如其名字一样以速度著称。但是由于维护人员不多，目前经常爆出各种漏洞。详细的可以参见网上的说明。 依赖如下，最新版本可以去中央仓库查看。

```xml
<!-- https://mvnrepository.com/artifact/com.alibaba/fastjson -->
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>fastjson</artifactId>
    <version>1.2.80</version>
</dependency>
```
[maven中央仓库的地址](https://mvnrepository.com/search?q=fastjson)


# 3.jackson
jackson是社区开源的，除了在某些方法上速度不如fastjson,网上好评还是不输fastjson的。*spring-boot*中默认的就是jackson，有的项目会把jackson换成fastJson。
依赖jackson需要导入三个依赖包，最新版本见中央仓库。

```xml

<!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind -->
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
    <version>2.9.3</version>
</dependency>
<!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core -->
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-core</artifactId>
    <version>2.9.3</version>
</dependency>
<!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-annotations -->
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-annotations</artifactId>
    <version>2.9.3</version>
</dependency>

```

# 4.Gson
Gson是谷歌发布的。目前版本依赖如下
```xml
<!-- https://mvnrepository.com/artifact/com.google.code.gson/gson -->
<dependency>
    <groupId>com.google.code.gson</groupId>
    <artifactId>gson</artifactId>
    <version>2.9.0</version>
</dependency>

```
