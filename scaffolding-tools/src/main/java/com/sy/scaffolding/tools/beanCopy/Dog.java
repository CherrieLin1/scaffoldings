package com.sy.scaffolding.tools.beanCopy;

import lombok.Data;

import java.io.Serializable;


/**
 * @author create by lsy on 2022/1/18 4:06 下午
 */
@Data
public class Dog implements Serializable {
    private static final long serialVersionUID = -4518066076153561833L;

    private String name;

    private int age;
}
