package com.sy.scaffolding.tools.beanCopy;

import com.sy.scaffolding.tools.objectComparator.IgnoreCheck;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName User
 * @Description
 * @Author linshengyan
 * @Date 2022/3/28 7:22 下午
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID = 8306231466760650915L;
    /**
     * 姓名
     */
    private String name;

    /**
     * 年龄
     */
    private int age;


    /**
     * 邮箱
     */
    private String email;


    /**
     * 地址
     */
    public String address;


    /**
     * 手机
     */
    protected String phone;


    /**
     * 是否已经删除
     */
    private boolean isDeleted;


    /**
     * 忽略检查的字段
     */
    @IgnoreCheck()
    private String ext;

    /**
     * 小狗狗
     */
    private Dog dog;


}
