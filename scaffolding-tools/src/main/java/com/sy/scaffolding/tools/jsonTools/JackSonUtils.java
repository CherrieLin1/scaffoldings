package com.sy.scaffolding.tools.jsonTools;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName JackSonUtils
 * @Description JackSon常用的API
 * @Author linshengyan
 * @Date 2022/4/6 11:28 上午
 */
public class JackSonUtils {
    private static String str = "{19:\"ResourceFreezeAction\",20:\"ResourceReturnAction\",21:\"ResourceUseAction\",24:\"SendAction\",\n" +
            "25:\"CodeSendAction\",27:\"SendReturnAction\",14:\"BuyerLimitReturnAction\",15:\"PackageSendAction\"}";


    private final static ObjectMapper objectMapper = new ObjectMapper();

    public static void main(String[] args) throws JsonProcessingException {
        //通过jackson将map转为String
        String string = buildJacksonString();

        //输出两种String的区别，jackSon的key有双引号，但是Gson和fastJson可以没有
        System.out.println("string = " + string);
        System.out.println("str = " + str);



        /*-----------> 判断一个string是否是JsonString  <-----------*/
        //return true
        System.out.println("isJsonString(string) = " + isJsonString(string));

        //return false
        System.out.println("isJsonString(str) = " + isJsonString(str));



        /*-----------> 将JsonString 转为指定格式Map<K,V>  <-----------*/
        //throw exception,jackson的JsonString 的key必须为String类型的
        Map<Integer, String> map1 = parseJsonStringToMap(str, Integer.class, String.class);
        System.out.println("map1 = " + map1);

        //Map<String,String> 转 Map<Integer,String> 可以
        Map<Integer, String> map2 = parseJsonStringToMap(string, Integer.class, String.class);
        System.out.println("map2 = " + map2);

        //throw exception,jackson的JsonString 的key必须为String类型的
        Map<String, String> map3 = parseJsonStringToMap(str, String.class, String.class);
        System.out.println("map3 = " + map3);

        //Map<String,String> 转 map<String,String> 可以
        Map<String, String> map4 = parseJsonStringToMap(string, String.class, String.class);
        System.out.println("map4 = " + map4);


    }


    /**
     * 将Json字符串转为指定类型的Map
     *
     * @param json
     * @param kClass
     * @param vClass
     * @param <K>
     * @param <V>
     * @return
     * @throws JsonProcessingException
     */
    public static <K, V> Map<K, V> parseJsonStringToMap(String json, Class<K> kClass, Class<V> vClass) {
        TypeReference<Map<K, V>> typeReference = new TypeReference<Map<K, V>>() {
        };
        try {
            return objectMapper.readValue(json, typeReference);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            System.out.println("将jsonString 转为指定类型的Map<K,V> 出错");
            return null;
        }
    }

    /**
     * 将Map转为String
     *
     * @return
     * @throws JsonProcessingException
     */
    public static String buildJacksonString() throws JsonProcessingException {
        Map map = new HashMap<Integer, String>(10);
        map.put(19, "ResourceFreezeAction");
        map.put(20, "ResourceReturnAction");
        map.put(21, "ResourceUseAction");
        map.put(24, "SendAction");
        map.put(25, "CodeSendAction");
        map.put(27, "SendReturnAction");
        map.put(14, "BuyerLimitReturnAction");
        map.put(15, "PackageSendAction");
        return objectMapper.writeValueAsString(map);
    }

    /**
     * 判断是否是jsonString
     *
     * @param str
     * @return
     */
    public static boolean isJsonString(String str) {
        try {
            objectMapper.readTree(str);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
