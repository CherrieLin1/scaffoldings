package com.sy.scaffolding.tools.jsonTools;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/**
 * @ClassName GsonTools
 * @Description gson常用的API
 * @Author linshengyan
 * @Date 2022/4/6 11:27 上午
 */
public class GsonUtils {
    private static final Gson gson = new Gson();
    private static String str = "{19:\"ResourceFreezeAction\",20:\"ResourceReturnAction\",21:\"ResourceUseAction\",24:\"SendAction\"," +
            "25:\"CodeSendAction\",27:\"SendReturnAction\",14:\"BuyerLimitReturnAction\",15:\"PackageSendAction\"}";


    public static void main(String[] args) {
        System.out.println("isJsonString1(str) = " + isJsonString1(str));
        System.out.println("isJsonString2(str) = " + isJsonString2(str));
    }


    /**
     * 判断一个String是否是json格式的
     *
     * @param str
     * @return
     */
    public static boolean isJsonString1(String str) {
        try {
            JsonElement jsonElement = JsonParser.parseString(str);
            return jsonElement.isJsonObject();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 判断一个String是否是json格式的
     *
     * @param str
     * @return
     */
    public static boolean isJsonString2(String str) {
        try {
            gson.fromJson(str, Object.class);
            return true;
        } catch (Exception e) {
            return false;
        }

    }
}
