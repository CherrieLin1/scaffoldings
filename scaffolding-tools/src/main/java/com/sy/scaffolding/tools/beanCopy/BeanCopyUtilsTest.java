package com.sy.scaffolding.tools.beanCopy;

/**
 * @ClassName TestBeanCopyUtils
 * @Description
 * @Author linshengyan
 * @Date 2022/3/28 7:30 下午
 */
public class BeanCopyUtilsTest {


    public static void main(String[] args) {
        User user = new User();
        user.setName("shengyan");
        user.setEmail("linfgjkl");
        Dog dog = new Dog();
        dog.setAge(45);
        dog.setName("小黄");
        user.setDog(dog);


        User user1 = BeanCopyUtils.beanCopy(user,User.class);
        System.out.println("user.equals(user1) = " + user.equals(user1));
        System.out.println("user.hashCode() == user1.hashCode() = " + (user.hashCode() == user1.hashCode()));
        System.out.println("user1 == user = " + (user1 == user));


        User user2 = BeanCopyUtils.beanCopy(user,User.class);
        System.out.println("user.equals(user2) = " + user.equals(user1));
        System.out.println("user.hashCode() == user2.hashCode() = " + (user.hashCode() == user2.hashCode()));
        System.out.println("user2 == user = " + (user2 == user));



    }
}
