package com.sy.scaffolding.tools.objectComparator;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 忽略校验的注解
 *
 * @author create by lsy on 2021/10/18 3:18 下午
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface IgnoreCheck {
    String name() default "";
}
