package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SacffoldingSpringbootApplication {

	public static void main(String[] args) {
		//初始化容器
		ConfigurableApplicationContext context = SpringApplication.run(SacffoldingSpringbootApplication.class, args);

		//This is important，销毁容器，这种方法一个Bean才会真正的走销毁流程被销毁
		context.registerShutdownHook();
	}

}
