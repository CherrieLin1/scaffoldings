package com.day04.proxy;


import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.aspectj.AspectJPointcutAdvisor;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;

/**
 * @ClassName TestProxy
 * @Description 测试代理
 * @Author linshengyan
 * @Date 2022/6/21 6:46 下午
 */
public class TestProxy {

    public static void main(String[] args) {
        //aspect（切面） = 通知（advice）+ 切点（point）,一个切面中可能有一到多个通知方法
        //advisor = 更细粒度的切面，包含一个通知和切点

        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.setTarget(new Target1());


        //通知，lambda表达式写法，对所有的方法进行增强,代理添加一个环绕通知MethodInterceptor
    /*    proxyFactory.addAdvice((MethodInterceptor) invocation -> {
            try {
                System.out.println("before....");
                return invocation.proceed();
            } finally {
                System.out.println("after.....");
            }
        });*/

        //为了不对所有的方法进行增强，我们需要设置切点
        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
        //代码匹配类中的foo()方法
        pointcut.setExpression("execution(* foo(..))");
        //更细粒度的切面，我们用advisor来写，指定通知和切面
        proxyFactory.addAdvisor(new DefaultPointcutAdvisor(pointcut, (MethodInterceptor) invocation -> {
            try {
                System.out.println("foo  before1....");
                return invocation.proceed();
            } finally {
                System.out.println("foo after1.....");
            }
        }));

        AspectJExpressionPointcut pointcut1 = new AspectJExpressionPointcut();
        pointcut1.setExpression("execution(* bar(..))");

        proxyFactory.addAdvisor(new DefaultPointcutAdvisor(pointcut1, (MethodInterceptor) invocation -> {
            try {
                System.out.println("bar  before1....");
                return invocation.proceed();
            } finally {
                System.out.println("bar after1.....");
            }
        }));

        //添加这一行的意思是，告诉代理工厂用这个类来代理，默认的CGLIB代理会被替换成jdk代理，代理对象不能强转为目标对象
        proxyFactory.addInterface(I1.class);
        I1 proxy = (I1) proxyFactory.getProxy();


        //默认使用cglib代理
        proxyFactory.setProxyTargetClass(true);



//        Target1 proxy = (Target1) proxyFactory.getProxy();
        //看看代理对象是什么方式代理的
        System.out.println("proxy.getClass() = " + proxy.getClass());
        proxy.bar();
        proxy.foo();

    }


    interface I1 {
        void foo();

        void bar();
    }

    static class Target1 implements I1 {

        @Override
        public void foo() {
            System.out.println("target foo");
        }

        @Override
        public void bar() {
            System.out.println("target bar");
        }
    }
}
