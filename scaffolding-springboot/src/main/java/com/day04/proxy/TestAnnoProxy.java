package com.day04.proxy;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.context.annotation.AnnotationConfigUtils;
import org.springframework.context.support.GenericApplicationContext;

/**
 * @ClassName TestAnnoProxy
 * @Description 注解方式实现切面通知
 * 原始方式实现切面通知还是比较复杂的，一般我们都用注解方式实现切面通知
 * 最终还是会转换成advisor和MethodInterceptor
 * @Author linshengyan
 * @Date 2022/6/22 10:29 上午
 */
public class TestAnnoProxy {
    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        AnnotationConfigUtils.registerAnnotationConfigProcessors(context.getDefaultListableBeanFactory());
        context.registerBean("target1", Target1.class);
        context.registerBean("target2", Target2.class);
        context.registerBean("aspect1", Aspect1.class);
        context.registerBean("aspect2", Aspect2.class);
        context.registerBean("aspect3", Aspect3.class);
        //注册自动代理处理器，不注册的话代理是不会生效的
        context.registerBean(AnnotationAwareAspectJAutoProxyCreator.class);
        context.refresh();


        /*AnnotationAwareAspectJAutoProxyCreator creator = context.getBean(AnnotationAwareAspectJAutoProxyCreator.class);
        //bean-->指需要检查的对象,例如 Target1.class  beanName 和cacheKey按照指定要求填即可，不重要。因为wrapIfNecessary是受保护的方法，所以没法直接调用，想要调用的方法为在自己的工程中创建同名的包，然后写测试类调用，这里没有实现
        creator.wrapIfNecessary(Object bean, String beanName, Object cacheKey);*/


        Target1 target1 = context.getBean(Target1.class);
        Target2 target2 = context.getBean(Target2.class);

        target1.foo();
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>");
        target2.bar();


    }

    static class Target1 {
        public void foo() {
            System.out.println("target1 foo()....");
        }
    }

    static class Target2 {
        public void bar() {
            System.out.println("target2 bar()....");
        }
    }

    /**
     * @Aspect代表这是一个切面
     */
    @Aspect
    static class Aspect1 {
        //@Around代表这是一个切点
        @Around("execution(* foo(..))")  //------>一个advisor切面
        public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
            System.out.println("Aspect1 around() --->target1 foo()");
            //返回执行的结果
            return joinPoint.proceed();
        }

    }

    @Aspect
    static class Aspect2 {
        @Before("execution(* foo(..))")  //------>一个advisor切面
        public void before() {
            System.out.println("Aspect2 before() --->target1 foo()");
        }

        @After("execution(* foo(..))")  //------>一个advisor切面
        public void after() {
            System.out.println("Aspect2 after() --->target1 foo()");
        }

    }

    @Aspect
    static class Aspect3 {
        @Before("execution(* bar(..))")  //------>一个advisor切面
        public void after() {
            System.out.println("Aspect3 after() --->target2 bar()");
        }

    }
}
