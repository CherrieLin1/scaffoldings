package com.day04.circularDependency.set;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigUtils;
import org.springframework.context.support.GenericApplicationContext;

/**
 * @ClassName Example_withProxy
 * @Description 和1的区别是加了加了一个代理，对A类中的foo方法进行功能增强
 * @Author linshengyan
 * @Date 2022/6/22 5:34 下午
 */
public class Example_withProxy {
    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        AnnotationConfigUtils.registerAnnotationConfigProcessors(context.getDefaultListableBeanFactory());
        //aspect后处理器，该类有两个方法有创建代理有关系，第一个是 public Object getEarlyBeanReference(Object bean, String beanName) ，第二个是	public Object postProcessAfterInitialization(@Nullable Object bean, String beanName) {
        context.registerBean(AnnotationAwareAspectJAutoProxyCreator.class);
        context.registerBean("a", A.class);
        context.registerBean("b", B.class);
        context.registerBean("myAspect",MyAspect.class);

        context.refresh();

        //调用方法
        context.getBean(A.class).foo();
    }


    static class A {
        private static final Logger loggerA = LoggerFactory.getLogger("A");


        private B b;

        public A() {
            loggerA.info("public A()");
        }

        @Autowired
        public void setB(B b) {
            loggerA.info("set B({})",b.getClass());
            this.b = b;
        }


        public void foo() {
            loggerA.info("foo()....");
        }
    }

    @Aspect
    static class MyAspect {
        @Before("execution(* foo())")
        public void before() {
            System.out.println("before....");

        }
    }

    static class B {
        private static final Logger loggerB = LoggerFactory.getLogger("B");

        private A a;

        public B() {

        }

        @Autowired
        public B(A a) {
            loggerB.info("public B()");
            this.a = a;
        }

        @Autowired
        public void setA(A a) {
            loggerB.info("set A({})",a.getClass());
            this.a = a;
        }
    }
}
