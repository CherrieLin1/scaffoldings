package com.day04.circularDependency.set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigUtils;
import org.springframework.context.support.GenericApplicationContext;

/**
 * @ClassName Example_1
 * @Description 示例1
 * @Author linshengyan
 * @Date 2022/6/22 4:12 下午
 */
public class Example_1 {

    /**
     * 创建两个Bean对象，两个bean对象互为对方的属性
     * 调试关键断点有两个：
     * （1）AbstractBeanFactory（Object sharedInstance = getSingleton(beanName);这一行）
     * （2）AbstractAutowireCapableBeanFactory（Object earlySingletonReference = getSingleton(beanName, false);这一行）
     *
     * 整个流程大致为：
     * A创建对象-->填充属性（发现是B，B这时候没有）--->A将自己放入三级缓存并调用B创建对象的方法
     *      B创建对象-->填充属性（发现是A,去三级缓存singletonFactories中找，发现有，这时候三级缓存提前创建A对象的代理，返回给B完成B的属性注入，并将代理放入二级缓存earlySingletonObjects，移除三级缓存中的A的半成品对象）-->对象B完成初始化，放入一级缓存：singletonObjects
     *          继续A未完成的创建过程，填充属性,一级缓存中查找到-->初始化，去二级缓存中查找是否有代理创建，发现有，拿过来使用--->初始化A,将A放入一级缓存，并清除二级缓存中的信息
     * @param args
     */
    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        AnnotationConfigUtils.registerAnnotationConfigProcessors(context.getDefaultListableBeanFactory());
        context.registerBean("a", A.class);
        context.registerBean("b", B.class);

        context.refresh();
    }


    static class A {
        private static final Logger loggerA = LoggerFactory.getLogger("A");


        private B b;

        public A() {
            loggerA.info("public A()");
        }

        @Autowired
        public void setB(B b) {
            loggerA.info(String.format("set A(),a = %s", b.getClass()));
            this.b = b;
        }
    }

    static class B {
        private static final Logger loggerB = LoggerFactory.getLogger("B");

        private A a;

        public B() {

        }

        @Autowired
        public B(A a) {
            loggerB.info("public B()");
            this.a = a;
        }

        @Autowired
        public void setA(A a) {
            loggerB.info(String.format("set A(),a = %s", a.getClass()));
            this.a = a;
        }
    }
}
