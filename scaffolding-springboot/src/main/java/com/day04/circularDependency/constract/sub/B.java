package com.day04.circularDependency.constract.sub;

import com.day02.LoggerUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @ClassName B
 * @Description
 * @Author linshengyan
 * @Date 2022/6/22 3:49 下午
 */
@Component
/**
 * 以下注解生成代理对象
 */
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class B {
    private A a;

    public B(A a) {
        this.a = a;
        LoggerUtils.get().info("public B(){}");
        LoggerUtils.get().info(String.format("a = %s:", a.getClass().getName()));
    }

    @PostConstruct
    public void init() {
        LoggerUtils.get().info("B init~");
    }

}
