package com.day04.circularDependency.constract;

import com.day02.LoggerUtils;
import org.springframework.context.annotation.AnnotationConfigUtils;
import org.springframework.context.support.GenericApplicationContext;

import javax.annotation.PostConstruct;
import javax.inject.Provider;

/**
 * @ClassName Solution4
 * @Description 解决方式四：Provider接口解决
 * 用 Provider 接口解决，原理上与 ObjectProvider 一样，Provider 接口是独立的 jar 包，需要加入依赖
 * <dependency>
 *     <groupId>javax.inject</groupId>
 *     <artifactId>javax.inject</artifactId>
 *     <version>1</version>
 * </dependency>
 * @Author linshengyan
 * @Date 2022/6/22 3:17 下午
 */
public class SolutionByProvider {
    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        AnnotationConfigUtils.registerAnnotationConfigProcessors(context.getDefaultListableBeanFactory());

        context.registerBean(A.class);
        context.registerBean(B.class);
        context.refresh();

        System.out.println(context.getBean(A.class).b.get());
        System.out.println(context.getBean(B.class));
    }

    static class A {
        private Provider<B> b;

        public A(Provider<B> b) {
            this.b = b;
            LoggerUtils.get().info("public A(){}");
            LoggerUtils.get().info(String.format("b =%s :", b.getClass().getName()));
        }


        @PostConstruct
        public void init() {
            LoggerUtils.get().info("A init~");
        }
    }


    static class B {
        private A a;

        public B(A a) {
            this.a = a;
            LoggerUtils.get().info("public B(){}");
            LoggerUtils.get().info(String.format("a = %s:", a.getClass().getName()));
        }

        @PostConstruct
        public void init() {
            LoggerUtils.get().info("B init~");
        }

    }
}
