package com.day04.circularDependency.constract;

import com.day02.LoggerUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.context.annotation.AnnotationConfigUtils;
import org.springframework.context.support.GenericApplicationContext;

import javax.annotation.PostConstruct;

/**
 * @ClassName Solution3
 * @Description 解决方式三：ObjectFactory/ObjectProvider 最优雅的方式
 * @Author linshengyan
 * @Date 2022/6/22 3:16 下午
 */
public class SolutionByObjectFactory {
    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        AnnotationConfigUtils.registerAnnotationConfigProcessors(context.getDefaultListableBeanFactory());

        context.registerBean(A.class);
        context.registerBean(B.class);
        context.refresh();
        System.out.println(context.getBean(A.class).b.getObject());
        System.out.println(context.getBean(B.class));
    }

    /**
     * 通过工厂类替换原有类型
     */
    static class A {
        private ObjectFactory<B> b;
//        private ObjectProvider<B> b;ObjectFactory的子类，用法一摸一样

        public A(ObjectFactory<B>  b) {
            this.b = b;
            LoggerUtils.get().info("public A(){}");
            LoggerUtils.get().info(String.format("b = %s:",b.getClass().getName()));
        }


        @PostConstruct
        public void init() {
            LoggerUtils.get().info("A init~");
        }
    }


    static class B {
        private A a;

        public B(A a) {
            this.a = a;
            LoggerUtils.get().info("public B(){}");
            LoggerUtils.get().info(String.format("a = %s:", a.getClass().getName()));
        }

        @PostConstruct
        public void init() {
            LoggerUtils.get().info("B init~");
        }

    }
}
