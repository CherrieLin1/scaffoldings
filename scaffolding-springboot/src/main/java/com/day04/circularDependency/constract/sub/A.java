package com.day04.circularDependency.constract.sub;

import com.day02.LoggerUtils;
import com.day04.circularDependency.constract.SolutionByScope;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @ClassName A
 * @Description
 * @Author linshengyan
 * @Date 2022/6/22 3:49 下午
 */
@Component
public class A {

    private B b;

    public A(B b) {
        this.b = b;
        LoggerUtils.get().info("public A(){}");
        LoggerUtils.get().info(String.format("b =%s :", b.getClass().getName()));
    }


    @PostConstruct
    public void init() {
        LoggerUtils.get().info("A init~");
    }
}
