package com.day04.circularDependency.constract;

import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.context.support.GenericApplicationContext;


/**
 * @ClassName Solution2
 * @Description 解决方式二：@Scope
 * @Author linshengyan
 * @Date 2022/6/22 3:15 下午
 */
public class SolutionByScope {
    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        //配合包的扫描
        ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(context.getDefaultListableBeanFactory());
        scanner.scan("com.day04.circularDependency.constract.sub");
        context.refresh();
    }

}
