package com.day04.circularDependency.constract;

import com.day02.LoggerUtils;
import org.springframework.context.annotation.AnnotationConfigUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.support.GenericApplicationContext;

import javax.annotation.PostConstruct;

/**
 * @ClassName Solution1
 * @Description 解决方式一：@Lazy
 * @Author linshengyan
 * @Date 2022/6/22 3:14 下午
 */
public class SolutionByLazy {
    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        AnnotationConfigUtils.registerAnnotationConfigProcessors(context.getDefaultListableBeanFactory());

        context.registerBean(A.class);
        context.registerBean(B.class);
        context.refresh();
    }


    static class A {
        private B b;

        public A(@Lazy B b) {
            this.b = b;
            LoggerUtils.get().info("public A(){}");
            LoggerUtils.get().info(String.format("b =%s :", b.getClass().getName()));
        }


        @PostConstruct
        public void init() {
            LoggerUtils.get().info("A init~");
        }
    }


    static class B {
        private A a;

        public B(A a) {
            this.a = a;
            LoggerUtils.get().info("public B(){}");
            LoggerUtils.get().info(String.format("a = %s:", a.getClass().getName()));
        }

        @PostConstruct
        public void init() {
            LoggerUtils.get().info("B init~");
        }

    }
}
