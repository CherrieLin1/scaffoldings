package com.day04.boot;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.AnnotationConfigUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DeferredImportSelector;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @ClassName TestDeferredImport
 * @Description DeferredImport类最后工作
 * @Author linshengyan
 * @Date 2022/6/21 5:09 下午
 */
public class TestDeferredImport {
    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        //获取默认的bean工厂
        DefaultListableBeanFactory defaultListableBeanFactory = context.getDefaultListableBeanFactory();
        //是否允许同名字的类被覆盖
        defaultListableBeanFactory.setAllowBeanDefinitionOverriding(false);
        //注册bean的后置处理器---自动注解
        AnnotationConfigUtils.registerAnnotationConfigProcessors(defaultListableBeanFactory);
        context.registerBean(MyConfig.class);
        context.refresh();

        System.out.println(context.getBean(MyBean.class));
    }


    /**
     *
     */
    @Configuration
    @Import(OtherConfig.class)//1.同一配置类中, @Import 先解析  @Bean 后解析，故同名类会被后定义的覆盖
//    @Import(MySelector.class)//2.强制关闭bean工厂的覆盖策略，发现会报错。不允许覆盖的情况下, 如何能够让 MyConfig(主配置类) 的配置优先? (虽然覆盖方式能解决)
                                //DeferredImportSelector 最后工作, 可以简单认为先解析 @Bean, 再 Import，配合@ConditionalOnMissingBean使用，即便关闭了bean工厂的不允许覆盖策略，依旧可行
    static class MyConfig {//主配置 - 程序员编写的

        @Bean
        public MyBean myBean() {
            return new Bean1();
        }

    }


    /**
     * 用延迟选择器，会优先加载用户定义的类
     */
    static class MySelector implements DeferredImportSelector {
        @Override
        public String[] selectImports(AnnotationMetadata importingClassMetadata) {
            return new String[]{OtherConfig.class.getName()};
        }
    }


    @Configuration
    static class OtherConfig {//从属配置 - 自动配置

        @Bean
        @ConditionalOnMissingBean
        public MyBean myBean() {
            return new Bean2();
        }
    }


    interface MyBean {

    }

    static class Bean1 implements MyBean {

    }

    static class Bean2 implements MyBean {

    }
}
