package com.day04.boot;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.AnnotationConfigUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DeferredImportSelector;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @ClassName TestImport
 * @Description 测试import类
 * @Author linshengyan
 * @Date 2022/6/21 4:22 下午
 */
public class TestImport {

    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        //注册bean的后置处理器---自动注解
        AnnotationConfigUtils.registerAnnotationConfigProcessors(context.getDefaultListableBeanFactory());
        context.registerBean(MyConfig.class);
        context.refresh();
        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name);

        }
    }


    @Configuration
//    @Import(Bean1.class) //1.引入单个Bean
//    @Import(OtherConfig.class) //2.引入一个配置类
//    @Import(MySelector.class) //3.selector引入多个类
    @Import(MyRegister.class) //4.通过 beanDefinition 注册器注册bean
    static class MyConfig {
    }

    @Configuration
    static class OtherConfig {
        @Bean
        public Bean1 bean1() {
            return new Bean1();
        }

    }


    /**
     * 通过selector引入多个类，MySelector本身不加入，只是作为一个选择器
     * DeferredImportSelector 是 ImportSelector的子类
     * Deferred延迟的意思
     */
    static class MySelector implements DeferredImportSelector {

        @Override
        public String[] selectImports(AnnotationMetadata importingClassMetadata) {
            return new String[]{Bean3.class.getName(), Bean4.class.getName()};
        }
    }


    /**
     * 通过ImportBeanDefinitionRegistrar注册
     * 以编程的方式注入一个bean
     */
    static class MyRegister implements ImportBeanDefinitionRegistrar {
        @Override
        public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
            registry.registerBeanDefinition("bean5", BeanDefinitionBuilder.genericBeanDefinition(Bean5.class).getBeanDefinition());
        }

    }


    static class Bean5 {

    }

    static class Bean3 {

    }

    static class Bean4 {

    }

    static class Bean1 {

    }
}
