package com.day04.boot;

import com.day02.LoggerUtils;
import com.day04.boot.sub.Bean1;
import org.springframework.boot.context.TypeExcludeFilter;
import org.springframework.context.annotation.AnnotationConfigUtils;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;

import java.io.IOException;

/**
 * @ClassName TestExcludeFilter
 * @Description 测试排除类
 * @Author linshengyan
 * @Date 2022/6/21 5:36 下午
 */
public class TestExcludeFilter {
    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        AnnotationConfigUtils.registerAnnotationConfigProcessors(context.getDefaultListableBeanFactory());
        context.registerBean(MyConfig.class);
        context.registerBean(MyFilter.class);

        //refresh才会使registerBean的功能生效
        context.refresh();

        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name);
        }
    }

    /**
     * sub包下有Bean1和Bean2两个类,自定义过滤器中排除了bean1，最终只会加载Bean2
     */
    @Configuration
    @ComponentScan(basePackages = {"com.day04.boot.sub"},excludeFilters = { @ComponentScan.Filter(type = FilterType.CUSTOM, classes = TypeExcludeFilter.class)})
    static class MyConfig {
    }


    /**
     * 实现一个自己的filter，不扫描bean1
     * 返回为true就会过滤
     * 返回false就不会过滤
     */
    static class MyFilter extends TypeExcludeFilter {

        @Override
        public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory)
                throws IOException {
            String className = metadataReader.getClassMetadata().getClassName();
            LoggerUtils.get().info("{}", className);
            if (className.equals(Bean1.class.getName())) {
                return true;
            }
            return false;
        }

    }
}
