package com.beanLifeCycle;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @ClassName UserService
 * @Description
 * @Author linshengyan
 * @Date 2022/9/15 4:45 下午
 */
//@Component
public class UserService implements BeanNameAware, BeanFactoryAware, ApplicationContextAware , BeanPostProcessor , InitializingBean
, DisposableBean {


    String beanName;

    BeanFactory beanFactory;

    ApplicationContext applicationContext;


    public UserService(){
        System.out.println("1--->UserService constructor method");
    }

    @Override
    public void setBeanName(String name) {
        this.beanName = name;
        System.out.println("2--->Aware：BeanNameAware.setBeanName()");

    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
        System.out.println("3--->Aware：BeanFactoryAware.setBeanFactory()");

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
        System.out.println("4---Aware：ApplicationContextAware.setApplicationContext()" );
    }

    @PostConstruct
    public void  testPostConstruct(){
        System.out.println("5--->注解方式：test @PostConstruct");
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("6--->初始化之前：BeanPostProcessor.postProcessBeforeInitialization()");
        return bean;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("7--->继承接口：初始化Bean在属性设置之后：InitializingBean.afterPropertiesSet()");

    }

    public void init() {
        System.out.println("8--->自定义方法：@Bean(init-Method = \"initMethod\")");
    }


    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("9--->初始化之后：BeanPostProcessor.postProcessAfterInitialization()");
        return bean;
    }



    //至此，Bean可以使用了



    //以下是Bean的核销流程
    @PreDestroy
    public void testAnnoPreDestory(){
        System.out.println("10--->销毁，注解方式：test @PreDestroy ");

    }

    @Override
    public void destroy() throws Exception {
        System.out.println("11--->销毁，继承接口：DisposableBean.destroy()");
    }

    public void destory(){
        System.out.println("12--->销毁，自定义方法：@Bean(destroy-Method = \"destroyMethod\")");
    }
}
