package com.beanLifeCycle;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @ClassName UserServiceConfig
 * @Description
 * @Author linshengyan
 * @Date 2022/9/15 5:35 下午
 */
@Component
public class UserServiceConfig {

    @Bean(initMethod = "init", destroyMethod = "destory")
    public UserService userService() {
        return new UserService();
    }
}
