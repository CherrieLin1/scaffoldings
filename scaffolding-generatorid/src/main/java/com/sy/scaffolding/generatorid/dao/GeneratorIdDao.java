package com.sy.scaffolding.generatorid.dao;

import com.sy.scaffolding.generatorid.domain.IdSequence;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface GeneratorIdDao {
    /**
     * 根据业务标识查询记录
     * @param businessTag
     * @return
     */
    IdSequence findByBusinessTag(@Param("businessTag") String businessTag);

    /**
     * 根据业务标识和当前最大maxId更新当前最大maxId
     * @param maxId
     * @param businessTag
     * @return
     */
    Integer updateCurrentMaxId(@Param("maxId") Long maxId, @Param("businessTag") String businessTag);

}
