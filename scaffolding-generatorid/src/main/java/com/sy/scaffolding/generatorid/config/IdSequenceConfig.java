package com.sy.scaffolding.generatorid.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix="springboot.phoenix.id.sequence")
public class IdSequenceConfig {
    private Integer retryNumber = 3;

    public Integer getRetryNumber() {
        return retryNumber;
    }

    public void setRetryNumber(Integer retryNumber) {
        this.retryNumber = retryNumber;
    }
}
