package com.sy.scaffolding.common.tools.localCache;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * @ClassName CaffeineConfig
 * @Description
 * @Author linshengyan
 * @Date 2022/4/7 4:51 下午
 */
@Configuration
public class CaffeineConfig {
    public static Integer ONE_DAY_TIME = 60 * 60 * 24;

    @Bean
    public Cache<String, Object> localCache() {
        return Caffeine.newBuilder()
                .softValues()
                .maximumSize(1000)
                .expireAfterWrite(ONE_DAY_TIME, TimeUnit.SECONDS)
                .build();
    }
}
