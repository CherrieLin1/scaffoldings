package com.sy.scaffolding.common.tools.localCache;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.TimeUnit;

/**
 * @ClassName LocalCache
 * @Description
 * @Author linshengyan
 * @Date 2022/4/6 7:29 下午
 */
public class CaffeineUtils {


    /**
     * 1.第一种创建缓存实例的方法
     * 本地缓存的实例，可以这么创建，也可以利用CaffeineConfig里面的方法创建
     */
    private static final Cache<String, Object> caffeineCache =
            Caffeine.newBuilder()
                    //整数，表示能存储多少个缓存对象,为什么要设置初始容量呢？因为如果提前能预估缓存的使用大小，那么可以设置缓存的初始容量，以免缓存不断地进行扩容，致使效率不高。
                    .initialCapacity(100)
                    .softValues()
                    .maximumSize(1000)
                    .expireAfterWrite(CaffeineConfig.ONE_DAY_TIME, TimeUnit.SECONDS)
                    .build();


    /**
     * 2.第二种创建缓存实例的方法
     */
    @Autowired
    static Cache<String, Object> localCache;

    /*--------------> 以下为常用的方法，更多的工作中遇到了可以去官网或者百度 <--------------*/

    /**
     * 本地缓存数据
     *
     * @param key
     * @param value
     */
    public static void putValue(String key, Object value) {
        localCache.put(key, value);
    }


    /**
     * 根据key获取数据
     *
     * @param key
     * @param tClass
     * @param <T>
     * @return
     */
    public static <T> T getValue(String key, Class<T> tClass) {
        Object ifPresent = localCache.getIfPresent(key);
        if (null != ifPresent) {
            return (T) ifPresent;
        }
        return null;
    }

    /**
     * 根据key删除本地缓存数据
     *
     * @param key
     */
    public static void deleteValue(String key) {
        localCache.invalidate(key);
    }


    /**
     * 删除所有的数据
     *
     * @param <T>
     */
    public static <T> void deleteAll() {
        localCache.invalidateAll();
    }


}
