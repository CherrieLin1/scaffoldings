package com.sy.scaffolding.common.tools.objectcomparator;


import java.util.Locale;

/**
 * 反射的类
 * @author create by lsy on 2021/10/18 3:23 下午
 */
public class ReflectUtils {

    public static Object getValueByFiledName(Object object,String filedName){
        Class<?> aClass = object.getClass();

        boolean startWithIs = filedName.startsWith("is");
        Object value = null;
        try {
            if (startWithIs){
                //is开头的属性的get方法的方法名和字段名一样
                value = aClass.getMethod(getSpecialMethodNameByFieldName(filedName)).invoke(object);
            }
        }catch (Exception e){
            System.out.println("获取is开头字段的值发生异常，异常信息为 = " + e.getMessage());

        }


        try {
            if (null == value){
                value = aClass.getMethod(getCommonMethodNameByFieldName(filedName)).invoke(object);

            }
        }catch (Exception e){
            System.out.println("获取普通字段的值发生异常，异常信息为 = " + e.getMessage());
        }

        return value;




    }


    /**
     * 获取普通的方法名字
     * get+"字段的名字"
     * @return
     */
    public static String getCommonMethodNameByFieldName(String filedName){
        String methodName = new StringBuilder().append("get").append(filedName.substring(0,1).toUpperCase(Locale.ROOT)).append(filedName.substring(1)).toString();
        System.out.println("commonMethodName = " + methodName);
        return methodName;
    }

    /**
     * 获取is打头的方法的名字,lombok自动生成的is打头的字段的get方法名字，就是字段的名字
     * @return
     */
    public static String getSpecialMethodNameByFieldName(String filedName){
//        String isStartMethod = new StringBuilder().append(filedName.substring(0,1).toUpperCase(Locale.ROOT)).append(filedName.substring(1)).toString();
//        System.out.println("isStartMethodName = " + isStartMethod);
        return filedName;
    }
}
