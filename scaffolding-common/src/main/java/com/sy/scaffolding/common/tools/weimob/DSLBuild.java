package com.sy.scaffolding.common.tools.weimob;

/**
 * @author create by lsy on 2021/11/3 6:42 下午
 */
public class DSLBuild {

    public static void main(String[] args) {
        //模版 32库，4表
        //code Setting 表32库，4表
        //模版同步表 表32库，4表
        //模版参与对象表 32库 8表
        //模版库存表 表32库，4表
        Integer dbCount = 16;
        Integer tableCount = 4;
        Long bosId = 123L;
        for (int i = 16; i < 32; i++) {
            System.out.println("use promotion_resource_" + i + ";");
            for (int j = 0; j < tableCount; j++) {
                System.out.println("DELETE from promotion_resource_" + i + ".resource_template_" + j + " where bos_id = " + bosId + ";");
                System.out.println("DELETE from promotion_resource_" + i + ".resource_template_stock_" + j + " where bos_id = " + bosId + ";");
                System.out.println("DELETE from promotion_resource_" + i + ".resource_template_sync_" + j + " where bos_id = " + bosId + ";");
                System.out.println("DELETE from promotion_resource_" + i + ".resource_template_code_setting_" + j + " where bos_id = " + bosId + ";");
            }
        }


        for (int i = 16; i < 32; i++) {
            System.out.println("use promotion_resource_" + i + ";");
            for (int j = 0; j < 8; j++) {
                System.out.println("DELETE from promotion_resource_" + i + ".resource_template_participate_" + j + " where bos_id = " + bosId + ";");
            }
        }
    }
}
