package com.sy.scaffolding.common.tools.beanclone;

import org.apache.commons.lang3.SerializationUtils;

import java.io.Serializable;

/**
 * @ClassName BeanCloneUtils  克隆出来的是同一类型的对象
 * @Description 对象的克隆  这里提供统一的方法，可以进行对象的克隆，除此之外对象本身还可以实现Cloneable接口，实现对象深克隆 ：deepClonePerson
 * 的拷贝【深、浅拷贝】
 * @Author linshengyan
 * @Date 2022/3/30 8:29 下午
 */
public class BeanCloneUtils {

    /**
     * 对象必须实现serialize接口，实现对象的深copy
     *
     * @param t
     * @param <T>
     * @return
     */
    public <T extends Serializable> T deepClone(T t) {
        T clone = (T) SerializationUtils.clone(t);
        return clone;
    }



}
