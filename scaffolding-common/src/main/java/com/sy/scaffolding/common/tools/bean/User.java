package com.sy.scaffolding.common.tools.bean;

import com.sy.scaffolding.common.tools.objectcomparator.IgnoreCheck;
import lombok.Data;

/**
 * @author create by lsy on 2021/10/14 7:35 下午
 */
@Data
public class User implements BasePojo {
    private static final long serialVersionUID = 8306231466760650915L;
    /**
     * 姓名
     */
    private String name;

    /**
     * 年龄
     */
    private int age;


    /**
     * 邮箱
     */
    private String email;


    /**
     * 地址
     */
    public String address;


    /**
     * 手机
     */
    protected String phone;


    /**
     * 是否已经删除
     */
    private boolean isDeleted;


    /**
     * 忽略检查的字段
     */
    @IgnoreCheck
    private String ext;

    /**
     * 小狗狗
     */
    private Dog dog;


    public User(int age) {
        this.age = age;
    }

    public User() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    private String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }
}
