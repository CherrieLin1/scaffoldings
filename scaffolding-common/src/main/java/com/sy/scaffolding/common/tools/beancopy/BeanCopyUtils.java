package com.sy.scaffolding.common.tools.beancopy;

import org.springframework.beans.BeanUtils;

/**
 * @ClassName BeanCopyUtils
 * @Description 实现对象的copy，两个不同对象之间的copy
 * @Author linshengyan
 * @Date 2022/3/31 10:55 上午
 */
public class BeanCopyUtils {

    /**
     * 浅copy,利用spring的原生方法
     *
     * @param targetClass
     * @param <T>
     * @return
     */
    public <T> T beanCopy(Object source, Class<T> targetClass) {
        //申明目标对象
        T targetObject = null;
        if (null == source) {
            return null;
        }
        try {
            targetObject = targetClass.newInstance();
            BeanUtils.copyProperties(source, targetObject);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return targetObject;
    }
}
