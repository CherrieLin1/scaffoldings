package com.sy.scaffolding.common.tools.objectcomparator;

import java.lang.reflect.Field;

/**
 * 对象比较器,可以通过注解忽略一下对象的检查
 * @author create by lsy on 2021/10/18 3:22 下午
 */
public class ComparatorUtils {

    /**
     * 比较两个对象的各个字段的值是否一致
     * @param first
     * @param second
     * @param <T>
     * @return
     */
    public static  <T> boolean  Comparator(T first, T second){
        //如果对象有null，直接返回null
        if (null == first || null == second){
            System.out.println("两个对象均不可为空");
            return false;
        }
        if (!first.getClass().equals(second.getClass())) {
            System.out.println("对象类型不一样");
            return false;
        }
        Field[] declaredFields = first.getClass().getDeclaredFields();
        for (Field declaredField : declaredFields) {
            String fieldName = declaredField.getName();
            if (fieldName.equals("serialVersionUID")){
                continue;
            }

            //如果是忽略注释的，直接跳过
            IgnoreCheck annotation = declaredField.getAnnotation(IgnoreCheck.class);
            if (null != annotation){
                continue;
            }

            Object firstValue = ReflectUtils.getValueByFiledName(first,fieldName);
            Object secondValue = ReflectUtils.getValueByFiledName(second,fieldName);

            //如果值为null的话且是String类型的话，都转为空字符串，因为数据库中String类型的字段默认为""
            firstValue = String.class.equals(declaredField.getType())? (firstValue == null ? "":firstValue) : firstValue;
            secondValue = String.class.equals(declaredField.getType())?(secondValue == null ? "":secondValue):secondValue;

            if (firstValue == null && secondValue !=null){
                System.out.println("校验失败，fieldName = " + fieldName);
                return false;
            }

            if (firstValue != null && !firstValue.equals(secondValue)){
                System.out.println("校验失败，fieldName = " + fieldName);
                return false;
            }
        }

        return true;
    }
}
