package com.sy.personnalclassload;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;

/**
 * @ClassName MyClassLoader
 * @Description
 * @Author linshengyan
 * @Date 2022/7/21 8:25 下午
 */
public class MyClassLoader extends ClassLoader {

    public static void test1(){
        System.out.println("一直在循环");
        test1();
    }

    public static void main(String[] args) throws Exception {
//        MyClassLoader mcl = new MyClassLoader();
//        Class<?> c1 = Class.forName("com.xrq.classloader.Person", true, mcl);
//        Object obj = c1.newInstance();
//        //其实就是打印toString方法
//        System.out.println(obj);
//        System.out.println(obj.getClass().getClassLoader());

        test1();
    }

    public MyClassLoader() {

    }

    public MyClassLoader(ClassLoader parent) {
        super(parent);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        File file = getClassFile(name);
        try {
            byte[] bytes = getClassBytes(file);
            Class<?> c = this.defineClass(name, bytes, 0, bytes.length);
            return c;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return super.findClass(name);
    }

    private File getClassFile(String name) {
        //Mac OS X的文件系统区分大小写.Users并且Desktop应该以大写字母开头,你的文件名也应该与大小写相匹配.
        //桌面文件加载方式
        String path="/Users/linshengyan/Desktop/PersonalLearn/Person.class";
        File file = new File(path);
        System.out.println("file.exists() = " + file.exists());
        return file;
    }

    private byte[] getClassBytes(File file) throws Exception {
        // 这里要读入.class的字节，因此要使用字节流
        FileInputStream fis = new FileInputStream(file);
        FileChannel fc = fis.getChannel();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        WritableByteChannel wbc = Channels.newChannel(baos);
        ByteBuffer by = ByteBuffer.allocate(1024);

        while (true) {
            int i = fc.read(by);
            if (i == 0 || i == -1) {
                break;
            }
            by.flip();
            wbc.write(by);
            by.clear();
        }

        fis.close();

        return baos.toByteArray();
    }

}
