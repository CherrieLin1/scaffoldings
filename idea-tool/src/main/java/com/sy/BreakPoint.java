package com.sy;

/**
 * @ClassName BreakPoint
 * @Description
 * @Author linshengyan
 * @Date 2022/8/18 8:53 上午
 */
public class BreakPoint {

    public static final int INT = 100;
    private static String stringInfo;

    public static void main(String[] args) {


    }

    public static void switchCase(int i, String sex) {
        stringInfo = "他的年龄是：";
        if (i > 8) {
            System.out.println(stringInfo + i + sex);
        } else {
            System.out.println(stringInfo + i + sex);

        }


    }


    public static void loadChange() {
        Person person = new Person("shengyan", 3);
        System.out.println(person);
    }

    public static void forLoop() {
        foreach();
    }

    private static void foreach() {
        for (int i = 0; i < INT; i++) {
            if (i == 50) {
                System.out.println("找到特殊的值");
            }
            System.out.println("i =" + i);
        }
    }


    public static void setValue(int age) {
        if (age <= 18) {
            System.out.println("好年轻啊！");
        } else {
            System.out.println("岁月是一把杀猪刀～");
        }

    }

    public static void back() {
        System.out.println("step1");
        System.out.println("调试断点回退");
    }
}
