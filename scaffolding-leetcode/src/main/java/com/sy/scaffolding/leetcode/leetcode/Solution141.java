package com.sy.scaffolding.leetcode.leetcode;


import java.util.HashSet;
import java.util.Set;

/**
 * @ClassName Solution141
 * @Description
 * @Author linshengyan
 * @Date 2022/9/13 2:34 下午
 */
public class Solution141 {
    /**
     * 1.快慢指针法
     * 时间复杂度：O(N),其中 N 是链表中的节点数
     * 空间复杂度：O(1)。我们只使用了两个指针的额外空间。
     *
     * @param head
     * @return
     */
    public boolean hasCycle(ListNode head) {
        //申明快两个指针，一个快，一个慢，循环的条件就是快指针和快指针的next都不为null,因为快指针的每一步next都已经判断了
        //所以慢指针不用判断，可以考虑为一个圆圈，一个操场，跑步快的和跑步慢的同时出发，跑步快的总会追上跑步慢的
        ListNode fast = head;
        ListNode slow = head;
        while (fast != null && fast.next != null) {
            //快指针，一次走两步
            fast = fast.next.next;
            //慢指针，一步走一次
            slow = slow.next;
            if (fast == slow) {
                return true;
            }
        }
        return false;
    }


    /**
     * 集合法
     * 时间复杂度：O(N)，其中 N 是链表中的节点数。最坏情况下我们需要遍历每个节点一次。
     * 空间复杂度：O(N)，其中 N是链表中的节点数。主要为哈希表的开销，最坏情况下我们需要将每个节点插入到哈希表中一次。
     *
     * @param head
     * @return
     */
    public boolean hasCycle1(ListNode head) {
        Set<ListNode> set = new HashSet<>();
        while (head != null) {
            //set如果成功放入了一个数，则返回true，反之false
            if (!set.add(head)) {
                return true;
            }
            //todo 这里不要忘记了head要向前移动一位
            head = head.next;
        }
        return false;
    }
}
