package com.sy.scaffolding.leetcode.leetcode;

import javafx.scene.web.WebHistory;

/**
 * @ClassName Solution206
 * @Description
 * @Author linshengyan
 * @Date 2022/9/9 3:47 下午
 */
public class Solution206 {
    public ListNode reverseList(ListNode head) {
        //申请节点，pre和 cur，pre指向null
        //新节点的头节点，为null
        ListNode pre = null;
        //当前的节点，认为是head节点
        ListNode cur = head;
        //存放临时变量的
        ListNode tmp = null;
        //循环的条件是head节点，即后来的cur节点不为null
        while (cur != null) {
            //当前节点就是head头节点，记录当前节点的下一个节点，放在临时变量temp中
            tmp = cur.next;
            //然后将当前节点指向pre，当前节点是cur,本来当前节点的next节点已经放入临时变量temp中了
            cur.next = pre;

            //pre和cur节点都前进一位
            //pre变成新节点的头节点，每一次都会移动，直至链表完成，变成新节点的头节点
            pre = cur;
            cur = tmp;
        }
        return pre;
    }

    public ListNode reverseList1(ListNode head) {
        ListNode cur = head;
        ListNode pre = null;
        ListNode temp = null;
        while (cur != null) {
            temp = cur.next;
            cur.next = pre;
            pre = cur;
            cur = temp;
        }
        return pre;

    }
}
