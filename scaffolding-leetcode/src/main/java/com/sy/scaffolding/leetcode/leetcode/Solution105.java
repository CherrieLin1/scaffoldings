package com.sy.scaffolding.leetcode.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName Solution105
 * @Description 重建二叉树
 * offer https://leetcode.cn/problems/zhong-jian-er-cha-shu-lcof/
 * @Author linshengyan
 * @Date 2022/9/17 11:41 上午
 */
public class Solution105 {
    public TreeNode buildTree1(int[] preorder, int[] inorder) {
        if (preorder == null || preorder.length == 0) {
            return null;
        }
        Map<Integer, Integer> indexMap = new HashMap<Integer, Integer>();
        int length = preorder.length;
        for (int i = 0; i < length; i++) {
            //indexMap中存放的中序遍历的值和指针，要注意，中序遍历中，根节点的左边的值都是左子树上的值，右边的值都是右子树上的
            indexMap.put(inorder[i], i);
        }
        TreeNode root = buildTree(preorder, 0, length - 1, inorder, 0, length - 1, indexMap);
        return root;

    }

    public TreeNode buildTree(int[] preorder, int preorderStart, int preorderEnd, int[] inorder, int inorderStart, int inorderEnd, Map<Integer, Integer> indexMap) {
        
        if (preorderStart > preorderEnd) {
            return null;
        }
        int rootVal = preorder[preorderStart];
        TreeNode root = new TreeNode(rootVal);
        if (preorderStart == preorderEnd) {
            return root;
        } else {
            //通过indexMap查找根节点的指针
            int rootIndex = indexMap.get(rootVal);
            //节点的个数
            int leftNodes = rootIndex - inorderStart;
            //右节点的个数
            int rightNodes = inorderEnd - rootIndex;
            //重建左子树,preEnd =
            TreeNode leftSubtree = buildTree(preorder, preorderStart + 1, preorderStart + leftNodes, inorder, inorderStart, rootIndex - 1, indexMap);
            //重建右子树
            TreeNode rightSubtree = buildTree(preorder, preorderEnd - rightNodes + 1, preorderEnd, inorder, rootIndex + 1, inorderEnd, indexMap);
            root.left = leftSubtree;
            root.right = rightSubtree;
            return root;
        }
    }
}
