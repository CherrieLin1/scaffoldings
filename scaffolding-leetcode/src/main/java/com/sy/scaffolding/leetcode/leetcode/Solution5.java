package com.sy.scaffolding.leetcode.leetcode;

/**
 * @ClassName Solution5
 * @Description 最长的回文字串
 * @Author linshengyan
 * @Date 2022/9/9 10:44 上午
 */
public class Solution5 {
    public static void main(String[] args) {
//        String s = "babad";
        String s = "cbbd";
        System.out.println("getString(s) = " + longestPalindrome(s));
    }


    /**
     * 动态规划解法
     *
     * @param str
     * @return
     */
    public static String longestPalindrome(String str) {
        int length = str.length();
        //长度为1，本身就是回文字
        if (length < 2) {
            return str;
        }
        char[] chars = str.toCharArray();
        //最长回文字的起始位置，最后截取的时候用
        int start = 0;
        //回文字的最大长度
        int max = 1;
        // dp[i][j] 表示 s[i..j] 是否是回文串
        //以下可以表示为一个二维数组，boolean[i][j],i<=j的情况下是有效的，否则无效，即为false,所以二维数组的左下角都是false，对角线是true,代表一个字符串长度
        //每一次循环都是按照固定长度的字符串进行规划，即二维数组坐上右下的斜对角线
        boolean[][] dp = new boolean[length][length];
        for (int i = 0; i < length; i++) {
            //长度为1的字段串都是true
            dp[i][i] = true;
        }
        //字符串的长度相等的一次循环，是斜对角线的结果
        // 递推开始
        // 先枚举子串长度，最长可以跟字符串一样长，为1的已经在上述标记为正确的回文字了
        for (int L = 2; L <= length; L++) {
            //左边的位置
            for (int i = 0; i < length; i++) {
                //计算右边的位置 j = i + L -1，因为坐标是从0开始的
                int j = i + L - 1;
                //右边界越界，退出循环
                if (j >= length) {
                    break;
                }
                //如果串的左右两个字符不一样，该位置为false
                if (chars[i] != chars[j]) {
                    dp[i][j] = false;
                } else {
                    //如果字符串长度只有2，且相等，则设置为true
                    if (L == 2) {
                        dp[i][j] = true;
                    } else {
                        //如果长度不为2，取决于左右各向中间靠一位的字符串，因为循环是从字符串长度为2开始的，所以短字符串的结果已经是确定的，所以这时候dp[i + 1][j - 1]的值是确定的
                        dp[i][j] = dp[i + 1][j - 1];
                    }
                }
                //只要 dp[i][L] == true 成立，就表示子串 s[i..L] 是回文，此时记录回文长度和起始位置
                if (dp[i][j] && (j - i + 1 > max)) {
                    max = j - i + 1;
                    start = i;
                }
            }

        }

        //subString 左闭右开
        return str.substring(start, start + max);

    }

}
