package com.sy.scaffolding.leetcode.leetcode;

/**
 * @ClassName Solution165
 * @Description 比较两个版本号
 * https://leetcode.cn/problems/compare-version-numbers/?envType=study-plan&id=kuaishou
 * @Author linshengyan
 * @Date 2022/9/8 3:26 下午
 */
public class Solution165 {
    public static void main(String[] args) {
        String version1 = "1";
        String version2 = "1.1";
        compareVersion(version1, version2);
    }

    /**
     * 解题时出现两个错误：
     * 1.`.`是特殊字符，在JAVA代码中需要转义
     IteratorTree     */
    public static int compareVersion(String version1, String version2) {
        String[] split1 = version1.split("\\.");
        String[] split2 = version2.split("\\.");
        //以小的长度为准
        int length = split1.length > split2.length ? split2.length : split1.length;
        //如果在循环中没有跳出去，说明有限长度中，两个的版本号一样
        for (int i = 0; i < length; i++) {
            Integer i1 = Integer.parseInt(split1[i]);
            Integer i2 = Integer.parseInt(split2[i]);
            if (i1.intValue() > i2.intValue()) {
                return 1;
            } else if (i1.intValue() < i2.intValue()) {
                return -1;
            } else {
                //跳过这次循环
                continue;
            }
        }
        //如果1的修订号长于2，且某一段的值大于0，则说明1大
        if (split1.length > split2.length) {
            for (int i = length; i < split1.length; i++) {
                if (Integer.parseInt(split1[i]) > 0) {
                    return 1;
                }
            }
            //如果2的修订号大于1的，且某一段的值大于0，则说明2大
        } else if (split1.length < split2.length) {
            for (int i = length; i < split2.length; i++) {
                if (Integer.parseInt(split2[i]) > 0) {
                    return -1;
                }
            }

        }
        return 0;

    }
}
