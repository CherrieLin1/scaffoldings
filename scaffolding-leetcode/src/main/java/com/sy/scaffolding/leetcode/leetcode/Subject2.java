package com.sy.scaffolding.leetcode.leetcode;

/**
 * 给你两个非空 的链表，表示两个非负的整数。它们每位数字都是按照逆序的方式存储的，并且每个节点只能存储一位数字。
 * <p>
 * 请你将两个数相加，并以相同形式返回一个表示和的链表。
 * <p>
 * 你可以假设除了数字 0 之外，这两个数都不会以 0开头。
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/add-two-numbers
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 * @author create by lsy on 2021/11/25 7:37 下午
 */
public class Subject2 {
    /**
     * 解题思路，两个节点补0对齐相加，考虑进位
     * 解题过程中欠缺的考虑
     * 1.当sum的值大于等于10的时候就需要考虑进位，不能只考虑大于10的情况
     * 2.while循环的条件是，两个链表都已经遍历完成且没有进位，第一次没有考虑进位
     * 3.结果节点的头指针需要保存下来，第一次没有保存，直接返回了结果的尾节点
     * 4.待相加的链表，只有当前的节点不为null的时候才指向下一节点，否则会有空指针异常
     * @param l1
     * @param l2
     * @return
     */
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode result = null;
        ListNode returnNode = null;
        //上一次的进位，初始值为0
        int temp = 0;

        while (null != l1 || null != l2 || temp != 0) {
            //当前节点的和为两个节点的值想加再加上上一次的进位
            int sum = (l1 == null ? 0 : l1.val) + (l2 == null ? 0 : l2.val) + temp;
            //如果值大于等于10，进位为1，当前值减10
            if (sum >= 10) {
                sum = sum - 10;
                temp = 1;
            } else {
                temp = 0;
            }
            //头节点，new一个对象
            if (null == result) {
                result = new ListNode(sum);
                returnNode = result;
            } else {
                //当前节点的下一个节点值生成
                result.next = new ListNode(sum);
                //当前节点设置为下一个节点
                result = result.next;

            }
            l1 = l1 == null ? null : l1.next;
            l2 = l2 == null ? null : l2.next;
        }
        return returnNode;
    }
}
