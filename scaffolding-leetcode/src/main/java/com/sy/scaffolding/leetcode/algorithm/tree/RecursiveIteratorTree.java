package com.sy.scaffolding.leetcode.algorithm.tree;

/**
 * https://blog.csdn.net/weixin_45685353/article/details/106694041?utm_medium=distribute.pc_relevant.none-task-blog-2~default~baidujs_title~default-0.no_search_link&spm=1001.2101.3001.4242.1
 * 递归遍历
 * @author create by lsy on 2021/11/15 2:36 下午
 */
public class RecursiveIteratorTree {

    /**先序遍历
     *
     * @param root 根节点
     * @param sb 用于拼接节点的值
     */
    public void preorder(Node root,StringBuilder sb){
        if(root != null){
            //访问根节点
            sb.append(root.data);
            //先序遍历左子树
            preorder(root.leftChild,sb);
            //先序遍历右子树
            preorder(root.rightChild,sb);
        }
    }


    /**中序遍历
     *
     * @param root 根节点
     * @param sb 用于拼接节点的值
     */
    public void inorder(Node root,StringBuilder sb){
        if(root != null){
            //中序遍历左子树
            inorder(root.leftChild,sb);
            //访问根节点
            sb.append(root.data);
            //中序遍历右子树
            inorder(root.rightChild,sb);
        }
    }



    /**后序遍历
     *
     * @param root 根节点
     * @param sb 用于拼接节点的值
     */
    public void postorder(Node root,StringBuilder sb){
        if(root != null){
            //后序遍历左子树
            inorder(root.leftChild,sb);
            //后序遍历右子树
            inorder(root.rightChild,sb);
            //访问根节点
            sb.append(root.data);
        }
    }



}
