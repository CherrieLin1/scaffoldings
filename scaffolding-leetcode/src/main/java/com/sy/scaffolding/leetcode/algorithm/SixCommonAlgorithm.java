package com.sy.scaffolding.leetcode.algorithm;

/**
 * @author create by lsy on 2021/11/8 5:50 下午
 */
public class SixCommonAlgorithm {
    /**
     * 1.冒泡排序  对比的次数是N-1次,两两比较，将最大的是挪到最后，经过N次排序，最后的N个数是有序的
     * 复杂度：n*n
     * 要点：
     * 1.标识符，如果第一次没有变化，则是有序的
     * 2.排序的次数length-1
     * 3.第i次比较的范围 0到length-i（i从1开始）
     *
     * @param array 待排序的数组
     * @return 排好序的数组
     */
    public static int[] bubbleSort(int[] array) {
        boolean flag = true;
        //1.第几次排序
        for (int i = 1; i < array.length; i++) {
            //2.每次都是从0开始，第i次拍完序后，数组的后I数都是有序的
            for (int j = 0; j < array.length - i; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                    flag = false;
                }
            }
            if (flag) {
                break;
            }
            System.out.println("冒泡第" + i + "次排序的结果为");
            display(array);

        }
        return array;
    }


    /**
     * 2.选择排序   需要N-1轮比较   每一次都是将最小的数选择出来，插入到当前需要排序的位置
     * 复杂度：n*n
     * 要点：
     * 开始选取数组的第一个数为最小的数，然后每一次往后找，只要只要比这个数小的数，就设置min的标志符为这个下标
     * 每一轮比较完成后，看选定的最小的数的下标是否和真实的最小的数的下标一致，如果不一致就交换位置
     * 第一轮拍完序，第一个数最有序，最小，第二轮拍完序后第二个数最小，以此类推
     * 1.比较的轮数  len -1
     * 2.每一轮待插入元素的位置
     * 3.比较，如果该位置不是最小的数值，将最小的数和待排序的数对调
     *
     * @param array 待排序的数组
     * @return 已经排好序的数组
     */
    public static int[] choiceSort(int[] array) {
        //1.需要进行length-1次排序，第N次将第N个数当成是最小的
        for (int i = 0; i < array.length - 1; i++) {
            int min = i;
            //2.第i轮，从i +1个元素之后开始比较
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[min]) {
                    min = j;
                }
            }
            //3.一轮比较完毕之后，发现最小的元素位置不是i，则交换位置
            if (i != min) {
                int temp = array[i];
                array[i] = array[min];
                array[min] = temp;
            }
            System.out.println("选择第" + (i + 1) + "次排序的结果为");
            display(array);
        }
        return array;
    }


    /**
     * 3.直接插入排序
     * 复杂度：n*n
     * 只要要n-1轮比较
     * 要点：首先要选择好待排序的那个数，在while循环中参与比较的数是temp,待比较的数
     *
     * @param array 倒排序的数组
     * @return 排好序的数组
     */
    public static int[] straightInsert(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int j = i + 1;
            //temp = current data，一直保持这个值，直到合适的位置找到位置
            int temp = array[j];
            //如果当前需要比较的值，还比前一个值小的话，前一个值就后退一位，放在当前需要比较的数值的索引上
            while (j > 0 && temp < array[j - 1]) {
                array[j] = array[j - 1];
                j--;
            }
            //此时的j已经是j--位置了，temp的值一直没有变
            array[j] = temp;
            System.out.println("直接插入第" + i + "次排序的结果为");
            display(array);
        }
        return array;
    }

    /**
     * low and high is the index of the array
     * 4.快速排序-冒泡排序的改进版，原理是选择一个基准位，每一次元数组分成两部分，一部分比基准数大，另一部分比基准数小
     * 注意：这个是简单版的快速排序，直接选择第一个数为基准数
     * 以下例子利用了分治算法（Divide and conquer algorithm）的快速排序，选择一个目标数字，将待排序的数组分成两个，
     * 一部分都比这个数字大，一部分都比这个数字小
     * 平均时间复杂度O(nlogn)
     * 最差的情况：n*n
     * <p>
     * <p>
     * 减少冒泡排序过程中比较的次数
     * 以下是双边循环快排
     */
    public static void quickSort(int[] arr, int low, int high) {
        int i, j, base, temp;
        if (low > high) {
            return;
        }
        i = low;
        j = high;
        //base是基准位，选择第一个数字为基准位，目前就是为了两数组分成两个部分，右边的大于基准值，左边的小于基准值
        //这个基准值是的赋值需要放到low和high的数字比较之后，因为可能会出现数组长度越界的情况
        base = arr[low];
        while (i < j) {
            //从数组右边的数据开始看，如果基准值是小于当前值的【因为数组要求升序排列，比基准值大的不用动】，当前指针做减法
            while (base <= arr[j] && i < j) {
                j--;
            }
            //再看数组的左边【即数组开始的位置】，如果基准值大于这个数，当前指针做加法
            while (base >= arr[i] && i < j) {
                i++;
            }
            //此时左边发现比其小的数，右边发现比其大的数，如果条件满足则交换
            temp = arr[j];
            arr[j] = arr[i];
            arr[i] = temp;
        }
        //走到这里说明i = j
        // 最后将基准与i和j相等位置的数字调换，因为在上述交换的过程中，base上的数据肯定是不变的，也就是arr[low]不变
        arr[low] = arr[i];
        arr[i] = base;
        //递归调用左半边
        quickSort(arr, low, j - 1);
        //递归（
        //递归算法（recursive algorithm）调用右半边，因为数字J在的位置已经是有序的，所以不再参与排序
        quickSort(arr, j + 1, high);

    }


    /**
     * 2h的shell排序，是直接插入排序的改进，
     * 希尔排序通过加大插入排序中元素的间隔，并在这些有间隔的元素中进行插入排序，从而使数据项能够大跨度的移动。
     * 2h = array.length(),向下取整
     * O(n^(1.3—2))
     *
     * @param array
     */
    public static void shellSortFor2h(int[] array) {
        int step;
        int len = array.length;
        //第一个for循环是数组循环的条件
        for (step = len / 2; step > 0; step /= 2) {
            //第二个循环是，每一次i的值都从步长开始，直到数组的最大值，比较的是当前的值和步长距离的值【每个步长间隔进行比较】
            for (int i = step; i < len; i++) {
                int j = i;
                int temp = array[j];
                //while循环的目的是，当步长足够小的时候，会有多个元素参与对比
                //有一个疑惑是，当元素为多个的时候，如果左边两个元素对比不成功，那么右边的元素更加不会进行对比了，因为不满足for循环的条件
                //解答是：当步长足够大的时候，在前面的比对中，大元素已经全部被移动到右边了
                while (j - step >= 0 && temp < array[j - step]) {
                    array[j] = array[j - step];
                    j -= step;
                }
                array[j] = temp;
            }
        }
    }


    /**
     * 二分查找 ：利用二分查找从一个有序数组中快速找到目标值
     * 时间复杂度：O(h)=O(log2n)
     */
    public static int binarySearch(int[] a, int target) {
        int front = 0;
        int end = a.length - 1;
        int temp;
        while (front <= end) {
            //长度有可能超过int的最大值
            //temp = (front + end) / 2;
            temp = front + (end - front) / 2;
            if (target == a[temp]) {
                return temp;
            } else if (target > a[temp]) {
                front = temp + 1;
            } else {
                end = temp - 1;
            }
        }
        return -1;
    }

//    public static void main(String[] args) {
//        int[] array = {1, 5, 8, 11, 19, 22, 31, 35, 40, 45, 48, 49, 50};
//        int target = 1;
//        int idx = binarySearch(array, target);
//        System.out.println(idx);
//    }


    //遍历显示数组
    public static void display(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] array = {9, 8, 7, 6, 5, 4, 3, 2, 1, 1, 0};
        int[] sortedArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] num = {3, 2, 1, 5, 6, 4};

//        int[] array1 = {1,3,5,7,9,10};
        //未排序数组顺序为
        System.out.println("未排序数组顺序为：");
        display(array);
        System.out.println("-----------------------");
        //冒泡排序
        shellSortFor2h(array);
//        quickSort(num,0,num.length-1);
        System.out.println("-----------------------");
        System.out.println("经过排序后的数组顺序为：");
        display(array);
    }

}
