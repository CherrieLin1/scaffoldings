package com.sy.scaffolding.leetcode.leetcode;

import java.util.Stack;

/**
 * @ClassName Solution7
 * @Description 反转一个整数，和第八题有点像
 * 注意怎么判断有没有整数溢出的
 * @Author linshengyan
 * @Date 2022/9/13 3:41 下午
 */
public class Solution7 {
    /**
     * 方法一：用取余的方法依次求的余数，求取的过程中判断是否溢出
     * 时间复杂度O(n)，其实就是数字的长度
     * 空间复杂度为O(2)，只申请了两个变量存放数据
     *
     * @param x
     * @return
     */
    public int reverse(int x) {
        //结果
        int result = 0;
        //上一次的结果
        int last = 0;
        //x为每次循环处以10的余数
        while (x != 0) {
            //每次取末尾的数字
            int temp = x % 10;
            last = result;
            //本次循环的结果
            result = result * 10 + temp;
            //如果这一次的循环的结果除以10和上一次的结果不一致，则说明溢出，返回0
            if (last != result / 10) {
                return 0;
            }
            x /= 10;
        }
        return result;
    }


    /**
     * 官方解法，只申请了一个变量，一次遍历、
     * 时间复杂度O(n)，其实就是数字的长度
     * 空间复杂度为O(2)，只申请了两个变量存放数据
     *
     * @param x
     * @return
     */
    public int reverse2(int x) {
        int res = 0;
        while (x != 0) {
            if (res < Integer.MIN_VALUE / 10 || res > Integer.MAX_VALUE / 10) {
                return 0;
            }
            int temp = x % 10;
            res = res * 10 + temp;
            x /= 10;
        }
        return res;
    }


    /**
     * 第三种解法：栈加字符串，以前提交的，略麻烦
     * @param x
     * @return
     */
    public int reverse3(int x) {
        int result;
        Stack<Character> stack = new Stack<>();
        String str = String.valueOf(x);
        for (int i = 0; i < str.length(); i++) {
            stack.push(str.charAt(i));
        }
        StringBuilder builder = new StringBuilder();
        while (!stack.empty()) {
            builder.append(stack.pop());
        }
        String strReverse = builder.toString();
        // 没有找到-号 说明是正数，直接判断有没有大于最大值  若没有直接返回
        if (strReverse.indexOf('-') == -1) {
            result = strToInt(strReverse);
        } else {
            strReverse = strReverse.substring(0, strReverse.length() - 1);
            strReverse = "-" + strReverse;
            result = strToInt(strReverse);
        }

        return result;
    }

    public int strToInt(String str) {
        try {
            return Integer.parseInt(str);
        } catch (Exception e) {
            return 0;
        }
    }

}
