package com.sy.scaffolding.leetcode.leetcode;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * @ClassName Solution20
 * @Description 有效的括号
 * @Author linshengyan
 * @Date 2022/9/13 4:04 下午
 */
public class Solution20 {
    public static void main(String[] args) {
        String str = "()";
        System.out.println("isValid(str) = " + isValid(str));
    }


    /**
     * 提交时候的错误在于if (!stack.isEmpty()) 这段代码写在了for循环里面
     * @param s
     * @return
     */
    public static boolean isValid(String s) {

        Map<Character, Character> map = new HashMap<>(3);
        map.put(')', '(');
        map.put('}', '{');
        map.put(']', '[');

        //若Stack的初始化名字为stack会有问题
        Stack<Character> stack1 = new Stack<Character>();
        System.out.println("stack.size() = " + stack1.size());

        for (int i = 0; i < s.length(); i++) {
            Character c = s.charAt(i);
            if (c.equals('[') || c.equals('{') || c.equals('(')) {
                stack1.push(c);
            } else {
                Character character = map.get(c);
                if (stack1.isEmpty() || !stack1.pop().equals(character)) {
                    return false;
                }
            }
        }
        //stack不是空的
        if (!stack1.isEmpty()) {
            return false;
        }

        return true;
    }


}
