package com.sy.scaffolding.leetcode.leetcode;



import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName Solution268
 * @Description    给定一个包含 [0, n] 中 n 个数的数组 nums ，找出 [0, n] 这个范围内没有出现在数组中的那个数。
 * https://leetcode.cn/problems/missing-number/?envType=study-plan&id=kuaishou
 * @Author linshengyan
 * @Date 2022/9/8 2:16 下午
 */
public class Solution268 {
    /**
     * 解法一：将数据放入集合中，然后遍历数组长度，找出不符合条件的数字
     * 执行时间1106ms
     * @param nums
     * @return
     */
    public int missingNumber(int[] nums) {
        //难点一：数组转列表
        //方式一，通过数据流转化，方式二，借助commom-lang工具，先将nums[]转为Integer[],此处不演示
        List<Integer> collect = Arrays.stream(nums).boxed().collect(Collectors.toList());

        int length = nums.length;
        //n个数字，最大的为N
        for (int i = 0; i <= length; i++) {
            if (!collect.contains(i)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 解法二：直接对数据进行排序，遍历数组，如果array[i] != i ,则缺失的数字便是i,如果都符合，那就是N没有
     * 执行时间6ms
     * @param nums
     * @return
     */
    public int missingNumber1(int[] nums) {
        Arrays.sort(nums);
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != i ) {
                return i;
            }
        }
        return nums.length;
    }
}
