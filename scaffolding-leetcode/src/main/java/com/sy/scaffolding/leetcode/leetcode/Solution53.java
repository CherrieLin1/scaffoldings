package com.sy.scaffolding.leetcode.leetcode;

/**
 * @ClassName Solution53
 * @Description
 * @Author linshengyan
 * @Date 2022/9/13 2:19 下午
 */
public class Solution53 {
    /**
     * 这一题题目中要求的分治算法不是最优解，且难以理解，放弃
     * 比较简单，且自己比较容易理解的是《正整数增益》，即一段数的sum>0,那么对后面的继续连续相加是有益的，反之，无益处
     * define int sum【每一次计算的结果】; int next【下一个需要遍历的数】;max int【记录每一次比较后最大的值】;
     * sum > 0 ;sum = sum + next; max(max,sum)
     * sum < 0; sum = next [下一次循环的时候,判断的还是sum]
     *
     * @param nums
     * @return
     */
    public int maxSubArray(int[] nums) {
        int sum = 0;
        //这里result初始值应该定义为nums[0],而不应该是0，因为可能里面就一个数,且这个数是负数，这时候输出的结果就会有问题了
        int result = nums[0];
        for (int num : nums) {
            if (sum > 0) {
                sum = sum + num;
            } else {
                sum = num;
            }
            result = Math.max(result, sum);
        }
        return result;
    }
}
