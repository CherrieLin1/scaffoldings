package com.sy.scaffolding.leetcode.algorithm;


/**
 * @ClassName TestTry
 * @Description
 * @Author linshengyan
 * @Date 2022/7/20 10:32 上午
 */
public class TestTry {
    public static void bubbleSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            for (int j = 0; j < array.length - i; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
            System.out.println("冒泡第" + i + "次排序的结果为");
            display(array);
        }

    }


    public static void choiceSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int min = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[min]) {
                    min = j;
                }
            }
            if (i != min) {
                int temp = array[i];
                array[i] = array[min];
                array[min] = temp;
            }
            System.out.println("选择第" + (i + 1) + "次排序的结果为");
            display(array);
        }


    }


    public static void straightInsert(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int j = i + 1;
            int temp = array[j];
            while (j > 0 && temp < array[j - 1]) {
                array[j] = array[j - 1];
                j--;
            }
            array[j] = temp;
            System.out.println("直接插入第" + i + "次排序的结果为");
            display(array);
        }

    }

    public static void quickSort(int[] array, int low, int high) {
        if (low > high) {
            return;
        }
        int left = low;
        int right = high;
        int base = array[low];
        while (left < right) {
            while (base <= array[right] && left < right) {
                right--;
            }
            while (base >= array[left] && left < right) {
                left++;
            }
            int temp = array[left];
            array[left] = array[right];
            array[right] = temp;
        }
        //left == right
        array[low] = array[left];
        array[left] = base;

        quickSort(array, low, left - 1);
        quickSort(array, left + 1, high);


    }

    public static void shellSortFor2h(int[] array) {
        int step;
        int len = array.length;
        for (step = len / 2; step > 0; step /= 2) {
            for (int i = step; i < len; i++) {
                int j = i;
                int temp = array[j];
                //array[j - step]写错成j-temp
                while (j - step >= 0 && temp < array[j - step]) {
                    array[j] = array[j - step];
                    j -= step;
                }
                array[j] = temp;
            }
        }
    }

    public static int binarySearch(int[] a, int target) {
        int left = 0;
        int right = a.length - 1;
        int temp;
        while (left <= right) {
            //防止溢出
            temp = left + (right - left) / 2;
            if (target == a[temp]) {
                return temp;
            } else if (target > a[temp]) {
                left = temp + 1;
            } else {
                right = temp - 1;
            }
        }
        return -1;
    }


    //遍历显示数组
    public static void display(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }


    public static void main(String[] args) {
        int[] array = {0, 9, 8, 9, 7, 6, 5, 3, 4, 3, 2, 2, 1, 1, 0};
        //未排序数组顺序为
        System.out.println("未排序数组顺序为：");
        display(array);
        System.out.println("-----------------------");
//        quickSort(array, 0, array.length - 1);
        shellSortFor2h(array);
        System.out.println("-----------------------");
        System.out.println("经过排序后的数组顺序为：");
        display(array);
    }

}
