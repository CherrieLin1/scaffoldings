package com.sy.scaffolding.leetcode.leetcode.subject1;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.SynchronousQueue;

/**
 * @ClassName Solution
 * @Description
 * @Author linshengyan
 * @Date 2022/8/12 2:45 下午
 */
public class Solution {
    public static int majorityElement(int[] nums) {
            int candidate = -1;
            int count = 0;
            for (int num : nums) {
                if (count == 0) {
                    candidate = num;
                }
                if (num == candidate) {
                    count++;
                } else {
                    count--;
                }
            }
            count = 0;
            int length = nums.length;
            for (int num : nums) {
                if (num == candidate) {
                    count++;
                }
            }
            return count * 2 > length ? candidate : -1;
    }

    public static void main(String[] args) {
        int[] a = new int[]{5,5,5,5,6,8,7,7,9,5};
        majorityElement(a);
    }
}