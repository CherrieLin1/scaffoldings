package com.sy.scaffolding.leetcode.leetcode;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * 括号的匹配
 * @author create by lsy on 2021/11/22 9:55 上午
 */
public class Test1 {
    private String name = "shengyan";
    public static void main(String[] args) {
        String string ="([a+b]-(rr{}))";
        boolean res =  match(string);
        System.out.println(res);
    }
    public static boolean match(String str) {
        Map<Character,Character> map = new HashMap<>();
        map.put(')', '(');
        map.put(']', '[');
        map.put('}', '{');
        Stack<Character> stack =new Stack<>();
        for(int i=0;i<str.length();i++) {
            Character c =str.charAt(i);
            if(map.containsValue(c)) {//左括号入栈
                stack.push(c);
            }
            else if(map.containsKey(c)) {//右括号出栈匹配
                if(stack.empty()) {
                    return false;
                }
                if(stack.peek().equals(map.get(c))) {
                    stack.pop();
                }else {
                    return false;
                }
            }
        }
        return stack.empty()?true:false;
    }
}
