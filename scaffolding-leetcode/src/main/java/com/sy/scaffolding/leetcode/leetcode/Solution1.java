package com.sy.scaffolding.leetcode.leetcode;

import javax.naming.directory.NoSuchAttributeException;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName Solution1
 * @Description
 * @Author linshengyan
 * @Date 2022/9/9 9:39 上午
 */
public class Solution1 {
    /**
     * 1.每组都有唯一的解
     * 2.下标不能重复
     *
     * @param nums
     * @param target
     * @return
     */
    public int[] twoSum(int[] nums, int target) {
        //<数值，下标>
        Map<Integer, Integer> map = new HashMap<>(nums.length);
        for (int i = 0; i < nums.length; i++) {
            int temp = target - nums[i];
            if (map.containsKey(temp)) {
                return new int[]{i, map.get(temp)};
            }
            map.put(nums[i], i);
        }
        //不合法的参数异常
        throw new IllegalArgumentException("no solution");
    }


}
