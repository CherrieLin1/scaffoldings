package com.sy.scaffolding.leetcode.leetcode;

/**
 * @ClassName Solution2
 * @Description 解题的思想就是两个数相加的时候，就是低位对齐相加，有进位，向前进一位即可
 * 而给出了数据的反向链表，从链表的尾部相加，就满足两个数据相加的原理
 * @Author linshengyan
 * @Date 2022/9/9 10:02 上午
 */
public class Solution2 {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        //当前操作的节点，每加一个数据，指针就要往前移动一位
        ListNode result = null;
        //返回值--头节点，链表返回的时候只能是头节点
        ListNode returnNode = null;
        //上一次的进位，初始值为0
        int temp = 0;
        //只要进位，或者两个数字任何一个不为null，就需要继续循环
        while (null != l1 || null != l2 || 0 != temp) {
            //当前节点的和为两个节点的值想加再加上上一次的进位
            int sum = (l1 == null ? 0 : l1.val) + (l2 == null ? 0 : l2.val) + temp;
            //如果值大于0，进位为1，当前值减10
            if (sum >= 10) {
                sum = sum - 10;
                temp = 1;
            } else {
                temp = 0;
            }
            //头节点，new一个对象
            if (null == result) {
                result = new ListNode(sum);
                returnNode = result;
            } else {
                //当前节点的下一个节点值生成
                result.next = new ListNode(sum);
                //当前节点设置为下一个节点
                result = result.next;

            }
            l1 = l1 == null ? null : l1.next;
            l2 = l2 == null ? null : l2.next;
        }
        return returnNode;
    }
}
