package com.sy.scaffolding.leetcode.algorithm.tree;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Stack;

/**
 * https://blog.csdn.net/qiuxinfa123/article/details/84880786
 * 非递归遍历
 * @author create by lsy on 2021/11/15 2:36 下午
 */
public class IteratorTree {

    /**
     *   2.先序遍历。非递归先序遍历的思路如下：
     *     1.先将根节点入栈
     *     2.访问根节点
     *     3.如果根节点存在右孩子，则将右孩子入栈
     *     4.如果根节点存在左孩子，则将左孩子入栈（注意：一定是右孩子先入栈，然后左孩子入栈）
     *     5.重复2-4
     * 前根序遍历
     * 根左右
     * @param root
     */
    public void preOrder(Node root){
        if (null == root){
            System.out.println("空树");
            return;
        }
        Stack<Node> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()){
            Node temp  = stack.pop();
            int data = temp.data;
            System.out.println(data);
            if (null != temp.rightChild){
                stack.push(temp.rightChild);
            }
            if (null != temp.leftChild){
                stack.push(temp.leftChild);
            }
        }
    }


    /**先序遍历
     * Deque:双端队列
     * queue:队列
     * @param root 根节点
     * @param sb 用于拼接节点的值
     */
    public void preorder(Node root,StringBuilder sb){
        if(root != null){
            Deque<Node> stack = new ArrayDeque<>();
            //根节点先入栈
            stack.addLast(root);
            while(!stack.isEmpty()){
                Node tn = stack.removeLast();
                //访问节点
                sb.append(tn.data);
                //右节点存在，右节点先入栈
                if(tn.rightChild != null) {
                    stack.addLast(tn.rightChild);
                }
                //左节点存在，左节点后入栈
                if(tn.leftChild != null){
                    stack.addLast(tn.leftChild);
                }
            }
        }
    }


    /**
     * 3.中序遍历。   非递归中序遍历的思路如下：
     *     1.先将根节点入栈
     *     2.将当前节点的所有左孩子入栈，直到左孩子为空
     *     3.访问栈顶元素，如果栈顶元素存在右孩子，则继续第2步
     *     4.重复第2、3步，直到栈为空并且所有的节点都被访问
     *
     */
    public void inOrder(Node Root) {
        if(Root==null) {
            System.out.println("空树");
            return;
        }
        Node tmp=Root;
        Stack<Node> s=new Stack<Node>();
        while(tmp!=null || !s.empty()) {
            //1.将根节点入栈
            //2.将所有左孩子入栈
            while(tmp!=null) {
                s.push(tmp);
                tmp=tmp.leftChild;
            }
            //3.访问栈顶元素
            tmp=s.pop();
            System.out.print(tmp.data+" ");
            //4.如果栈顶元素存在右孩子，则将右孩子赋值给tmp，也就是将右孩子入栈
            if(tmp.rightChild!=null) {
                tmp=tmp.rightChild;
            }
            //否则，将tmp置为null，表示下次要访问的是栈顶元素
            else {
                tmp=null;
            }
        }
        System.out.println();
    }


    /**中序遍历
     *
     * @param root 根节点
     * @param sb 用于拼接节点的值
     */
    public void inorder(Node root,StringBuilder sb){
        if(root != null){
            Deque<Node> stack = new ArrayDeque<>();
            while (!stack.isEmpty() || root !=null){
                while (root != null){
                    stack.addLast(root);
                    root = root.leftChild;
                }
                if(!stack.isEmpty()){
                    root = stack.removeLast();
                    sb.append(root.data);
                    root = root.rightChild;
                }
            }
        }
    }



    /**
     * 4.后序遍历。     后续遍历的非递归实现思路：
     *     1.根节点入栈
     *     2.将根节点的左子树入栈，直到最左，没有左孩子为止
     *     3.得到栈顶元素的值，先不访问，判断栈顶元素是否存在右孩子，如果存在并且没有被访问，则将右孩子入栈，否则，就访问栈顶元素
     * @param Root
     */
    public void postOrder(Node Root) {
        if(Root==null) {
            System.out.println("空树");
            return;
        }
        //当前节点
        Node tmp=Root;
        //上一次访问的节点
        Node prev=null;
        Stack<Node> s=new Stack<Node>();
        while(tmp!=null || !s.empty()) {
            //1.将根节点及其左孩子入栈
            while(tmp!=null) {
                s.push(tmp);
                tmp=tmp.leftChild;
            }

            if(!s.empty()) {
                //2.获取栈顶元素值
                tmp=s.peek();
                //3.没有右孩子，或者右孩子已经被访问过
                if(tmp.rightChild==null || tmp.rightChild==prev) {
                    //则可以访问栈顶元素
                    tmp=s.pop();
                    System.out.print(tmp.data+" ");
                    //标记上一次访问的节点
                    prev=tmp;
                    tmp=null;
                }
                //4.存在没有被访问的右孩子
                else {
                    tmp=tmp.rightChild;
                }
            }
        }
        System.out.println();
    }


    /**后序遍历
     *
     * @param root 根节点
     * @param sb 用于拼接节点的值
     */
    public void postorder(Node root,StringBuilder sb){
        if(root != null){
            Deque<Node> stack = new ArrayDeque<>();
            stack.addLast(root);
            while (!stack.isEmpty()){
                root = stack.removeLast();
                sb.append(root.data);
                //注意这里跟先序遍历相反，最后的结果需对sb逆序输出即sb.reverse()
                if(root.leftChild != null){
                    stack.addLast(root.leftChild);
                }
                if (root.rightChild != null){
                    stack.addLast(root.rightChild);
                }
            }
        }
    }


    /**层次遍历
     *
     * 层次遍历
     * 操作： 建立一个循环队列，先将二叉树头节点入队列，然后出队列，访问该节点，如果它有左子树，则将左子树入队列，如果它有右子树，则将右子树入队列。然后出队列，对出队节点访问，如此反复，直到队列为空为止。
     * 图1二叉树的层次遍历结果为：A，B，E，C，D，F
     * @param root 根节点
     * @param sb 用于拼接节点的值
     */
    public void level(Node root,StringBuilder sb){
        if(root != null){
            Deque<Node> que = new ArrayDeque<>();
            //先将根节点入队列
            que.addLast(root);
            while (!que.isEmpty()){
                //出队列
                root = que.removeFirst();
                //访问该节点
                sb.append(root.data);
                //左子树存在，入队列
                if (root.leftChild != null){
                    que.addLast(root.leftChild);
                }
                //右子树存在，入队列
                if (root.rightChild != null){
                    que.addLast(root.rightChild);
                }
            }
        }
    }


}
