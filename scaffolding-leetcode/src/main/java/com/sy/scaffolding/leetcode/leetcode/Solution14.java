package com.sy.scaffolding.leetcode.leetcode;

/**
 * @ClassName Solution14
 * @Description 最长公共前缀
 * @Author linshengyan
 * @Date 2022/9/9 4:19 下午
 */
public class Solution14 {


    public String longestCommonPrefix1(String[] strs) {
        if (strs.length == 0) {
            return "";
        }
        String res = strs[0];
        for (int i = 1; i < strs.length; i++) {
            int j = 0;
            while (j < res.length() && j < strs[i].length() && res.charAt(j) == strs[i].charAt(j)) {
                j++;
            }
            res = res.substring(0, j);

            //这一段可加可不加
            if (res == "") {
                return "";
            }

        }
        return res;
    }


    public String longestCommonPrefix(String[] strs) {
        if (strs.length == 0) {
            return "";
        }
        // 初始值为首位元素
        String res = strs[0];
        for (int i = 1; i < strs.length; i++) {
            int j = 0;
            // 挨着对比
            while (j < res.length() && j < strs[i].length() && res.charAt(j) == strs[i].charAt(j)) {
                j++;
            }
            // substring 是左闭右开的
            res = res.substring(0, j);
        }
        return res;
    }
}
