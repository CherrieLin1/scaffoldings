package com.sy.scaffolding.leetcode.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName Solution3
 * @Description leetcode的第三题  给定一个字符串 s ，请你找出其中不含有重复字符的 最长子串 的长度。
 * 0 <= s.length <= 5 * 104
 * s 由英文字母、数字、符号和空格组成
 * https://leetcode.cn/problems/longest-substring-without-repeating-characters/
 * @Author linshengyan
 * @Date 2022/9/8 10:08 上午
 */
public class Solution3 {
    /**
     * 本题的解题思想是滑动窗口法，在滑动的时候，对比记录每个窗口期max的最大值
     * left是窗口最左边的位置
     * @param s
     * @return
     */
    public static int lengthOfLongestSubstring(String s) {
        if (null == s || s.length() == 0) {
            return 0;
        }
        HashMap<Character, Integer> map = new HashMap<Character, Integer>();
        //最长字串的长度
        int max = 0;

        //新的子串开始左边的位置，窗口滑动
        int left = 0;

        for (int i = 0; i < s.length(); i++) {
            if (map.containsKey(s.charAt(i))) {
                //找到当前左边的位置，选择Max是因为重复的数据有可能不在当前的字串中，例如【1543145】，left指在3的位置，右指针指在5这个位置时
                //map.get(s.charAt(i))成立，说明当前位置是重复的值，如果这时候值在left的右边，左指针必须滑动到这个值的右一位，如果这时候指针在left左边，就是上述说的情况，还是以当前left的位置为准
                //todo 言简意赅看这里的解释  map.get(s.charAt(i)) + 1说明当前指针在left的右边，否则在他左边
                left = Math.max(left, map.get(s.charAt(i)) + 1);
            }
            map.put(s.charAt(i), i);
            max = Math.max(max, i - left + 1);
        }
        return max;
    }


    /**
     * 这个方法是求最长子串，而不简单的是最长的长度，因为最长子穿可能出现在前面，也可能出现在后面，也可能出现多个，所以解决的方案是：
     * 记录下最长max的时候，left的位置，结束的时候取出map中对应位置的key
     * 默认只出现一个，否则题目太难了
     *
     * @param s
     * @return
     */
    public static String longestSubstring(String s) {
        if (null == s || s.length() == 0) {
            return "";
        }
        int left = 0;
        //max最大值时left的值，最后返回值取str，当前默认只有一个最长的str，否则取的是最后一个的长度
        int maxLeft = 0;
        int max = 0;
        Map<Character, Integer> map = new HashMap<Character, Integer>(s.length());
        for (int i = 0; i < s.length(); i++) {
            if (map.containsKey(s.charAt(i))) {
                left = Math.max(left, map.get(s.charAt(i)) + 1);
            }
            //将值放入到Map中
            map.put(s.charAt(i), i);

            //如果当前的max长度小于新的max长度，更新max的值，并记录下max长度时左侧的位置
            if (max < i - left + 1) {
                //左侧的开始位置重新记录下
                maxLeft = left;
                max = i - left + 1;
            }

        }

        //最后计算一下字符串，左边的位置，不包含
        int right = maxLeft + max;
        //左闭右开
        String substring = s.substring(maxLeft, right);
        System.out.println(substring);
        return substring;
    }

    public static void main(String[] args) {
        String s = " jja koinb8 09hg";
        int i = lengthOfLongestSubstring(s);
        longestSubstring(s);


    }
}
