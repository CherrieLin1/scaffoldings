package com.sy.scaffolding.leetcode.leetcode;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @ClassName Solution215
 * @Description 找一个无序数组中第K大的数
 * 1.首先要对数组进行排序 -- 排序方法的确定
 * 2.选出数组中第K大的数
 * @Author linshengyan
 * @Date 2022/9/21 12:12 下午
 */
public class Solution215 {

    public static void main(String[] args) {
      /*  ConcurrentLinkedQueue<Integer> blockingQueue = new ConcurrentLinkedQueue<Integer>();
        for (int i = 0; i < 10; i++) {
            blockingQueue.add(i);
        }
        for (int i = 0; i < 10; i++) {
            Integer poll = blockingQueue.poll();
            System.out.println("poll = " + poll);
        }*/
        int[] num = {3, 2, 1, 5, 6, 4};
        int k = 2;
        System.out.println(findNum1(num, k));

    }


    /**
     * 解决方法一：用最小堆法解决
     * 优先队列可以用一个 for 循环解决哈
     * 就是在 for 循环里面判断小顶堆里面的 size() 是否大于 k 个数，
     * 是的话就 poll() 出去；整个 for 循环结束之后剩下来的就是 k 个数的小顶堆。堆顶即第 k 大的数。
     *
     * @param arg
     * @param k
     * @return
     */
    public static int findNum(int[] arg, int k) {
        //优先队列,排序是按照升序排列的，所以形成的队列是升序的，当数组中的长度大于k,移除上面的小值，这样子的话留下来的就是最大的值
        PriorityQueue<Integer> queue = new PriorityQueue<>((n1, n2) -> n1 - n2);
        for (int i = 0; i < arg.length; i++) {
            queue.add(arg[i]);
            if (queue.size() > k) {
                queue.poll();
            }
        }

        return queue.poll();
    }


    public static int findNum1(int[] nums, int k) {
        int[] sorted = quickSort(nums, 0, nums.length - 1);
        return sorted[nums.length - k];
    }

    public static int[] quickSort(int[] nums, int low, int high) {
        if (low >high){
             return nums;
        }
        //排序数组的左边的边界
        int left = low;
        //排序数组的右边的边界
        int right = high;
        //选定的基准值
        int base = nums[low];
        //临时变量
        int temp;
        while (left < right) {
            while (base <= nums[right] && left < right) {
                right--;
            }

            //这里谢错误过
            while (base >= nums[left] && left < right) {
                left++;
            }

            temp = nums[left];
            nums[left] = nums[right];
            nums[right] = temp;

        }
        nums[low] = nums[left];
        nums[left] = base;
        quickSort(nums, low, left - 1);
        quickSort(nums, left + 1, high);
        return nums;
    }
}
