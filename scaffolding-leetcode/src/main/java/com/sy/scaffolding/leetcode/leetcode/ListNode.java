package com.sy.scaffolding.leetcode.leetcode;

/**
 * @author create by lsy on 2021/11/25 7:38 下午
 */
public class ListNode {
    /**
     * 值
     */
    int val;

    /**
     * 下一个节点
     */
    ListNode next;

    ListNode(int x) {
        val = x;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}
