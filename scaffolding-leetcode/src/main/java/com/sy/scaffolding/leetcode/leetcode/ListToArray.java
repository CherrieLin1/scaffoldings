package com.sy.scaffolding.leetcode.leetcode;


import org.apache.commons.lang.ArrayUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName ListToArray
 * @Description
 * @Author linshengyan
 * @Date 2022/9/8 2:26 下午
 */
public class ListToArray {
    public void test5(){
        int[] array = {1, 2, 5, 5, 5, 5, 6, 6, 7, 2, 9, 2};

        /*int[]转list*/
        //方法一：需要导入apache commons-lang3  jar
        List<Integer> list = Arrays.asList(ArrayUtils.toObject(array));
        //方法二：java8及以上版本
        List<Integer> list1 = Arrays.stream(array).boxed().collect(Collectors.toList());

        /*list转int[]*/
        //方法一：
        Integer[] intArr =  list.toArray(new Integer[list.size()]);
        //方法二：java8及以上版本
        int[] intArr1 =  list.stream().mapToInt(Integer::valueOf).toArray();

    }
}
