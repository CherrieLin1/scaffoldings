package com.sy.scaffolding.leetcode.leetcode;

/**
 * @ClassName Solution191
 * @Description 位1的个数   编写一个函数，输入是一个无符号整数（以二进制串的形式），返回其二进制表达式中数字位数为 '1' 的个数（也被称为汉明重量）。
 * https://leetcode.cn/problems/number-of-1-bits/
 * @Author linshengyan
 * @Date 2022/9/8 3:04 下午
 */
public class Solution191 {
    /**
     * 1 << n意思是指 1 * 2的n次方
     *
     * @param n
     * @return
     */
    public int hammingWeight(int n) {
        int count = 0;
        for (int i = 0; i < 32; i++) {
            //对比到第几位就用2的n次方，2的n次方，其他位上都是0，只有对应n的对应位置上不为0，结果才不为0
            if ((n & (1 << i)) != 0) {
                count++;
            }
        }
        return count;
    }


    /**
     * @param n
     * @return
     */
    public int hammingWeight1(int n) {
        int count = Integer.bitCount(n);
        return count;
    }
}
