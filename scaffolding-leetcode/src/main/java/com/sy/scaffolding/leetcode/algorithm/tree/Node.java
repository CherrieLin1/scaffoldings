package com.sy.scaffolding.leetcode.algorithm.tree;

/**
 * 节点的类型
 *
 * @author create by lsy on 2021/11/15 2:27 下午
 */
public class Node {
    int data;
    Node leftChild;
    Node rightChild;

    public Node(int data) {
        this.data = data;
    }
}
