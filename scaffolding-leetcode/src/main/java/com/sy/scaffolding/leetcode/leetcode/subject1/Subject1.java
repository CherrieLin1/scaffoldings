package com.sy.scaffolding.leetcode.leetcode.subject1;

import java.util.HashMap;
import java.util.Map;

/**
 * 给定一个整数数组 nums和一个整数目标值 target，请你在该数组中找出 和为目标值 target 的那两个整数，并返回它们的数组下标。
 * <p>
 * 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。
 * <p>
 * 你可以按任意顺序返回答案。
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/two-sum
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 * @author create by lsy on 2021/11/25 7:16 下午
 */
public class Subject1 {

    /**
     * s1:解法1
     * 暴力解，直接循环两次，找到对应的解法，时间复杂度o(n*n)
     *
     * @param nums
     * @param target
     * @return
     */
    public int[] twoSum_s1(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            int another = target - nums[i];
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[j] == another) {
                    return new int[]{i, j};
                }
            }
        }

        throw new IllegalArgumentException("no two sum solution.");

    }


    /**
     * s2:解法2
     * 两次循环，因为数组中的元素不会重复，所以可以直接存为HashMap中的key，但是在二次循环的时候需要判断
     *
     * @param nums
     * @param target
     * @return
     */
    public int[] twoSum_s2(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>(nums.length);
        for (int i = 0; i < nums.length; i++) {
            //因为数组中的元素不会重复，且最后返回的是下标
            map.put(nums[i], i);
        }

        for (int i = 0; i < nums.length; i++) {
            int temp = target - nums[i];
            //如果map中有余数，说明找到了,但是要保证这个数不能是自身
            //特殊场景target = 10,数组中有5，要排除5这种情况
            if (map.containsKey(temp) && i != map.get(temp)) {
                return new int[]{i, map.get(temp)};
            }
        }
        throw new IllegalArgumentException("No two sum solution");
    }

    /**
     * twoSum_s3
     *
     * @param nums
     * @param target
     * @return
     */
    public static int[] twoSum_s3(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>(nums.length);
        for (int i = 0; i < nums.length; i++) {
            int temp = target - nums[i];
            if (map.containsKey(temp)) {
                return new int[]{i, map.get(temp)};
            }
            map.put(nums[i],i);
        }

        throw new IllegalArgumentException("No two sum solution");
    }


    public static void main(String[] args) {
        int[] input= {2,7,11,15};
        int target = 9;

        int[] ints = twoSum_s3(input, target);
        for (int anInt : ints) {
            System.out.println(anInt);
        }
    }
}
