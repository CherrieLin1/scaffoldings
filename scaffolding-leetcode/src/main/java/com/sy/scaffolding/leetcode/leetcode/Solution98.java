package com.sy.scaffolding.leetcode.leetcode;

/**
 * @ClassName Solution98
 * @Description 验证二叉搜索树  给你一个二叉树的根节点 root ，判断其是否是一个有效的二叉搜索树。
 * https://leetcode.cn/problems/validate-binary-search-tree/
 * @Author linshengyan
 * @Date 2022/9/8 4:29 下午
 */
public class Solution98 {
    /**
     * 解题的时候抛开给的测试用例，结合二叉树的数据结构，在脑海里过一遍，得知二叉树的root节点以后，这棵树是什么样子的
     * 搜索二叉树：左节点所有的值小于根节点，右节点所有的值大于根节点，无重复数据
     * ‼️基于上述性质，二叉树中序遍历【左根中右】一定是一个有序的升序数组，按照中序遍历，如果当前的数大于钱一个值，说明这个搜索二叉树是成立的
     * 最终解决方案：中序遍历 + 广度优先算法
     */


    public static void main(String[] args) {
        System.out.println("Integer.MIN_VALUE = " + Integer.MIN_VALUE);
        int value = -2147483648;
        TreeNode treeNode = new TreeNode(value);
        boolean validBST = isValidBST(treeNode);
        System.out.println(validBST);


    }


    /**
     * 这里使用Long的原因是，这个Node有可能是只有一个节点，且值为最小正整数，这时候必须要用Long来解决了，比如【Inter.MIN】
     */
    static long pre = Long.MIN_VALUE;

    public static boolean isValidBST(TreeNode root) {
        if (root == null) {
            return true;
        }
        // 访问左子树
        //左节点判断
        if (!isValidBST(root.left)) {
            return false;
        }
        // 访问当前节点：如果当前节点小于等于中序遍历的前一个节点，说明不满足BST，返回 false；否则继续遍历。
        //todo,对于一颗平衡二叉树来说，没有重复的节点数值，为了<=成立，所以上述必须用long
        if (root.val <= pre) {
            return false;
        }
        //当前root的值，与右节点进行比较
        pre = root.val;
        // 访问右子树
        return isValidBST(root.right);
    }


}


class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode() {
    }

    TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}