package com.sy.scaffolding.leetcode.leetcode;

/**
 * @ClassName Solution9
 * @Description 判断一个整数是不是回文数
 * @Author linshengyan
 * @Date 2022/9/9 3:17 下午
 */
public class Solution9 {
    //2147483647最大正整数的值

    public static void main(String[] args) {
        //max int
        int i = 2147483647;
        isPalindrome(i);
    }

    /**
     * 这一题直接反转这个整数，判断反转后的数和当前这个数是否是一致的，如果是一致的，自然就是回文数
     * @param x
     * @return
     */
    public static boolean isPalindrome1(int x) {
        if (x < 0) {
            return false;
        }
        int num = x;
        int cur = 0;
        while (num != 0) {
            cur = cur * 10 + num % 10;
            num /= 10;
        }
        return cur == x;
    }


    public static boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }
        int cur = 0;
        int num = x;
        while (num != 0) {
            cur = cur * 10 + num % 10;
            //"/"是取整数，%是取余数
            num /= 10;
        }
        System.out.println("cur = " + cur);
        return cur == x;
    }
}
