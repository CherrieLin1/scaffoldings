package com.sy.scaffolding.leetcode.leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Solution6
 * @Description
 * @Author linshengyan
 * @Date 2022/9/9 2:16 下午
 */
public class Solution6 {
    /**
     * 最终结果就是从第一行到最后一行的拼接的结果
     * 时间复杂度 O(N) ：遍历一遍字符串 s；
     * 空间复杂度 O(N) ：各行字符串共占用 O(N)额外空间。
     * @param s
     * @param numRows
     * @return
     */
    public String convert(String s, int numRows) {
        if (numRows < 2) {
            return s;
        }
        List<StringBuilder> list = new ArrayList<StringBuilder>();
        for (int i = 0; i < numRows; i++) {
            StringBuilder stringBuilder = new StringBuilder();
            list.add(stringBuilder);
        }
        //当前的行数，从0开始
        int i = 0;
        int flag = -1;
        char[] chars = s.toCharArray();
        //遍历字符串
        for (char aChar : chars) {
            list.get(i).append(aChar);
            //当进行到达第一行或者最后一行的时候，，flag反转，使行索引正确的变化
            if (i == 0 || i == numRows - 1) {
                flag = -flag;
            }
            i = i + flag;
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (StringBuilder builder : list) {
            stringBuilder.append(builder);
        }
        return stringBuilder.toString();
    }
}
