package com.sy.scaffolding.leetcode.leetcode;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @ClassName Solution19
 * @Description 删除链表指定位置的元素
 * @Author linshengyan
 * @Date 2022/9/9 4:47 下午
 */
public class Solution19 {
    public static void main(String[] args) {
        //for 循环中的i++ 和 ++i 没有区别
        for (int i =0 ; i<9 ; i ++){
            System.out.println(i);
        }

        for (int i =0 ; i<9 ; ++ i){
            System.out.println(i);
        }

    }



    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode dummy = new ListNode(0, head);
        Deque<ListNode> stack = new LinkedList<ListNode>();
        ListNode cur = dummy;
        while (cur != null) {
            //push进数据
            stack.push(cur);
            cur = cur.next;
        }
        //这里的++i和i++没有什么区别
        for (int i = 0; i < n; ++i) {
            //弹出数据
            stack.pop();
        }
        //这里使用pop和peek一样的效果，只是peek不删除第一个元素
        ListNode prev = stack.peek();
        //倒数N+1个元素的下一个N被删除，直接指向prev.next.next
        prev.next = prev.next.next;
        ListNode ans = dummy.next;
        return ans;
    }

}
