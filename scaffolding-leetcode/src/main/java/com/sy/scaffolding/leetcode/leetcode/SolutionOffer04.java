package com.sy.scaffolding.leetcode.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName SolutionOffer04
 * @Description 高效搜索二维数组中目标值
 * @Author linshengyan
 * @Date 2022/9/17 10:37 上午
 */
public class SolutionOffer04 {
    public boolean findNumberIn2DArray(int[][] matrix, int target) {
        int i = matrix.length - 1;
        int j = 0;
        while (i >= 0 && j < matrix[0].length) {
            if (matrix[i][j] > target) {
                //说明i行都大于
                i--;
            } else if (matrix[i][j] < target) {
                //说明J列都小于
                j++;
            } else {
                return true;
            }
        }
        return false;

    }

    public int lengthOfLongestSubstring(String s) {
        int max = 0;
        int left = 0;
        int length = s.length();
        HashMap<Character, Integer> map = new HashMap<>(length);
        for (int i = 0; i < length; i++) {
            char c = s.charAt(i);
            if (map.containsKey(c)) {
                left = Math.max(left, map.get(c) + 1);
            }
            map.put(c, i);
            max = Math.max(max, i - left + 1);

        }
        return max;

    }
}
