package com.sy.scaffolding.leetcode.leetcode;

/**
 * @ClassName Solution1710
 * @Description 主要元素
 * 数组中占比超过一半的元素称之为主要元素。给你一个 整数 数组，找出其中的主要元素。若没有，返回 -1 。请设计时间复杂度为 O(N) 、空间复杂度为 O(1) 的解决方案。
 * @Author linshengyan
 * @Date 2022/9/17 9:08 上午
 */
public class Solution1710 {
    public int majorityElement1(int[] nums) {
        //说明题目中的数字都是正数，候选人为-1
        int candidate = -1;
        //最开始的计数，即投票数。可以明确，如果是主要元素，数据的数量肯定超过一半
        //根据投票数的变化，重新选定候选人
        int count = 0;
        for (int num : nums) {
            //第一次，count == 0,第一个数赋值给candidate,紧接着count就会自增，
            //下一次，即便不一样的数，count自减一次，为0
            //再下一次便会重复第一次的情况，count永远不会为0
            if (count == 0) {
                candidate = num;
            }
            if (num == candidate) {
                count++;
            } else {
                count--;
            }
        }

        //投票结束，重新设定计数，计算数组中的数据和最后候选人一样的数据
        count = 0;
        int length = nums.length;
        for (int num : nums) {
            if (num == candidate) {
                count++;
            }
        }
        return count * 2 > length ? candidate : -1;

    }

    public int majorityElement(int[] nums) {
        int count = 0;
        int candidate = -1;
        for (int num : nums) {
            if (count == 0) {
                candidate = num;
            }
            if (candidate == num) {
                count += 1;
            } else {
                count -= 1;
            }
        }
        count = 0;
        for (int num : nums) {
            if (num == candidate) {
                count++;
            }
        }
        return count * 2 > nums.length ? candidate : -1;
    }
}


