package com.sy.scaffolding.dao.sharingalgorithm;

import com.alibaba.fastjson.JSON;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.util.Collection;

/**
 * @ClassName WIdSharingAlgorithm
 * @Description 自定义标准分片策略----wid自定义分表策略
 * @Author linshengyan
 * @Date 2022/8/2 10:51 上午
 */
public class WidSharingAlgorithm implements PreciseShardingAlgorithm<Long> {

    @Override
    public String doSharding(Collection<String> collection, PreciseShardingValue<Long> preciseShardingValue) {

        System.out.println("collection: " + JSON.toJSONString(collection) + " ,preciseShardingValue: "
                + JSON.toJSONString(preciseShardingValue));

        Long id = preciseShardingValue.getValue();
        for (String name : collection) {
            //直接对表数量取余数
            if (name.endsWith(id % collection.size() + "")) {
                System.out.println("return name: " + name);
                return name;
            }
        }
        throw new IllegalArgumentException();
    }
}


