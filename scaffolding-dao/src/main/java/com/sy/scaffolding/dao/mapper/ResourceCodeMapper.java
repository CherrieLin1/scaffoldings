package com.sy.scaffolding.dao.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.scaffolding.dao.entity.ResourceCodeDO;
import org.springframework.stereotype.Repository;

@Repository
/**
 * Mybatis实现
 */
public interface ResourceCodeMapper extends BaseMapper<ResourceCodeDO> {

    int deleteByPrimaryKey(Long id);


    ResourceCodeDO selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ResourceCodeDO record);


    ResourceCodeDO selectByIdAndCode(Long id,String code);
}