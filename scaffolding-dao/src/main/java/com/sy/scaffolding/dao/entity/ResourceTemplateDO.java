package com.sy.scaffolding.dao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)//实现链式变成，可以理解成Build
@TableName("resource_template")
public class ResourceTemplateDO {
    /**
     * ID
     *
     * @mbg.generated
     */
    private Long id;

    /**
     * 资源模板名称
     *
     * @mbg.generated
     */
    private String name;

    /**
     * 状态值 0：不可用，1：可用
     *
     * @mbg.generated
     */
    private Integer status;

    /**
     * 创建人
     *
     * @mbg.generated
     */
    private Long creator;

    /**
     * 资源类型，1：券，2：码，3：组合资源
     *
     * @mbg.generated
     */
    private Integer type;

    /**
     * 创建时间
     *
     * @mbg.generated
     */
    private Date createTime;

    /**
     * 更新时间
     *
     * @mbg.generated
     */
    private Date updateTime;

    /**
     * 逻辑删除：0-未删除，1-已删除
     *
     * @mbg.generated
     */
    private Integer isDeleted;


    /**
     * 商家id，分库分表key
     *
     * @mbg.generated
     */
    private Long merchantId;

    /**
     * 商家下的具体treeid
     *
     * @mbg.generated
     */
    private Long bosId;

    /**
     * 归属vid
     *
     * @mbg.generated
     */
    private Long vid;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }


    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public Long getBosId() {
        return bosId;
    }

    public void setBosId(Long bosId) {
        this.bosId = bosId;
    }

    public Long getVid() {
        return vid;
    }

    public void setVid(Long vid) {
        this.vid = vid;
    }


}