package com.sy.scaffolding.dao.sharingalgorithm;

import org.apache.shardingsphere.api.sharding.hint.HintShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.hint.HintShardingValue;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @ClassName Hint
 * @Description 强制路由的算法
 * 路由的配置方式有多种，
 * 之前实现了inline行表达式、
 * stander标准方法--RangeShardingAlgorithm、PreciseShardingAlgorithm
 * 以及复杂的分片策略--ComplexKeysShardingAlgorithm
 * 现在实现的是强制路由--
 * @Author linshengyan
 * @Date 2022/8/3 2:56 下午
 */
public class HintAlgorithm implements HintShardingAlgorithm {
    /**
     *
     * @param collection  可能的目标名称，比如库的名字db0 db1 或者表的名字 table1-tableN
     * @param hintShardingValue   sharding value injected by hint, not in SQL.指定的库表的索引，强制进入哪张表，强制路由的时候写入的
     * @return
     */
    @Override
    public Collection<String> doSharding(Collection collection, HintShardingValue hintShardingValue) {
        Integer index = (Integer) hintShardingValue.getValues().iterator().next();
        Object target = new ArrayList<>(collection).get(index);
        ArrayList arrayList = new ArrayList();
        arrayList.add(target);
        return arrayList;
    }
}
