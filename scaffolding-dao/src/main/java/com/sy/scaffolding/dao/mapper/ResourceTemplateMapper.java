package com.sy.scaffolding.dao.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.scaffolding.dao.entity.ResourceTemplateDO;
import org.springframework.stereotype.Repository;

@Repository
public interface ResourceTemplateMapper extends BaseMapper<ResourceTemplateDO> {

    ResourceTemplateDO selectByIdAndBosId(Long id, Long bosId);
}