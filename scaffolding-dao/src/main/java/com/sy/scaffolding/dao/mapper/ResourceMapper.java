package com.sy.scaffolding.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.scaffolding.dao.entity.ResourceDO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
/**
 * 继承baseMapper其实就是使用了mybatisPlus
 */
public interface ResourceMapper extends BaseMapper<ResourceDO> {

    int deleteByPrimaryKey(Long id);

    int insertSelective(ResourceDO record);

    ResourceDO selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ResourceDO record);

    int updateByPrimaryKey(ResourceDO record);

    List<ResourceDO> getAll();
}