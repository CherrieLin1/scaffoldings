package com.sy.scaffolding.dao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)//实现链式变成，可以理解成Build
//mybatisPlus这个一定要加，因为默认表为模型首字母小些
@TableName("resource_code")
public class ResourceCodeDO {
    /**
     * ID
     *
     * @mbg.generated
     */
    private Long id;

    /**
     * 资源码
     *
     * @mbg.generated
     */
    private String code;

    /**
     * 资源模板id
     *
     * @mbg.generated
     */
    private Long resourceTemplateId;

    /**
     * 资源id(领取或者使用后会和具体用户绑定，新增 resource表记录对应id
     * )
     *
     * @mbg.generated
     */
    private Long resourceId;

    /**
     * 状态值 0：不可用，1：可用
     *
     * @mbg.generated
     */
    private Integer status;

    /**
     * 买家id
     *
     * @mbg.generated
     */
    private Long wid;

    /**
     * 创建时间
     *
     * @mbg.generated
     */
    private Date createTime;

    /**
     * 更新时间
     *
     * @mbg.generated
     */
    private Date updateTime;

    /**
     * 逻辑删除：0-未删除，1-已删除
     *
     * @mbg.generated
     */
    private Long isDeleted;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getResourceTemplateId() {
        return resourceTemplateId;
    }

    public void setResourceTemplateId(Long resourceTemplateId) {
        this.resourceTemplateId = resourceTemplateId;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getWid() {
        return wid;
    }

    public void setWid(Long wid) {
        this.wid = wid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Long isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", code=").append(code);
        sb.append(", resourceTemplateId=").append(resourceTemplateId);
        sb.append(", resourceId=").append(resourceId);
        sb.append(", status=").append(status);
        sb.append(", wid=").append(wid);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", isDeleted=").append(isDeleted);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}