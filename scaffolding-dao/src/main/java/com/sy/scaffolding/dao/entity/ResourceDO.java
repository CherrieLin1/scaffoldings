package com.sy.scaffolding.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @ClassName ResourceDO
 * @Description
 * @Author linshengyan
 * @Date 2022/3/16 2:07 下午
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)//实现链式变成，可以理解成Build
@TableName("resource")
public class ResourceDO {
    /**
     * 主键ID
     *
     * @mbg.generated
     */
    private Long id;

    /**
     * 资源码
     *
     * @mbg.generated
     */
    private String code;

    /**
     * 资源模板名称
     *
     * @mbg.generated
     */
    private String name;

    /**
     * 资源模板id
     *
     * @mbg.generated
     */
    private Long resourceTemplateId;

    /**
     * 状态值 0：不可用，1：可用
     *
     * @mbg.generated
     */
    private Integer status;

    /**
     * 买家id
     *
     * @mbg.generated
     */
    private Long wid;

    /**
     * 创建时间
     *
     * @mbg.generated
     */
    private Date createTime;

    /**
     * 更新时间
     *
     * @mbg.generated
     */
    private Date updateTime;

    /**
     * 逻辑删除：0-未删除，大于0-已删除
     *
     * @mbg.generated
     */
    private Long isDeleted;

    /**
     * 商家id
     *
     * @mbg.generated
     */
    private Long merchantId;

    /**
     * tree_id
     *
     * @mbg.generated
     */
    private Long bosId;

    /**
     * 具体产生数据的节点
     *
     * @mbg.generated
     */
    private Long vid;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getResourceTemplateId() {
        return resourceTemplateId;
    }

    public void setResourceTemplateId(Long resourceTemplateId) {
        this.resourceTemplateId = resourceTemplateId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getWid() {
        return wid;
    }

    public void setWid(Long wid) {
        this.wid = wid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Long isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public Long getBosId() {
        return bosId;
    }

    public void setBosId(Long bosId) {
        this.bosId = bosId;
    }

    public Long getVid() {
        return vid;
    }

    public void setVid(Long vid) {
        this.vid = vid;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", code=").append(code);
        sb.append(", name=").append(name);
        sb.append(", resourceTemplateId=").append(resourceTemplateId);
        sb.append(", status=").append(status);
        sb.append(", wid=").append(wid);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", isDeleted=").append(isDeleted);
        sb.append(", merchantId=").append(merchantId);
        sb.append(", bosId=").append(bosId);
        sb.append(", vid=").append(vid);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}