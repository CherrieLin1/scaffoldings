package com.sy.scaffolding.dao.sharingalgorithm;

import org.apache.shardingsphere.api.config.sharding.strategy.HintShardingStrategyConfiguration;
import org.apache.shardingsphere.core.strategy.route.ShardingStrategy;
import org.apache.shardingsphere.core.strategy.route.ShardingStrategyFactory;

/**
 * @ClassName HintShardingStrategyFactory
 * @Description Hint算法的工厂
 * @Author linshengyan
 * @Date 2022/8/3 3:28 下午
 */
public class HintShardingStrategyFactory {
    private volatile static ShardingStrategy hitShardingStrategy;

    public static ShardingStrategy getInstance() {
        if (hitShardingStrategy == null) {
            synchronized (HintShardingStrategyFactory.class) {
                if (hitShardingStrategy == null) {
                    hitShardingStrategy = ShardingStrategyFactory.newInstance(new HintShardingStrategyConfiguration(new HintAlgorithm()));
                }

            }
        }
        return hitShardingStrategy;
    }
}
